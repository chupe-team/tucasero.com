<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stand extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('stand_model'); 
	}

	public function index() {
		$this->load->view('public_profile_stand_view');
	}

	public function Stand() {
		$standId = $_GET['id'];
		$standInfo1 = $this->stand_model->getStandFromStand($standId);
		$standInfo2 = $this->stand_model->getUsersFromStand($standId);
		$standInfo3 = $this->stand_model->getProductsFromStand($standId);
		$listUsers = $this->stand_model->getUserOwnersFromStand($standId);
		$findStand = $this->stand_model->findStand($standId);
		$data['standInfo1']=$standInfo1;
		$data['standInfo2']=$standInfo2;
		$data['standInfo3']=$standInfo3;
		$data['standId']=$standId;
		$data['listUsers']=$listUsers;
		$originalStandId=-1;
		foreach ($standInfo1->result() as $row) { 
			$originalStandId = $row->standId;
			$data['originalStandId'] = $originalStandId;
			$data['numStandProducts'] = $this->stand_model->getNumStandProducts($originalStandId);
			$data['maxNumStandProducts'] = $this->stand_model->getMaxNumStandProducts($originalStandId);
			$data['getMaxNumStandOwners'] = $this->stand_model->getMaxNumStandOwners($originalStandId);
		}
		$listOfUsersForStand = $this->stand_model->getAllUsersForStand($originalStandId);
		$isOwner = $this->stand_model->isPartOfStand($originalStandId);
		$data['listOfUsersForStand'] = $listOfUsersForStand;
		$data['isOwner'] = $isOwner;
		$idStand = $this->stand_model->getStandId($standId);
		$data['standOrders'] = $this->stand_model->cargarPedidosPorStand($idStand);
		$data['numberOfReports'] = $this->stand_model->contarNumeroDenuncias($_SESSION['userSesion']);
		if($findStand->num_rows() > 0){ //if stand exists in database
			if($_SESSION['userSesion'] !== null) {
				$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$data['sendUserRole'] = 0; 
			}
			$this->load->view('public_profile_stand_view', $data);
		}
		else{
			if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$dataHead['sendUserRole'] = 0; 
			}
			$dataHead['headTitle'] = "Error, Stand No Existe"; 

			$this->load->view('headFooter/head', $dataHead);
			$sendError['sendError'] = "No existe este stand!";
			$this->load->view('error_404_not_found_view', $sendError);
			$this->load->view('headFooter/footer');
		}
	}

	public function RegistrarStand() {
		$userId = $_SESSION['userSesion'];
		$numUserStands = $this->user_model->getNumUserStands($userId, 1);
		$maxNumUserStands = $this->user_model->getMaxNumUserStands($userId);
		if($numUserStands < $maxNumUserStands) {
			if($_SESSION['userSesion'] !== null) {
				$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$data['sendUserRole'] = 0; 
			}
			$this->load->view('register_stand_view',$data);
		}
		else{
			redirect('Usuario/TuZona','refresh');
		}
	}

	public function RegisterStand() {
		$data['standName']=$_POST['standName'];
		$data['standDescription']=$_POST['standDescription'];
		$encryptedStandId = $this->stand_model->insertStand($data);
		$idStand = $this->stand_model->getStandId($encryptedStandId."");
		copy($_FILES['upload']['tmp_name'],'images/TuCaseroImages/stands/'.$idStand.'.jpg');
		redirect('stand/stand?id='.$encryptedStandId);
	}

	public function ActualizarStand() {
		if(isset($_POST['standId'])){
			$standId = $_POST['standId'];
			$data['standId'] = $standId;
			$data['lastUpdate'] = date('Y-m-d H:i:s');
			$data['standInfo'] = $this->stand_model->getStand($standId);
			/*$data['numOfCharInStandName'] = ;
			$data['numOfCharInStandDescription'] = ;*/
			if($_SESSION['userSesion'] !== null) {
				$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$data['sendUserRole'] = 0; 
			}
			$this->load->view('update_stand_view',$data);
		}
		else{
			redirect('Usuario/TuZona','refresh');
		}
	}

	public function UpdateStand() {
		date_default_timezone_set("America/La_Paz");
		$StandID2 = $_POST['standId'];
		$data['standName'] = $_POST['name'];
		$data['standDescription'] = $_POST['description'];
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		if($_FILES['upload']['tmp_name'])
		{
			unlink('images/TuCaseroImages/stands/'.$StandID2.'.jpg');
			copy($_FILES['upload']['tmp_name'],'images/TuCaseroImages/stands/'.$StandID2.'.jpg');
		}
		$this->stand_model->updateStand($StandID2,$data);
		redirect('stand/stand?id='.crypt(crypt(crypt($StandID2,'st'),'st'),'st'));
	}

	public function DeleteStand() {
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$StandIDD = $_POST['standIdD'];
		$data['status'] = 0;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$this->stand_model->deleteStand($StandIDD, $data);
		redirect('Usuario/TuZona','refresh');
	}

	public function DeleteStand2() {
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$StandIDD = $_POST['standIdD'];
		$data['status'] = 0;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$this->stand_model->deleteStand($StandIDD, $data);
		redirect('Stand/ListaStands','refresh');
	}

	public function ReportStand() {
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$data['standId'] = $_POST['standIdD'];
		$data['userId'] = $_SESSION['userSesion'];
		$data['reportTypeId'] = $_POST['reportTypeId'];
		$data['description'] = $_POST['description'];
		$this->stand_model->reportStand($data);
		$standIdDB = $this->stand_model->getStand($_POST['standIdD']);
    	foreach ($standIdDB->result() as $row) {
    		$standId = $row->encryptedStandId;
    	}
    	redirect('stand/stand?id='.$standId);
	}

	public function UpdateStandOwners() {
		$standId = $_POST['standId'];

		$usersFromDB = array();
		$nonMainOwnersFromStand = $this->stand_model->getNonMainOwnersFromStand($standId);
		foreach($nonMainOwnersFromStand->result() as $row) {
			$usersFromDB[] = $row->userId; //user id (base de datos)
		}

		if(isset($_POST['users'])){
			$users = $_POST['users'];
			$this->stand_model->updateMainOwnersFromStand1($standId, $users, $usersFromDB);
		}
		else{
			$this->stand_model->updateMainOwnersFromStand2($standId, $usersFromDB);
		}

		if (isset($_SERVER["HTTP_REFERER"])) {
			header("Location: " . $_SERVER["HTTP_REFERER"]);
		}
	}

	public function ListaStands() {
		$lista = $this->stand_model->getStand2(1);
		$data['standList'] = $lista;
		if($_SESSION['userSesion'] !== null) {
			$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$data['sendUserRole'] = 0; 
		}
		$this->load->view('list_of_stands_view',$data);
	}

	public function ListaStandsPDF() {
    	$this->load->library('pdf_mc'); //cargar la libreria pdf
    	$lista = $this->stand_model->getStand2(1);
    	$totalStands = $this->stand_model->selectTotalStands();
    	$totalStandsPremium = $this->stand_model->selectPremiumStands();
    	$totalNormalStands = $this->stand_model->selectNormalStands();
    	$lista=$lista->result();
    	$registeredStandByMonth = $this->stand_model->registeredStandByMonth();
    	$registeredStandByMonth=$registeredStandByMonth->result();
    	$this->pdf=new pdf_mc('L','mm','A4');
    	$this->pdf->AddPage(); //agregamos una pagina
		$this->pdf->AliasNbPages(); //identificador que imprime pagina n de n
		$this->pdf->SetTitle("Stands"); //titulo docuemento
		$this->pdf->SetLeftMargin(5);
		$this->pdf->SetRightMargin(5);
		$this->pdf->SetFillColor(210,210,210); //color de fondo
		$this->pdf->SetFont("Arial","B",10);
		$this->pdf->Cell(30); //celdas (ancho)
		$this->pdf->Cell(200,15,utf8_decode('STANDS'),0,0,'C',1); //ancho, alto, texto,borde,comienzoLinea,linea,align,fondo
		
		$this->pdf->Ln(30);
		$this->pdf->SetWidths(Array(55,50,55));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(55,10,utf8_decode('TOTAL DE STANDS'),'TBLR',0,'C',1);
		$this->pdf->Cell(50,10,utf8_decode('STANDS PREMIUM'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('STANDS NORMALES'),'TBLR',0,'C',1);

		$this->pdf->Ln(10);
		$this->pdf->RowDatos(array(
				$totalStands,
				$totalStandsPremium,
				$totalNormalStands
			));

		$this->pdf->Ln(15);

		$this->pdf->SetWidths(Array(30,50));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(30,10,utf8_decode('Mes'),'TBLR',0,'C',1);
		$this->pdf->Cell(50,10,utf8_decode('Stands Registrados'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);

		foreach ($registeredStandByMonth as $row) 
		{
			$month=$row->month;
			$totalStandsRegistered=$row->totalStandsRegistered;

			$this->pdf->RowDatos(array(
				$month,
				$totalStandsRegistered
			));
		}

		$this->pdf->Ln(20);

		$this->pdf->SetWidths(Array(7,55,145,30,30,20));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(7,10,utf8_decode('#'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Nombre'),'TBLR',0,'C',1);
		$this->pdf->Cell(145,10,utf8_decode('Descripción'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Añadido'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Actualizado'),'TBLR',0,'C',1);
		$this->pdf->Cell(20,10,utf8_decode('Tipo'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);

		
		$indice = 1;
		foreach ($lista as $row) 
		{
			$standName=$row->standName;
			$description=$row->standDescription;
			$createDate=$row->createDate;
			$lastUpdate=$row->lastUpdate;
			switch ($row->standTypeId) {
				case "1":
					$type="Normal";
					break;
				case "2":
					$type="Premium";
					break;
			}
			$this->pdf->Row(array(
				$indice,
				$standName,
				$description,
				$createDate,
				$lastUpdate,
				$type
			));
			$indice++;
		}
		$this->pdf->Output('stands.pdf','I');
    }

    public function ListaStandsDenunciasPDF() {
    	$this->load->library('pdf_mc'); //cargar la libreria pdf
    	$lista = $this->stand_model->getReportsStands();
    	$totalStands = $this->stand_model->selectTotalStands();
    	$totalReports = $this->stand_model->selectTotalReports();
    	$lista=$lista->result();
    	$selectReportsByMonth = $this->stand_model->selectReportsByMonth();
    	$selectReportsByMonth=$selectReportsByMonth->result();
    	$this->pdf=new pdf_mc('L','mm','A4');
    	$this->pdf->AddPage(); //agregamos una pagina
		$this->pdf->AliasNbPages(); //identificador que imprime pagina n de n
		$this->pdf->SetTitle("Stands/Denuncias"); //titulo docuemento
		$this->pdf->SetLeftMargin(5);
		$this->pdf->SetRightMargin(5);
		$this->pdf->SetFillColor(210,210,210); //color de fondo
		$this->pdf->SetFont("Arial","B",10);
		$this->pdf->Cell(35); //celdas (ancho)
		$this->pdf->Cell(200,15,utf8_decode('STANDS / DENUNCIAS'),0,0,'C',1); //ancho, alto, texto,borde,comienzoLinea,linea,align,fondo


		$this->pdf->Ln(30);
		$this->pdf->SetWidths(Array(55,50));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(55,10,utf8_decode('TOTAL DE STANDS'),'TBLR',0,'C',1);
		$this->pdf->Cell(50,10,utf8_decode('TOTAL DENUNCIAS'),'TBLR',0,'C',1);

		$this->pdf->Ln(10);
		$this->pdf->RowDatos(array(
				$totalStands,
				$totalReports
			));

		$this->pdf->Ln(15); //salto de linea

		$this->pdf->SetWidths(Array(30,55));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(30,10,utf8_decode('Mes'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Denuncias'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);

		foreach ($selectReportsByMonth as $row) 
		{
			$mes=$row->mes;
			$standsReported=$row->standsReported;
			$this->pdf->RowDatos(array(
				$mes,
				$standsReported
			));
		}
		$this->pdf->Ln(20);
		
		$this->pdf->SetWidths(Array(7,55,55,100,40));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(7,10,utf8_decode('#'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Nombre Stand'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Dueño'),'TBLR',0,'C',1);
		$this->pdf->Cell(100,10,utf8_decode('Correo Electrónico'),'TBLR',0,'C',1);
		$this->pdf->Cell(40,10,utf8_decode('Denuncias recibidas'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);

		
		$indice = 1;
		foreach ($lista as $row) 
		{
			$standName=$row->NombreStand;
			$userName=$row->Dueño;
			$email=$row->Email;
			$totalReports=$row->totalDenuncias;
			$this->pdf->Row(array(
				$indice,
				$standName,
				$userName,
				$email,
				$totalReports
			));
			$indice++;
		}
		$this->pdf->Output('standsDenuncias.pdf','I');
    }
}