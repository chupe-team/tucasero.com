<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	public function index() {
		if($this->session->userdata('userId') !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0; 
		}
		$dataHead['headTitle'] = "Error, Página No Existe"; 

		$this->load->view('headFooter/head', $dataHead);
		$sendError['sendError'] = "No existe esta página!";
		$this->load->view('error_404_not_found_view', $sendError);
		$this->load->view('headFooter/footer');
	}
}