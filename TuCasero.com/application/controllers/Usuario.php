<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html */

	public function __construct() {
		parent::__construct();
		$this->load->model('user_model'); 
	}

	public function index() {
		$this->eliminarSesiones();
		redirect('Producto/Search', 'refresh');
		/*$dataHead['sendUserRole'] = 0;	
		$dataHead['headTitle'] = "Inicio";
		$dataHead['products'] = $this->product_model->obtener12ProductosAleatorios();
		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('home_view');
		$this->load->view('headFooter/footer');*/
	}

	public function CerrarSesion() {
		$this->eliminarSesiones();
		$_SESSION['userSesion'] = 0;
		$_SESSION['recuerdame'] = 0;
		$_SESSION['userRole'] = 0;

		redirect('Producto/Search', 'refresh');

		/*$dataHead['sendUserRole'] = 0;
		$dataHead['headTitle'] = "Inicio";
		$dataHead['products'] = $this->product_model->obtener12ProductosAleatorios();
		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('home_view');
		$this->load->view('headFooter/footer');*/
	}

	public function IniciarSesion() {
		$data['msg'] = $this->uri->segment(3);
		$this->load->view('login_view', $data);
	}

	public function crearSesiones() {
		$_SESSION['nombres'] ="";
		$_SESSION['primerApellido'] ="";
		$_SESSION['segundoApellido'] ="";
		$_SESSION['username'] ="";
		$_SESSION['password'] ="";
		$_SESSION['password2'] ="";
		$_SESSION['dateOfBirth'] ="";
		$_SESSION['ci'] ="";
		$_SESSION['email'] ="";
		$_SESSION['phoneNumber'] ="";
		$_SESSION['gender'] ="";
	}

	public function eliminarSesiones() {
		unset($_SESSION['nombres']);
		unset($_SESSION['primerApellido']);
		unset($_SESSION['segundoApellido']);
		unset($_SESSION['username']);
		unset($_SESSION['password']);
		unset($_SESSION['password2']);
		unset($_SESSION['dateOfBirth']);
		unset($_SESSION['ci']);
		unset($_SESSION['email']);
		unset($_SESSION['phoneNumber']);
		unset($_SESSION['gender']);
	}

	public function Registrarse() {
		$this->eliminarSesiones();
		$this->crearSesiones();
		$this->load->view('register_user_view');
	}

	public function RegisterUser() {
		$ComfirmUsername = FALSE;
		$ComfirmCi = FALSE;
		$ComfirmEmail = FALSE;
		$ComfirmPhoneNumber= FALSE;


		$nombres=$_POST['name'];
		$primerApellido=$_POST['firstSurname'];
		$segundoApellido=$_POST['secondSurname'];
		$username=$_POST['username'];
		$password=$_POST['password'];
		$password2=$_POST['re_password'];
		$dateOfBirth=$_POST['dateOfBirth'];
		$ci=$_POST['ci'];
		$email=$_POST['email'];
		$phoneNumber=$_POST['phoneNumber'];
		$gender=$_POST['gender'];

		$_SESSION['nombres']=$nombres;
		$_SESSION['primerApellido']=$primerApellido;
		$_SESSION['segundoApellido']=$segundoApellido;
		$_SESSION['username'] = $username;
		$_SESSION['password'] =$password;
		$_SESSION['password2'] =$password2;
		$_SESSION['dateOfBirth'] =$dateOfBirth;
		$_SESSION['ci'] =$ci;
		$_SESSION['email'] =$email;
		$_SESSION['phoneNumber'] =$phoneNumber;
		$_SESSION['gender'] =$gender;

		$queryUsername=$this->user_model->validateUsername($username);
		if($queryUsername->num_rows()>0)
		{
			$_SESSION['username'] = "";
		}
		else
		{
			$ComfirmUsername = TRUE;
		}
			
		$queryCi=$this->user_model->validateCi($ci);
		if($queryCi->num_rows()>0)
		{
			$_SESSION['ci'] = "";
		}
		else
		{
			$ComfirmCi = TRUE;
		}

		$queryEmail=$this->user_model->validateEmail($email);
		if($queryEmail->num_rows()>0)
		{
			$_SESSION['email'] = "";
		}
		else
		{
			$ComfirmEmail = TRUE;
		}
				
		$queryNumber=$this->user_model->validatePhoneNumber($phoneNumber);
		if($queryNumber->num_rows()>0)
		{
			$_SESSION['phoneNumber'] = "";
		}
		else
		{
			$ComfirmPhoneNumber = TRUE;
		}

		if($ComfirmUsername && $ComfirmCi && $ComfirmEmail && $ComfirmPhoneNumber)
		{
			$data['username']=$username;
			$data['password']=md5($password);
			$data['name']=$nombres;
			$data['firstSurname']=$primerApellido;
			$data['secondSurname']=$segundoApellido;
			$data['dateOfBirth']=$dateOfBirth;
			$data['email']=$email;
			$data['id']=$ci;
			$data['phoneNumber']=$phoneNumber;
			$data['gender']=$gender;
			$data['roleId'] = 2;
			$this->user_model->insertUser($data);
			$this->eliminarSesiones();
			redirect('Usuario/IniciarSesion','refresh');
		}
		else
		{
			$this->load->view('register_user_view');
		}	
	}

	public function TuZona() {
		$this->eliminarSesiones();
		$UserID = $_SESSION['userSesion'];
		$EncryptedUserID = crypt(crypt($UserID,'st'),'st');
		$query=$this->user_model->getUser($EncryptedUserID);
		if($query->num_rows()>0) {
			foreach($query->result() as $row) {
				$data['username'] = $row->username;
				$data['name'] = $row->name;
				$data['firstSurname'] = $row->firstSurname;
				$data['secondSurname'] = $row->secondSurname;
				$data['dateOfBirth'] = $row->dateOfBirth;
				$data['email'] = $row->email;
				$data['phoneNumber'] = $row->phoneNumber;
				$data['gender'] = $row->gender;
			}
		}
		$query1=$this->stand_model->selectStandFromUser($UserID, 1);
		$data['listOfStands']=$query1;
		$query2=$this->stand_model->selectStandFromUser($UserID, 0);
		$data['listOfNonMainOwnerStands']=$query2;
		$data['listOfProductsFavorites']=$this->user_model->getProductsFavorites($UserID);
		$data['numUserStands']=$this->user_model->getNumUserStands($UserID, 1);
		$data['numNonMainUserStands']=$this->user_model->getNumUserStands($UserID, 0);
		$data['maxNumUserStands']=$this->user_model->getMaxNumUserStands($UserID);

		if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0; 
		}
		$dataHead['headTitle'] = "Tu Zona"; 
		$data['userOrders']=$this->user_model->getUserOrders($UserID);
		$this->load->view('headFooter/head', $dataHead);
		
		$this->load->view('update_user_view', $data);
		$this->load->view('headFooter/footer');
	}

	public function ValidateUser() {
		$this->eliminarSesiones();
		$userName=$_POST['userName'];
		$password=md5($_POST['password']);
		$query=$this->user_model->validateUser($userName, $password); //TO DO: verificar que en el modelo los nombres de los metodos
		if($query->num_rows()>0) {
			foreach($query->result() as $row) {
				$_SESSION['userRole'] =$row->roleId;
				$_SESSION['userSesion'] =$row->userId;
				$_SESSION['isPremium']= $this->user_model->isPremium($_SESSION['userSesion']);
			}
			if(isset($_POST['recuerdame']))
			{
				$_SESSION['recuerdame'] = 1;
			}
			redirect('Producto/Search','refresh');
		}
		else {
			redirect('Usuario/IniciarSesion/1','refresh');
		}
	}

	public function UpdateUser() {
		$this->eliminarSesiones();
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$data['name']=$_POST['name'];
		$data['firstSurname']=$_POST['firstSurname'];
		$data['secondSurname']=$_POST['secondSurname'];
		$data['dateOfBirth']=$_POST['dateOfBirth'];
		$data['phoneNumber']=$_POST['phoneNumber'];
		$data['email']=$_POST['email'];
		$data['gender']=$_POST['gender'];
		$data['lastUpdate']=date('Y-m-d H:i:s');
		$this->user_model->updateUser($_SESSION['userSesion'], $data);
		redirect('Usuario/TuZona','refresh');
	}

	public function DeleteUser() {
		$this->eliminarSesiones();
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$data['status'] = 0;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$this->user_model->deleteUser($_SESSION['userSesion'], $data);
		redirect('Usuario/index','refresh');
	}
	
	public function Inicio() {
		$this->eliminarSesiones();
		
		redirect('Producto/Search', 'refresh');
		/*if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0;
		}
		$dataHead['headTitle'] = "Inicio";
		$dataHead['products'] = $this->product_model->obtener12ProductosAleatorios();
		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('home_view');
		$this->load->view('headFooter/footer');*/
	}

	public function UsuariosHabilitados() {
		$this->eliminarSesiones();
		$userList = $this->user_model->selectUser(1);
		$totalUser = $this->user_model->selectTotalUser();
		$totalEnabledUser = $this->user_model->selectEnabledUser(1);
		$data['userList'] = $userList;
		$data['totalUser'] = $totalUser;
		$data['totalEnabledUser'] = $totalEnabledUser;
		if($_SESSION['userSesion'] !== null) {
			$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$data['sendUserRole'] = 0; 
		}
		$this->load->view('list_of_users_view', $data);
	}

    public function ListaUsuariosHabilitados() {
    	$this->load->library('pdf_mc'); //cargar la libreria pdf
		$this->eliminarSesiones();
    	$lista = $this->user_model->selectUser(1);
    	$totalUser = $this->user_model->selectTotalUser();
    	$totalEnabledUser = $this->user_model->selectEnabledUser(1);
    	$lista=$lista->result();
    	$usersRegisteredMonth = $this->user_model->selectRegisterUserByMonth();
    	$usersRegisteredMonth=$usersRegisteredMonth->result();
    	$this->pdf=new pdf_mc('L','mm','A4');
    	$this->pdf->AddPage(); //agregamos una pagina
		$this->pdf->AliasNbPages(); //identificador que imprime pagina n de n
		$this->pdf->SetTitle("Usuarios Habilitados"); //titulo docuemento
		$this->pdf->SetLeftMargin(2);
		$this->pdf->SetRightMargin(2);
		$this->pdf->SetFillColor(210,210,210); //color de fondo
		$this->pdf->SetFont("Arial","B",10);
		$this->pdf->Cell(30); //celdas (ancho)
		$this->pdf->Cell(200,15,utf8_decode('USUARIOS HABILITADOS'),0,0,'C',1); //ancho, alto, texto,borde,comienzoLinea,linea,align,fondo

		$this->pdf->Ln(30);
		$this->pdf->SetWidths(Array(50,55));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(50,10,utf8_decode('Total de Usuarios'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Usuarios Habilitados'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		$this->pdf->RowDatos(array(
				$totalUser,
				$totalEnabledUser
		));

		$this->pdf->Ln(15);
		$this->pdf->SetWidths(Array(50,55)); //total 
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(50,10,utf8_decode('Mes'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Usuarios Registrados'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		foreach ($usersRegisteredMonth as $row) 
		{
			$monthRU=$row->mesRU;
			$totalUserRegister=$row->totalUsersRegister;
			$this->pdf->RowDatos(array(
				$monthRU,
				$totalUserRegister
			));
		}
		$this->pdf->Ln(20); //salto de linea

		$this->pdf->SetWidths(Array(10,30,90,20,22,85,20,12));
		$this->pdf->SetLineHeight(6);		
		$this->pdf->Cell(10,10,utf8_decode('#'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Usuario'),'TBLR',0,'C',1);
		$this->pdf->Cell(90,10,utf8_decode('Nombre Completo'),'TBLR',0,'C',1);
		$this->pdf->Cell(20,10,utf8_decode('C.I'),'TBLR',0,'C',1);
		$this->pdf->Cell(22,10,utf8_decode('Fecha Nac.'),'TBLR',0,'C',1);
		$this->pdf->Cell(85,10,utf8_decode('Correo Electrónico'),'TBLR',0,'C',1);
		$this->pdf->Cell(20,10,utf8_decode('Teléfono'),'TBLR',0,'C',1);
		$this->pdf->Cell(12,10,utf8_decode('Sexo'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		
		$indice = 1;
		foreach ($lista as $row) 
		{
			$username=$row->username;
			$name=$row->name.' '.$row->firstSurname.' '.$row->secondSurname;
			$id=$row->id;
			$dateOfBirth=$row->dateOfBirth;
			$email=$row->email;
			$phoneNumber=$row->phoneNumber;
			switch ($row->gender) {
				case "M":
					$gender="M";
					break;
				case "F":
					$gender="F";
					break;
				case "O":
					$gender="O";
					break;
			}

			$this->pdf->Row(array(
				$indice,
				$username,
				$name,
				$id,
				$dateOfBirth,
				$email,
				$phoneNumber,
				$gender
			));
			$indice++;
		}
		$this->pdf->Output('usuariosHabilitados.pdf','I');
    }

    public function ListaUsuariosDeshabilitados() {
    	$this->load->library('pdf_mc'); //cargar la libreria pdf
		$this->eliminarSesiones();
    	$lista = $this->user_model->selectUser(0);
    	$totalUser = $this->user_model->selectTotalUser();
    	$totalDisabledUser = $this->user_model->selectDisabledUser(0);
    	$lista=$lista->result();
    	$selectDisabledUserByMonth = $this->user_model->selectDisabledUserByMonth();
    	$selectDisabledUserByMonth=$selectDisabledUserByMonth->result();
    	$this->pdf=new pdf_mc('L','mm','A4');
    	$this->pdf->AddPage(); //agregamos una pagina
		$this->pdf->AliasNbPages(); //identificador que imprime pagina n de n
		$this->pdf->SetTitle("Usuarios Inhabilitados"); //titulo docuemento
		$this->pdf->SetLeftMargin(1);
		$this->pdf->SetRightMargin(1);
		$this->pdf->SetFillColor(210,210,210); //color de fondo
		$this->pdf->SetFont("Arial","B",11);
		$this->pdf->Cell(30); //celdas (ancho)
		$this->pdf->Cell(200,15,utf8_decode('USUARIOS INHABILITADOS'),0,0,'C',1); //ancho, alto, texto,borde,comienzoLinea,linea,align,fondo
		$this->pdf->Ln(30); //salto de linea

		$this->pdf->SetWidths(Array(50,60));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(50,10,utf8_decode('TOTAL DE USUARIOS'),'TBLR',0,'C',1);
		$this->pdf->Cell(60,10,utf8_decode('USUARIOS DESHABILITADOS'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		$this->pdf->RowDatos(array(
				$totalUser,
				$totalDisabledUser
		));
		$this->pdf->Ln(15); //salto de linea

		$this->pdf->SetWidths(Array(30,50));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(30,10,utf8_decode('Mes'),'TBLR',0,'C',1);
		$this->pdf->Cell(50,10,utf8_decode('Usuarios deshabilitados'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		foreach ($selectDisabledUserByMonth as $row) 
		{
			$monthDU=$row->monthDU;
			$totalUsersDisabled=$row->totalUsersDisabled;

			$this->pdf->RowDatos(array(
				$monthDU,
				$totalUsersDisabled
			));
		}
		$this->pdf->Ln(20);

		$this->pdf->SetWidths(Array(10,30,90,20,28,85,20,12));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(10,10,utf8_decode('#'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Usuario'),'TBLR',0,'C',1);
		$this->pdf->Cell(90,10,utf8_decode('Nombre Completo'),'TBLR',0,'C',1);
		$this->pdf->Cell(20,10,utf8_decode('C.I'),'TBLR',0,'C',1);
		$this->pdf->Cell(28,10,utf8_decode('Fecha de Baja'),'TBLR',0,'C',1);
		$this->pdf->Cell(85,10,utf8_decode('Correo Electrónico'),'TBLR',0,'C',1);
		$this->pdf->Cell(20,10,utf8_decode('Teléfono'),'TBLR',0,'C',1);
		$this->pdf->Cell(12,10,utf8_decode('Sexo'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		
		$indice = 1;
		foreach ($lista as $row) 
		{
			$username=$row->username;
			$name=$row->name.' '.$row->firstSurname.' '.$row->secondSurname;
			$id=$row->id;
			$dateOfDown=$row->lastUpdate;
			$email=$row->email;
			$phoneNumber=$row->phoneNumber;
			switch ($row->gender) {
				case "M":
					$gender="M";
					break;
				case "F":
					$gender="F";
					break;
				case "O":
					$gender="O";
					break;
			}

			$this->pdf->Row(array(
				$indice,
				$username,
				$name,
				$id,
				$dateOfDown,
				$email,
				$phoneNumber,
				$gender
			));
			$indice++;
		}
		$this->pdf->Output('usuariosInhabilitados.pdf','I');
    }

    public function ListaPedidosHabilitados() {
		$this->load->library('pdf_mc'); //cargar la libreria pdf
		$this->eliminarSesiones();
    	$lista = $this->user_model->selectUserOrder();
    	$totalOrders1 = $this->user_model->selectTotalOrders();
    	$totalQuantityProduct = $this->user_model->selectTotalQuantityProduct();
    	$selectTotalValueOrders = $this->user_model->selectTotalValueOrders();
    	$lista=$lista->result();
    	$selectUserOrderByMonth = $this->user_model->selectUserOrderByMonth();
    	$selectUserOrderByMonth = $selectUserOrderByMonth->result();
    	$this->pdf=new pdf_mc('L','mm','A4');
    	$this->pdf->AddPage(); //agregamos una pagina
		$this->pdf->AliasNbPages(); //identificador que imprime pagina n de n
		$this->pdf->SetTitle("Lista Pedidos de Usuarios Habilitados"); //titulo docuemento
		$this->pdf->SetLeftMargin(5);
		$this->pdf->SetRightMargin(5);
		$this->pdf->SetFillColor(210,210,210); //color de fondo
		$this->pdf->SetFont("Arial","B",11);
		$this->pdf->Cell(30); //celdas (ancho)
		$this->pdf->Cell(200,15,utf8_decode('USUARIOS/PEDIDOS'),0,0,'C',1); //ancho, alto, texto,borde,comienzoLinea,linea,align,fondo
		$this->pdf->Ln(30); //salto de linea

		$this->pdf->SetWidths(Array(30,50,75,55));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(30,10,utf8_decode('Mes'),'TBLR',0,'C',1);
		$this->pdf->Cell(50,10,utf8_decode('Total de Pedidos'),'TBLR',0,'C',1);
		$this->pdf->Cell(75,10,utf8_decode('Total de Productos en Pedidos'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Total de Pedidos (Bs.)'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		
		foreach ($selectUserOrderByMonth as $row) 
		{
			$mes=$row->mes;
			$totalOrders=$row->totalOrders;
			$totalQuantity=$row->totalQuantity;
			$totalMoney=$row->totalMoney;

			$this->pdf->RowDatos(array(
				$mes,
				$totalOrders,
				$totalQuantity,
				$totalMoney
			));
		}
		$this->pdf->Ln(5);
		$this->pdf->SetWidths(Array(30,50,75,55)); //total 
		$this->pdf->SetLineHeight(6);
		$this->pdf->Ln(10);
		$nada = 'Total';
		$this->pdf->RowDatos(array(
				$nada,
				$totalOrders1,
				$totalQuantityProduct,
				$selectTotalValueOrders
		));

		$this->pdf->Ln(20); //salto de linea

		$this->pdf->SetWidths(Array(10,30,90,57,55,45));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(10,10,utf8_decode('#'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Usuario'),'TBLR',0,'C',1);
		$this->pdf->Cell(90,10,utf8_decode('Email'),'TBLR',0,'C',1);
		$this->pdf->Cell(57,10,utf8_decode('Pedidos Totales Realizados'),'TBLR',0,'C',1);
		$this->pdf->Cell(55,10,utf8_decode('Cantidad total de Productos'),'TBLR',0,'C',1);
		$this->pdf->Cell(45,10,utf8_decode('Total de Pedidos (Bs.)'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);
		
		$indice = 1;
		foreach ($lista as $row) 
		{
			$username=$row->Dueño;
			$email=$row->Email;
			$totalPedidos=$row->totalPedidos;
			$cantidadTotalPedidos=$row->cantidadTotalPedidos;
			$totalVendido=$row->totalVendido;

			$this->pdf->Row(array(
				$indice,
				$username,
				$email,
				$totalPedidos,
				$cantidadTotalPedidos,
				$totalVendido
			));
			$indice++;
		}
		$this->pdf->Output('listaUsuariosPedidosHabilitados.pdf','I');
	}

	public function DisableUser() {
		$this->eliminarSesiones();
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$data['status'] = 0;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$this->user_model->deleteUser($_POST['UserID'], $data);
		redirect('Usuario/UsuariosHabilitados','refresh');
	}

	public function UsuariosDeshabilitados() {
		$this->eliminarSesiones();
		$userList = $this->user_model->selectUser(0);
		$totalUser = $this->user_model->selectTotalUser();
		$totalDisabledUser = $this->user_model->selectDisabledUser(0);
		$data['userList'] = $userList;
		$data['totalUser'] = $totalUser;
		$data['totalDisabledUser'] = $totalDisabledUser;
		if($_SESSION['userSesion'] !== null) {
			$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$data['sendUserRole'] = 0; 
		}
		$this->load->view('list_of_disabled_users_view', $data);
	}

	public function EnableUser() {
		$this->eliminarSesiones();
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$data['status'] = 1;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$this->user_model->deleteUser($_POST['UserID'], $data);
		redirect('Usuario/UsuariosDeshabilitados','refresh');
	}

	public function Usuario() {
		$this->eliminarSesiones();
		$UserID=$_GET['id'];
		$user_from_user=$this->user_model->getUser($UserID);
		$stars_from_user=$this->user_model->getStarsFromUser($_SESSION['userSesion'], $UserID);
		$avg_stars_from_user = $this->user_model->getAverageStarsFromUser($UserID);
		$products_from_user=$this->user_model->getProductsFromUser($UserID);
		$stands_from_user=$this->user_model->getStandsFromUser($UserID);
		$findUser = $this->user_model->findUser($UserID);
		$data['products']=$products_from_user;
		$data['stands']=$stands_from_user;
		$data['userInfo']=$user_from_user;
		$data['userStarInfo']=$stars_from_user;	
		$data['userAvgStarInfo']=$avg_stars_from_user;
		if($findUser->num_rows() > 0){ //if user exists in database
			if($_SESSION['userSesion'] !== null) {
				$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$data['sendUserRole'] = 0; 
			}
			$this->load->view('public_profile_user_view',$data);
		}
		else{
			if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$dataHead['sendUserRole'] = 0; 
			}
			$dataHead['headTitle'] = "Error, Usuario No Existe"; 

			$this->load->view('headFooter/head', $dataHead);

			$sendError['sendError'] = "No existe este usuario!";
			$this->load->view('error_404_not_found_view', $sendError);
			$this->load->view('headFooter/footer');
		}
	}

	public function ProductAddFavorite(){
		$this->eliminarSesiones();
    	date_default_timezone_set("America/La_Paz");
		$data['userId']=$_SESSION['userSesion'];
    	$data['productId'] = $_POST['productId'];
    	$data['favoriteDate'] = date('Y-m-d H:i:s');
    	$validate=$this->user_model->ValidateProductAddFavorites($data);
    	if($validate->num_rows()>0){
    		$this->user_model->ProductDeleteFavorites($data);
    	}
    	else {
    		$this->user_model->ProductAddFavorites($data);
    	}
    	$productIdDB = $this->product_model->getProduct($_POST['productId']);
    	foreach ($productIdDB->result() as $row) {
    		$productId = $row->encryptedProductId;
    	}
    	redirect('Producto/Producto?id='.$productId);
    }

    public function ProductDeleteFavorite(){
		$this->eliminarSesiones();
		$data['userId']=$_POST['userId'];
    	$data['productId'] = $_POST['productId'];
		$this->user_model->ProductDeleteFavorites($data);
		redirect('Usuario/ProductAddFavorites');

    }

	public function UsernameExists() {
		$this->eliminarSesiones();
		$query=$this->user_model->validateUsername($_POST['username']);
		if($query->num_rows()==0)
		{
			return "0";		
		}
		return "1";
	}

	public function RatingUser() {
		$this->eliminarSesiones();
		$userRatingId = $_SESSION['userSesion']; //2
		$userRatedId = $_POST['postid']; //1
		$rating = $_POST['rating']; //num of stars
		echo $this->user_model->rateUser($userRatingId, $userRatedId, $rating);
	}

	public function RecuperarContrasena() {
		$this->eliminarSesiones();
		$data['msg'] = $this->uri->segment(3);
		$this->load->view('recover_password_view',$data);
	}

	public function ResetPassword() {
		$this->eliminarSesiones();
		$data=[];
		$token = $_GET['token'];
		if(!empty($token)){
			$query=$this->user_model->verifyToken($token);
			if($query->num_rows()==0) {
				$data['error'] = "Usuario no encontrado";
			}
		}
		else {
			$data['error'] = "Lo siento :(. Acceso no autorizado";
		}
		$this->load->view('reset_password_view', $data);
	}

	public function ResetNewPassword() {
		$this->eliminarSesiones();
		date_default_timezone_set("America/La_Paz"); //TO-DO: Refactor :)
		$UserID = $_POST['token'];
		$data['password']=md5($_POST['newPassword2']);
		$data['lastUpdate']=date('Y-m-d H:i:s');
		$this->user_model->updatePasswordRecover($UserID,$data);
		echo'<div class="ui positive message">
				<i class="close icon"></i>
				<p>Nueva contraseña establecida.</p>
			 </div>';
		$data['msg'] = $this->uri->segment(3);
		$this->load->view('login_view', $data);
	}

	public function Enviar() {
		$this->eliminarSesiones();
		require 'PHPMailer/PHPMailerAutoload.php';

		//Create a new PHPMailer instance
		$mail = new PHPMailer();
		$mail->IsSMTP();
		 
		//Configuracion servidor mail
		$mail->From = "tuCasero2020@gmail.com"; //remitente
		$mail->FromName = 'TuCasero.com';
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls'; //seguridad
		$mail->Host = "smtp.gmail.com"; // servidor smtp
		$mail->Port = 587; //puerto
		$mail->Username ='tuCasero2020@gmail.com'; //nombre usuario
		$mail->Password = 'miCasero20'; //contraseña

		$emailRecover = $_POST['emailRecover'];
		$query=$this->user_model->getUserFromEmail($emailRecover);
		if($query->num_rows()>0) {
			//Agregar destinatario
			foreach($query->result() as $row) {
				$token = $row->encryptedUserId;
				$mail->AddAddress($emailRecover);
				$mail->Subject = "Restablecimiento de Contraseña";
				$mail->isHTML(true);
				$mail->Body = 'Por favor haga click en el enlace a continuación para completar el proceso.<br><br>'
								. '<a href="'.base_url().'Usuario/ResetPassword?token='.$token.'">Restablecer Contraseña</a>';
				if ($mail->Send()) {
					echo'<div class="ui message">
							<i class="close icon"></i>
							<p>En unos instantes recibirá un enlace de recuperación de contraseña en su dirección de correo electrónico.</p>
						</div>';
					$data['msg'] = $this->uri->segment(3);
					$this->load->view('login_view', $data);
				} else {
					echo'<script type="text/javascript">
							alert("NO ENVIADO, intentar de nuevo");
						</script>';
				}
			}
		}
		else {
			redirect('Usuario/RecuperarContrasena/1','refresh');
		}
	}

	public function premium() {
		if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0; 
		}
		$dataHead['headTitle'] = "Volverse Premium"; 
		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('premium_user_view'); //, $data);
		$this->load->view('headFooter/footer');
	}

	public function becomingPremium(){
		$data['premiumUserId'] = $_SESSION['userSesion'];
		$data['cardNumber'] = $_POST['cardNumber'];
		$data['expiryMonth'] = $_POST['expiryMonth']."-01";
		$data['cvCode'] = md5($_POST['cvCode']);
		$this->user_model->registerPremiumUser($data);
		$_SESSION['isPremium']= 1;
		redirect('Producto/Search','refresh');
	}
}