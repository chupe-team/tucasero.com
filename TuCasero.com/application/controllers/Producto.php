<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('product_model'); 
	}

	public function index() {
		$this->load->view('_view');
	}

	public function Producto() {
		$productId = $_GET['id'];
		$productInfo1 = $this->product_model->getProductFromProduct($productId);
		$productInfo2 = $this->product_model->getStandFromProduct($productId);
		$productInfo3 = $this->product_model->getUserFromProduct($productId);
		$findProduct = $this->product_model->findProduct($productId);
		$data['productInfo1']=$productInfo1;
		$data['productInfo2']=$productInfo2;
		$data['productInfo3']=$productInfo3;
		$data['productId']=$productId;
		if($findProduct->num_rows() > 0) { //if product exists in database
			if($_SESSION['userSesion'] !== null) {
				$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$data['sendUserRole'] = 0; 
			}
			$this->load->view('public_profile_product_view', $data);
		}
		else {
			if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$dataHead['sendUserRole'] = 0; 
			}
			$dataHead['headTitle'] = "Error, Producto No Existe"; 

			$this->load->view('headFooter/head', $dataHead);
			$sendError['sendError'] = "No existe este producto!";
			$this->load->view('error_404_not_found_view', $sendError);
			$this->load->view('headFooter/footer');
		}
	}

	public function RegistrarProducto() {
		$standId = $_POST['standId'];
		$data['standId'] = $standId;
		$numStandProducts = $this->stand_model->getNumStandProducts($standId);
		$maxNumStandProducts = $this->stand_model->getMaxNumStandProducts($standId);
		$data['getCategories'] = $this->product_model->getCategories();
		if($numStandProducts < $maxNumStandProducts) {
			if($_SESSION['userSesion'] !== null) {
				$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
			} else {
				$data['sendUserRole'] = 0; 
			}
			$this->load->view('register_product_view', $data);
		}
		else{
			$encryptedStandId = $this->stand_model->getStand($standId);
			foreach ($encryptedStandId->result() as $row) {
				redirect('Stand/Stand?id='.$row->encryptedStandId,'refresh');
			}
		}
	}

	public function RegisterProduct() {
		$data['productName']=$_POST['title'];
		$data['cost']=$_POST['costNumber'];
		$data['quantity']=$_POST['quantity'];
		$data['description']=$_POST['description'];
		$data['standId']=$_POST['standId'];
		$data['categoryId']=$_POST['category'];
		$tags=$_POST['tags'];
		$productId = $this->product_model->insertProduct($data, $tags);
		$idProduct = $this->product_model->getProductId($productId."");
		mkdir('images/TuCaseroImages/products/'.$idProduct, 0777);
		copy($_FILES['upload']['tmp_name'],'images/TuCaseroImages/products/'.$idProduct.'/'.$idProduct.'.jpg');
		redirect('Producto/Producto?id='.$productId,'refresh');
	}

	/*public function SelectProducts()
	{
		$this->product_model->SelectProducts();
	}*/

	/*public function UpdateProduct($ProductID){
		date_default_timezone_set("America/La_Paz");
		$data['name']=$_POST['name'];
		$data['description']=$_POST['description'];
		$data['quantity']=$_POST['quantity'];
		$data['cost']=$_POST['cost'];
		$data['lastUpdate']=date('Y-m-d H:i:s');
		$this->product_model->updateProduct($ProductID,$data);
		redirect('stand/index','refresh');
	}*/
	public function ActualizarProducto() {
		$string='';
		if(!isset($_POST['productIdU'])){
    		redirect('Usuario/TuZona', 'refresh');
		}
		$productId = $_POST['productIdU'];
		$data['productId'] = $productId;
		$data['productInfo'] = $this->product_model->getProduct($productId);
		$data['getCategories'] = $this->product_model->getCategories();
		$data['tags'] = $this->product_model->getTagsProduct($productId);
		if($_SESSION['userSesion'] !== null) {
			$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$data['sendUserRole'] = 0; 
		}
		$this->load->view('update_product_view',$data);
	}

	public function UpdateProduct() {
		date_default_timezone_set("America/La_Paz");
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$data['productName']=$_POST['title'];
		$data['cost']=$_POST['costNumber'];
		$data['quantity']=$_POST['quantity'];
		$data['description']=$_POST['description'];
		$data['standId']=$_POST['standId'];
		$data['categoryId']=$_POST['category'];
		$tags=$_POST['tags'];
		$productId = $_POST['productId'];

		$tagsFromDB = array();
		$tagsFromProduct = $this->product_model->getTagsProduct($productId);
		foreach($tagsFromProduct->result() as $row) {
			$tagsFromDB[] = $row->tagName;
		}

		$encryptedProductId = $this->product_model->updateProduct($productId, $data, $tags, $tagsFromDB);
		
		/*unlink('images/TuCaseroImages/products/'.$productId.'/'.$productId.'.jpg');
		copy($_FILES['upload']['tmp_name'],'images/TuCaseroImages/products/'.$productId.'/'.$productId.'.jpg');*/
		if($_FILES['upload']['tmp_name'])
		{
			unlink('images/TuCaseroImages/products/'.$productId.'/'.$productId.'.jpg');
			copy($_FILES['upload']['tmp_name'],'images/TuCaseroImages/products/'.$productId.'/'.$productId.'.jpg');
		}	
    	redirect('Producto/Producto?id='.$encryptedProductId,'refresh');
	}

	/*esto es del drogo
	public function updateProduct($ProductID, $data,$tags) {
		$this->db->where('productID', $ProductID);
		$this->db->update('product', $data);


		$this->db->trans_begin();
		
		for ($i=0; $i < count($tags); $i++) {
			$labelName = $tags[$i];
			$search=$this->db->query("SELECT labelId FROM label WHERE nameLabel='$labelName'");
			if ($search->num_rows() == 0) {
				$label['nameLabel'] = $tags[$i];
				$this->db->insert('label',$label);
				$ID2 = $this->db->query("SELECT ifnull(MAX(labelId),0) AS LabelMaxID FROM label");
				foreach ($ID2->result() as $row)
				{
					$IDL = $row->LabelMaxID;
				}
				$insertLP['labelId']=$IDL;
				$insertLP['productId']=$IDP;
				$this->db->insert('product_label',$insertLP);
			}
			else
			{
				foreach ($search->result() as $row)
				{
					$labelId1 = $row->labelId;
				}
				$IDS = $this->db->query("SELECT labelId,productID AS LabelMaxID FROM product_label WHERE labelId='$labelId1' AND '$ProductID'");
				if ($IDS->num_rows() > 0) {
					$insertLP['labelId']=$labelId1;
					$insertLP['productId']=$IDP;
					$this->db->insert('product_label',$insertLP);
				}	
			}
		}
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
	}*/

    public function DeleteProduct() {
		date_default_timezone_set("America/La_Paz");
		$ProductIDD = $_POST['productIdD'];
		$StandIDD = $_POST['standIdD'];
		$data['status'] = 0;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$encryptedProductId = $this->product_model->deleteProduct($ProductIDD,$data);
		redirect('Stand/Stand?id='.$encryptedProductId,'refresh');
	}

	public function DeleteProduct2() {
		date_default_timezone_set("America/La_Paz");
		$ProductIDD = $_POST['productIdD'];
		$data['status'] = 0;
		$data['lastUpdate'] = date('Y-m-d H:i:s');
		$this->product_model->deleteProduct2($ProductIDD,$data);
		redirect('Producto/ListaProductos','refresh');
	}

	public function cancelarPedido() {
		$orderId = $_POST['ordenId'];
		$data['status'] = 0;
		$this->product_model->cancelarOrden($orderId, $data);
		redirect('Usuario/TuZona','refresh');
	}

	public function Search(){		
		$data["tags"]=$this->product_model->getTags();	
		$data["categories"]=$this->product_model->getCategories();
		if(isset($_GET['textoBusqueda'])){
			$data['textoBusqueda'] = $_GET['textoBusqueda'];
		} else {
			$data['textoBusqueda'] = "";
		}
		$variable_para_saber_que_cosa_buscar=0;
		if(isset($_GET['categoria']))
		{
			$cat=$_GET['categoria'];
			if ($cat!='todas las categorías') {
				$data['categoria']=$cat;
				$variable_para_saber_que_cosa_buscar=$variable_para_saber_que_cosa_buscar+3;
			}
		}
		if (isset($_GET['checkDesc'])) {
			$variable_para_saber_que_cosa_buscar=$variable_para_saber_que_cosa_buscar+4;
		}
				$data["limit_per_page"]=6;
				$config['base_url'] = base_url() . 'Producto/Search';
           		
            	$config['per_page'] = $data["limit_per_page"];
				$config["uri_segment"] = 3;       
				$config['num_links'] = 3;
				$config['use_page_numbers'] = TRUE;
				$config['reuse_query_string'] = TRUE;
				
				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<li><a href="javascript:void(0);">';
				$config['cur_tag_close'] = '</a></li>';
				$config['next_link'] = 'Next';
				$config['prev_link'] = 'Prev';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>'; 
				$page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
				$offset = $page;
				$data["start_index"] = $offset*$data['limit_per_page'];

		switch ($variable_para_saber_que_cosa_buscar) {
			case 3:
				$config['total_rows'] = $this->product_model->cantSearchWithCategory($data)->num_rows();
				
				$data['productos']=$this->product_model->SearchWithCategory($data);
				$this->pagination->initialize($config);
				break;
			case 4:
				$config['total_rows'] = $this->product_model->cantSearchWithDescription($data)->num_rows();
				
				$data['productos']=$this->product_model->SearchWithDescription($data);
				$this->pagination->initialize($config);
				break;
			case 7:
				$config['total_rows'] = $this->product_model->cantSearchWithCategoryDescription($data)->num_rows();
				
				$data['productos']=$this->product_model->SearchWithCategoryDescription($data);
				$this->pagination->initialize($config);
				break;			
			default:
				$config['total_rows'] = $this->product_model->cantSearch($data)->num_rows();
				$data['productos']=$this->product_model->Search($data);    
           		$this->pagination->initialize($config);
            // build paging links
				$data["links"] = $this->pagination->create_links();
		}
		if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0; 
		}
		if(isset($_GET['textoBusqueda'])){
			if($_GET['textoBusqueda'] == "") $dataHead['headTitle'] = "Buscando";
			else $dataHead['headTitle'] = 'Buscando "'.$_GET['textoBusqueda'].'"';
		}
		else{
			$dataHead['headTitle'] = 'Inicio';
		}
		

		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('search_product_view',$data);
		$this->load->view('headFooter/footer');

		
	}
	public function carrito(){ //metodo para añadir objetos al carrito
		$data=$this->input->post();

		$this->product_model->AddProductToCart($data);

		echo 'se añadio el producto con el idproducto: '.$data['productId'];
		echo ' se añadio el producto con el id usuario: '.$data['userId'];
		echo ' se añadio el producto con el cantidad: '.$data['quantity'];
		echo ' se añadio el producto con la imagen: '.$data['image'];
			
	}
	public function mostrarCarrito(){
		$data["userId"]=$_SESSION['userSesion'];
		$items=$this->product_model->showCart($data);
		foreach($items->result() as $item) {
			echo '<div class="row">';
				echo '<div class="col-md-5">';
					echo '<img class="imgCarrito" src="'.$item->image.' " ></div>';
							echo '<div class="col col-md-6 tituloCarrito">';
								echo '<b>'.$item->productName.'</b>';
								echo '<p>'.$item->quantity.'<b> U.</b></p>';
								echo '<p>'.$item->cost.'<b> Bs.</b></p>';
							echo '</div>';
			echo '</div>';
		}
	}
	
	public function vaciarCarrito(){ 
		$data["userId"]=$_SESSION['userSesion'];
		$this->product_model->destroyCart($data);
		echo "carrito vaciado";
	}

	public function visitarCarrito(){
		if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0; 
		}
		$dataHead['headTitle'] = "Carrito de Compras"; 
		$data['userId']=$_SESSION['userSesion'];
		$data['productos']=$this->product_model->showCart($data);
		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('visitarCarrito',$data);
		$this->load->view('headFooter/footer');
	}
	public function quitarItemCarrito(){
		$data['cartId']=$_POST['idItemCarrito'];
		$this->product_model->deleteItemCart($data);

		redirect('Producto/visitarCarrito','refresh');
		
	}
	public function PeticionProducto(){
		$data["userId"]=$_POST["idUsuario"];
		$data["productId"]=$_POST["idProducto"];
		$data["message"]=$_POST["message"];
		$data["quantity"]=$_POST["cantidad"];
		$this->product_model->insertarPedido($data);
		$datarow['cartId']=$_POST['idItemCarrito'];
		$this->product_model->deleteItemCart($datarow);

		redirect('Producto/visitarCarrito','refresh');
	}
 	// aqui termina con la insercion del producto del carrito a la base de datos
 
	public function ListaProductos() {
		$lista = $this->product_model->getProduct2(1);
		$data['productList'] = $lista;
		if($_SESSION['userSesion'] !== null) {
			$data['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$data['sendUserRole'] = 0; 
		}
		$this->load->view('list_of_products_view',$data);
	}

	public function ListaProductosPDF() {
    	$this->load->library('pdf_mc'); //cargar la libreria pdf
    	$lista = $this->product_model->getProduct2(1);
    	$lista=$lista->result();
    	$selectProductsByMonths = $this->product_model->selectProductsByMonths();
    	$selectProductsByMonths=$selectProductsByMonths->result();
    	$this->pdf=new pdf_mc('L','mm','A4');
    	$this->pdf->AddPage(); //agregamos una pagina
		$this->pdf->AliasNbPages(); //identificador que imprime pagina n de n
		$this->pdf->SetTitle("Productos"); //titulo docuemento
		$this->pdf->SetLeftMargin(2);
		$this->pdf->SetRightMargin(2);
		$this->pdf->SetFillColor(210,210,210); //color de fondo
		$this->pdf->SetFont("Arial","B",10);
		$this->pdf->Cell(30); //celdas (ancho)
		$this->pdf->Cell(200,15,utf8_decode('PRODUCTOS'),0,0,'C',1); //ancho, alto, texto,borde,comienzoLinea,linea,align,fondo
		$this->pdf->Ln(30); //salto de linea

		$this->pdf->SetWidths(Array(30,50));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(30,10,utf8_decode('Mes'),'TBLR',0,'C',1);
		$this->pdf->Cell(50,10,utf8_decode('Productos Registrados'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);

		foreach ($selectProductsByMonths as $row) 
		{
			$mesP=$row->mesP;
			$totalProducts=$row->totalProducts;

			$this->pdf->RowDatos(array(
				$mesP,
				$totalProducts
			));
		}
		$this->pdf->Ln(15);

		$this->pdf->SetWidths(Array(7,35,150,30,30,40));
		$this->pdf->SetLineHeight(6);
		$this->pdf->Cell(7,10,utf8_decode('#'),'TBLR',0,'C',1);
		$this->pdf->Cell(35,10,utf8_decode('Nombre'),'TBLR',0,'C',1);
		$this->pdf->Cell(150,10,utf8_decode('Descripción'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Añadido en'),'TBLR',0,'C',1);
		$this->pdf->Cell(30,10,utf8_decode('Actualizado en'),'TBLR',0,'C',1);
		$this->pdf->Cell(40,10,utf8_decode('Categoria'),'TBLR',0,'C',1);
		$this->pdf->Ln(10);

		
		$indice = 1;
		foreach ($lista as $row) 
		{
			$productName=$row->productName;
			$description=$row->description;
			$lastUpdate=$row->lastUpdate;
			$createDate=$row->createDate;
			switch ($row->categoryId) {
				case "1":
					$category="Salud";
					break;
				case "2":
					$category="Comida";
					break;
				case "3":
					$category="Móviles y telefonía";
					break;
				case "4":
					$category="Informática y electrónica";
					break;
				case "5":
					$category="Deporte y Ocio";
					break;
				case "6":
					$category="Bicicletas";
					break;
				case "7":
					$category="Consolas y Videojuegos";
					break;
				case "8":
					$category="Hogar y Jardín";
					break;
				case "9":
					$category="Electrodomésticos";
					break;
				case "10":
					$category="Cine, Libros y Música";
					break;
				case "11":
					$category="TV, Audio y Foto";
					break;
			}
			$this->pdf->Row(array(
				$indice,
				$productName,
				$description,
				$createDate,
				$lastUpdate,
				$category
			));
			$indice++;
		}
		$this->pdf->Output('productos.pdf','I');
	}
	

	public function aceptarPedido() {
		$standId = $_POST['standId'];
		$orderId = $_POST['orderID'];
		$data['is_checked'] = 1;
		$this->product_model->updateStatus($orderId, $data);
		$this->EnviarComfirmacion($orderId, $standId);
		
	}

	
	public function EnviarCancelacion($orderId, $standEncryptedId) 
	{
		require 'PHPMailer/PHPMailerAutoload.php';
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->From = "tuCasero2020@gmail.com";
		$mail->FromName = 'TuCasero.com';
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls'; 
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->Username ='tuCasero2020@gmail.com';
		$mail->Password = 'miCasero20';
		$query1=$this->user_model->getDataForCorrectEmailbyIdOrderId($orderId);
		if($query1->num_rows()>0) 
		{
			foreach($query1->result() as $row) {
				$mail->AddAddress($row->email);
				$mail->Subject = "Pedido Cancelado";
				$mail->isHTML(true);
				$mail->Body = 'Hola '.$row->username.' el stand " '.$row->standName.' " cancelo su pedido:" '.$row->productName.' " con el mensaje:" '.$row->orderMessage.' " puedes ver mas aqui: <a href="'.base_url().'index.php">Volver A La Pagina</a>';
				if ($mail->Send()) {
					redirect('stand/stand?id='.$standEncryptedId);
				} else {
					echo'<script type="text/javascript">
							alert("NO ENVIADO, intentar de nuevo");
						</script>';
				}
			}
		}
	}

	public function EnviarComfirmacion($orderId, $standEncryptedId) 
	{
		require 'PHPMailer/PHPMailerAutoload.php';
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->From = "tuCasero2020@gmail.com";
		$mail->FromName = 'TuCasero.com';
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls'; 
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->Username ='tuCasero2020@gmail.com';
		$mail->Password = 'miCasero20';
		$query1=$this->user_model->getDataForCorrectEmailbyIdOrderId($orderId);
		if($query1->num_rows()>0) 
		{
			foreach($query1->result() as $row) {
				$encargados="";
				$idStand = $row->standId;
				$email = $row->email;
				$user = $row->username;
				$stand= $row->standName;
				$productName = $row->productName;
				$order = $row->orderMessage;
				$query2=$this->user_model->encargadosByStandid($idStand);
				if($query2->num_rows()>0) 
				{
					foreach($query2->result() as $row)
					{
						$encargados = $encargados.'Nombre Completo: '.$row->fullname.', ';
						$encargados = $encargados.'Username: '.$row->user.', ';
						$encargados = $encargados.'Email: '.$row->email.', ';
						$encargados = $encargados.'Numero De Celular: '.$row->phoneNumber.'.<br>';
					}
				}
						
				date_default_timezone_set("America/La_Paz");
				$mail->AddAddress($email);
				$mail->Subject = "Pedido Aceptado";
				$mail->isHTML(true);
				$mail->Body = 'Hola '.$user.' el stand " '.$stand.' " acepto su pedido:" '.$productName.' " con el mensaje:" '.$order.' " puedes comunicarte con los encargados desde sus respectivas redes:<br><br>'
				.'Encargados en la fecha: '.date('Y-m-d H:i:s').'<br><br>'
				.'	'.$encargados;

				if ($mail->Send()) {
					redirect('stand/stand?id='.$standEncryptedId);
				} else {
					echo'<script type="text/javascript">
							alert("NO ENVIADO, intentar de nuevo");
						</script>';
				}
			}
		}
	}
	public function denegarPedido() {
		$standId = $_POST['standId'];
		$orderId = $_POST['orderID'];
		$data['is_checked'] = 2;
		$this->product_model->updateStatus($orderId, $data);
		$this->EnviarCancelacion($orderId, $standId);
	}



}