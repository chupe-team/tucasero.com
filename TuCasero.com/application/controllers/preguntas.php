<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preguntas extends CI_Controller {
	public function index() {
		if($_SESSION['userSesion'] !== null) {
			$dataHead['sendUserRole'] = $this->user_model->getUserRole($_SESSION['userSesion']);
		} else {
			$dataHead['sendUserRole'] = 0; 
		}
		$dataHead['headTitle'] = "Preguntas Frecuentes"; 

		$this->load->view('headFooter/head', $dataHead);
		$this->load->view('frequently_asked_questions_view');
		$this->load->view('headFooter/footer');
	}
}