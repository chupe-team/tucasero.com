<div id="notfound">
	<div class="notfound">
		<div class="notfound-404">
			<h1>Oops!</h1>
			<h2>404 - <?php echo $sendError; ?></h2>
		</div>
		<a id="goBackToMainPageFromErrorPage" href="<?php echo site_url('Producto/Search') ?>">VOLVER A INICIO</a>
	</div>
</div>