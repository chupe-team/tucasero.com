<div class="container">
  <div class="row" style="margin-bottom: 40px;">
    <div class="col-md-12">
      <?php
      $atributos=array('method'=>'get');
       echo form_open_multipart('Producto/Search',$atributos) ?>
          <div class="row"> 
            <div class="col-md-3">
            </div>
          <input type="text" class="form-control" id="textoBusqueda" name="textoBusqueda" style="width: 50%; margin-right:20px;">
          <input type="submit" class="btn btn-secondary" value="Buscar">
          </div> 
      <?php echo form_close(); ?>
    </div>
  </div>
    <div class="row">
      <?php foreach ($products->result() as $row) { ?>   
        <div class="col-md-3" >
          <div class="ui link cards" style="width:100%;"  >
          <a href="<?php echo site_url('Producto/Producto?id='.$row->encryptedProductId);?>" class="card">
							<img class="card-img-top" src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" height="235" width="100%" alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title"><?php echo $row->productName  ?></h5>
								<p class="card-text"><?php echo $row->cost; ?> bs.</p>
                <p class="card-text"><?php echo $row->quantity; ?> u.</p>
							</div>
						</a>
            </br>
          </div>
        </div>
      <?php } ?> 
    </div>
</div>