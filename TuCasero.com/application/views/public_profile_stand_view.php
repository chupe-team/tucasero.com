<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php foreach ($standInfo1->result() as $row) { ?>
	<title><?php echo $row->standName; ?> | TuCasero</title>
	<?php } ?>
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/publicProfileStandStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/styleHead.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/footerStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/carritoStyle.css">
</head>
<body>
<nav class="menu" id="menu">
		<div class="contenedor contenedor-botones-menu">
			<button id="btn-menu-barras" class="btn-menu-barras"><i class="fas fa-bars"></i></button>
			<button id="btn-menu-cerrar" class="btn-menu-cerrar"><i class="fas fa-times"></i></button>
		</div>
		<div class="contenedor contenedor-enlaces-nav">
			<a href="<?php echo site_url('Producto/Search') ?>">
				<img id="logoBienChevere" height="60px" src="<?php echo base_url(); ?>/images/logo3.png">
			</a>
			<div class="btn-departamentos" id="btn-departamentos">
				<p>Todas las <span>Categorias</span></p>
				<i class="fas fa-caret-down"></i>
			</div>

			<div class="enlaces ml-auto">
				<?php if($_SESSION['userSesion']) { ?>
					<input type="hidden" id="MostrarCarrito" value="<?php echo base_url();?>index.php/Producto/mostrarCarrito">     		
						<a class="dropdown-toggle" id="carritoMostrarDatos"  data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="true"><i id="dropdownCarrito" class="fas fa-cart-plus" style="color: yellow;" ></i></a>
						<div class="dropdown-menu" aria-labelledby="dropdownCarrito" id="resultado">
							<div class="contenedorCarrito" id="contenedorCarrito">
														
							</div>
							<div class="botones-carrito" style="text-align: center;">
								<a class="btn btn-danger btn-lg" href="" id="vaciarCarrito">vaciar</a>
								<input id="direccionVaciarCarrito" type="hidden" value="<?php echo base_url()?>index.php/Producto/vaciarCarrito">
								<a class="btn btn-success btn-lg active"  href="<?php echo base_url(); ?>index.php/Producto/visitarCarrito">ir de compras</a>
							</div>
						</div>
				<?php } ?>	
			
				<?php if($_SESSION['userSesion'] == null) { ?>
					<a href="<?php echo base_url(); ?>Usuario/IniciarSesion">Iniciar Sesion</a>
					<a href="<?php echo base_url(); ?>Usuario/Registrarse">Registrarse</a>
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/TuZona">Mi Zona</a>
				<?php } ?>
				<?php if($_SESSION['userRole'] == 1) { ?>
					<div class="dropdown">
					  <a class="dropbtn" >Control de Datos</a>
					  <div class="dropdown-content">
					  	<a href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados" style="color: black;">Lista de Usuarios</a>
					    <a href="<?php echo base_url(); ?>Stand/ListaStands" style="color: black;">Lista de Stands</a>
					    <a href="<?php echo base_url(); ?>Producto/ListaProductos" style="color: black;">Lista de Productos</a>
					  </div>
					</div>
					<!--<div class="dropdown">
						<a class="dropa" href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados">Lista de Usuarios</a>
						<div class="dropdown-content">
							<a href="#">Lista de Productos</a>
							<a href="#">Lista de Stands</a>
						</div>
					</div> -->
					
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/CerrarSesion">Cerrar Sesion</a>
				<?php } 
				if($_SESSION['userRole'] == 2 && $_SESSION['isPremium'] == 0) { ?>
					<a class="btn btn-danger" href="<?php echo base_url(); ?>Usuario/premium" style="border-radius: 2px;" role="button"><b style="color: white; ">VOLVERSE PREMIUM</b></a>
				<?php } ?>
			</div>
		</div>

		<div class="contenedor contenedor-grid">
			<div class="grid" id="grid">
				<div class="categorias">
					<button class="btn-regresar"><i class="fas fa-arrow-left"></i> Regresar</button>
					<h3 class="subtitulo">Categorias</h3>

					<a href="#" data-categoria="tecnologia-y-computadoras">Tecnología y Computadoras <i class="fas fa-angle-right"></i></a>
					<a href="#" data-categoria="libros">Libros <i class="fas fa-angle-right"></i></a>
					<a href="#" data-categoria="ropa-y-accesorios">Ropa y Accesorios <i class="fas fa-angle-right"></i></a>
					<a href="#" data-categoria="hogar-y-cocina">Hogar y Cocina <i class="fas fa-angle-right"></i></a>
					<a href="#" data-categoria="juegos-y-juguetes">Juegos y Juguetes <i class="fas fa-angle-right"></i></a>
					<a href="#" data-categoria="salud-y-belleza">Salud y Belleza <i class="fas fa-angle-right"></i></a>
					<a href="#" data-categoria="alimentos-y-bebidas">Alimentos y Bebidas <i class="fas fa-angle-right"></i></a>
				</div>

				<div class="contenedor-subcategorias">
					<div class="subcategoria" data-categoria="tecnologia-y-computadoras">
						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/tecnologia-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/tecnologia-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/tecnologia-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/tecnologia-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/tecnologia-galeria-4.png" alt="">
							</a>
						</div>
					</div>

					<div class="subcategoria" data-categoria="libros">					
						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/libros-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/libros-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/libros-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/libros-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/libros-galeria-4.png" alt="">
							</a>
						</div>
					</div>

					<div class="subcategoria" data-categoria="ropa-y-accesorios">					

						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/ropa-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/ropa-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/ropa-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/ropa-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/ropa-galeria-4.png" alt="">
							</a>
						</div>
					</div>

					<div class="subcategoria" data-categoria="hogar-y-cocina">					
						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/hogar-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/hogar-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/hogar-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/hogar-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/hogar-galeria-4.png" alt="">
							</a>
						</div>
					</div>

					<div class="subcategoria" data-categoria="juegos-y-juguetes">
						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/juegos-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/juegos-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/juegos-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/juegos-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/juegos-galeria-4.png" alt="">
							</a>
						</div>
					</div>

					<div class="subcategoria" data-categoria="salud-y-belleza">
						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/belleza-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/belleza-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/belleza-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/belleza-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/belleza-galeria-4.png" alt="">
							</a>
						</div>
					</div>

					<div class="subcategoria" data-categoria="alimentos-y-bebidas">
						<div class="banner-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/comida-banner-1.png" alt="">
							</a>
						</div>

						<div class="galeria-subcategoria">
							<a href="#">
								<img src="<?php echo base_url();?>public/img/comida-galeria-1.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/comida-galeria-2.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/comida-galeria-3.png" alt="">
							</a>
							<a href="#">
								<img src="<?php echo base_url();?>public/img/comida-galeria-4.png" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">		
		<div class="row" style="padding-bottom: 30px;">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-9">
						<?php foreach ($standInfo1->result() as $row) { ?>
							<h1 id="title"><?php echo $row->standName; ?></h1>
							<h5 id="description"><?php echo $row->standDescription; ?></h5>
						<?php } ?>
					</div>
					<?php  $userOwner = false;
					foreach ($listUsers->result() as $row) {
						if($row->userId == $_SESSION['userSesion']){
							$userOwner = true;
						}
					} 
					if($userOwner == true) { ?>
					<div class="col-md-3">
						<div class="ui icon button circular black cc_pointer" data-tooltip="Administrar Usuarios" data-position="bottom center" data-inverted="" style="position: absolute; top: 50%; left: 50%; -ms-transform: translateY(-50%); transform: translateY(-50%);" data-toggle="modal" data-target="#addStandOwnerModal" id="btnAddStandOwners">
							<i class="user plus icon"></i>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="row" style="padding-bottom: 30px;">
					<div class="col-md-12">
						<h4 id="tituloEncargados">Encargados:</h4>
						<div class="ui middle aligned animated list">
							<?php foreach ($standInfo2->result() as $row) { ?>
								<div class="item">
									<div class="content">
										<a href="<?php echo site_url('Usuario/Usuario?id='.$row->encryptedUserId); ?>" style="font-size: 17px;">
											<?php echo $row->name.' '.$row->firstSurname.' '.$row->secondSurname; ?>
										</a>		
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="image-area mt-4">
					<?php foreach ($standInfo1->result() as $row) { ?>			
						<img id="imageResult" src="<?php echo base_url(); ?>/images/TuCaseroImages/stands/<?php echo $row->standId;?>.jpg" alt="" class="img-fluid rounded shadow-sm mx-auto d-block">
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<h2 id="productTitle" style="vertical-align: middle; margin-top: 0px;"><i class="angle right icon"></i>Productos</h2>
			</div>
			<?php if($userOwner == true){ ?>
				<div class="col-md-4">
					<?php
					$atributos=array('method'=>'POST');
					echo form_open_multipart('Stand/ActualizarStand',$atributos); ?>
						<?php foreach ($standInfo1->result() as $row) { ?>
							<input type="text" name="standId" value="<?php echo $row->standId; ?>" hidden>
						<?php } ?>
						<button type="submit" class="btn btn-outline-secondary btn-block" role="button" aria-pressed="true" style="vertical-align: middle; border-radius: 2px;">Actualizar Stand</button>
					<?php echo form_Close(); ?>
				</div>
			<?php } else { ?>
				<div class="col-md-4">
				<?php if($_SESSION['userSesion'] && $numberOfReports == 0) { ?>
					<button type="button" class="ui button" data-toggle="modal" data-target="#openDeleteStandModal" id="btnReportStand" style="border-radius: 2px;">Reportar Stand</button>
					<?php } ?>		
					<div class="modal fade" id="openDeleteStandModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Reportar Stand</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">¿Estás seguro que quieres reportar este stand?</div>
								<?php $atributos=array('id'=>'uiFormDeleteStand','class'=>'ui form');
									echo form_open_multipart('Stand/ReportStand',$atributos); ?>
									<div class="field modal-body">
										<div class="form-group row">
										    <div class="col-sm-12">
										      	<select class="ui search selection dropdown" id="category" name="reportTypeId">
											    	<option value="" selected disabled>Tipo</option>
											    	<option value="1">Sospecha de fraude</option>
											    	<option value="2">Fraude</option>
											    	<option value="3">No asistencia a la cita</option>
											    	<option value="4">Mal comportamiento o abuso</option>
											    	<option value="5">Artículo defectuoso o incorrecto</option>
											    	<option value="6">Otras causas</option>
											  	</select>
										    </div>
										</div>
									</div>
									<div class="field modal-body">
										<div class="form-group row">
										    <div class="col-sm-10">
										      	<textarea name="description" id="description" placeholder="Estoy denunciando a este Stand porque..." maxlength="100" rows="3"></textarea>
										      	<div id="maxCharacters">
													<span id="current">0</span>
													<span id="maximum">/ 100</span>
												</div>
										    </div>
										</div>
									</div> 
								<div class="modal-footer">
								<button type="button" class="ui blue button" data-dismiss="modal" id="btnCancel">Cancelar</button>
									<input type="text" name="standIdD" value="<?php echo $row->standId;?>" hidden>
									<button type="submit" class="ui red button" id="btnReportStand2">Reportar</button>
									<?php echo form_Close(); ?>	
								</div>
							</div>
						</div>
					</div><br>
				</div>
			<?php } ?>

				


		</div>

		
		<div class="row" style="margin-bottom: 20px; margin-top: 40px;">
			<div class="col-md-12">
				<main>
					<div class="row">
						<div class="col-md-12">
							<div class="ui four doubling cards">
								<?php foreach ($standInfo3->result() as $row) { 
									$atributos=array('id'=>'uiForm','class'=>'ui form card');?>
									<a href="<?php echo site_url('Producto/Producto?id='.$row->encryptedProductId);?>" class="card">
										<img class="card-img-top" src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" height="235" alt="Card image cap">
										<div class="card-body">
											<h5 id="h5PPS" class="card-title"><?php echo $row->cost; ?></h5>
											<h6 class="card-text" id="productCardTitle"><?php echo $row->productName; ?></h6>
											<p class="card-text" id="pPPS"><?php echo $row->description; ?></p>
										</div>
									</a>
								<?php } ?>

								<?php
								if($userOwner == true && $numStandProducts < $maxNumStandProducts){
									$atributos=array('id'=>'uiForm','class'=>'ui form card');
									echo form_open_multipart('Producto/RegistrarProducto',$atributos); ?>
									<button type="submit" id="addStandBtn" style="border-style: none;">
										<?php foreach ($standInfo1->result() as $row) { ?>
											<input type="text" name="standId" value="<?php echo $row->standId; ?>" hidden>
										<?php } ?>
										<div class="card-img-top" height="235" style="margin-top: 90px; margin-bottom: 40px;">
											<h5 id="h5PPS" class="card-title" style="display: inline-block;">
												<span class="fa fa-plus-circle fa-5x" aria-hidden="true"></span>
											</h5>
										</div>
										<div class="card-body" style="display: inline-block; padding-bottom: 143px;">
											<p id="pCard" class="card-text">Agregar Producto</p>
										</div>
									</button>
								<?php echo form_Close();
								}

								if($userOwner == true){ ?>

								<div class="modal fade" id="addStandOwnerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document" style="margin-top: 200px;">
										<div class="modal-content" role="document">
											<div class="modal-header">
												<h5 class="modal-title" id="h5PPS">Dueños del Stand</h5>
											    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												Estás son los dueños del stand. Puedes quitar el cargo a un usuario o agregar a un usuario nuevo.
											</div>
											<div class="modal-footer">
												<?php $atributos=array('id'=>'multipleStandOwners', 'class'=>'ui form segment btn-block border-0','method'=>'POST');
													echo form_open_multipart('Stand/UpdateStandOwners',$atributos); ?>
													<input type="hidden" name="standId" value="<?php echo $originalStandId; ?>">
													<div class="row">
														<div class="col-md-12">
															<select id="users" name="users[]" class="ui fluid search dropdown" multiple="multiple" maxlength="1">
																<option value="">Usuarios</option>
																<?php foreach ($listOfUsersForStand as $row) {
																	if($row->role != 1) { ?>
																	<option value="<?php echo $row->userId;?>"
																		<?php if($row->role == 0) { ?> selected="" <?php } ?>>
																		<?php echo $row->username." (".$row->name.")"; ?>
																	</option>
																<?php }
																} ?>
															</select>
														</div>
													</div>
													<div class="ui error message"></div>
													<div class="row" style="margin-top: 10px;">
														<div class="col-md-12">
															<button type="submit" class="fluid ui inverted red button cc_pointer" style="border-radius: 2px;">Actualizar Dueños de Stand</button>
														</div>
													</div>
												<?php echo form_close();?>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</main>
			</div>
		</div>

		<?php if($userOwner == true && $standOrders->num_rows() > 0){ ?>
				<div class="col-md-8">
					<h2 id="productTitle" style="vertical-align: middle; margin-top: 0px;"><i class="angle right icon"></i>Pedidos</h2>
				</div>
				<div class="col-md-12">
				<table class="ui selectable inverted table" style="margin-bottom: 50px;">
					<thead>
						<tr>
							<th>Imagen Producto</th>
							<th>Nombre Producto</th>
							<th>Mensaje</th>
							<th>Total</th>
							<th>Solicitante</th>
							<th>Fecha De Petición</th>
							<th>Aceptar Pedido</th>
							<th>Denegar Pedido</th>      
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach ($standOrders->result() as $row) 
					{?>
						<tr>					
							<th>  
								<a href="<?php echo base_url()?>Producto/Producto?id=<?php echo $row->encryptedProductId ?>">
									<img src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" width="75" height="75"alt="Card image cap">
								</a> 
							</th>
							<th>  
								<a href="<?php echo base_url()?>Producto/Producto?id=<?php echo $row->encryptedProductId ?>" style="color:#FFF;">
									<?php echo $row->productName; ?>
								</a> 
							</th>
							<th><?php echo $row->orderMessage; ?></th>
							<th><?php echo $row->total; ?> Bs.</th>	
							<th>  
								<a href="<?php echo base_url()?>Usuario/Usuario?id=<?php echo $row->encryptedUserId ?>" style="color:#FFF;">
									<?php echo $row->username; ?>
								</a> 
							</th>
							<th><?php echo $row->orderDate; ?> </th>
							<th>
								<?php echo form_open_multipart('Producto/aceptarPedido') ?>
									<input type="hidden" name="orderID" value="<?php echo $row->orderID ?>">
									<input type="hidden" name="standId" value="<?php echo $row->standId ?>">
									<input class="btn btn-success" type="submit" value="Aceptar">
								<?php echo form_close() ?>
							</th>
							<th>
								<?php echo form_open_multipart('Producto/denegarPedido') ?>
									<input type="hidden" name="orderID" value="<?php echo $row->orderID ?>">
									<input type="hidden" name="standId" value="<?php echo $row->standId ?>">
									<input type="hidden" name="userId" value="<?php echo $row->userId ?>">
									<input class="btn btn-danger" type="submit" value="Denegar">
								<?php echo form_close() ?>
							</th>
						</tr>                   
					<?php
					}?>
					</tbody>
				</table>
			</div>
				

		<?php } ?>

	</div>


	
	<br>
	<footer style="padding-bottom: 30px; margin-top: 30px;">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('Producto/Search') ?>">
	                    <i class="fas fa-home iconos"></i>
	                    <h5>Inicio</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-money-check-alt iconos"></i>
	                    <h5>Formas de Pago</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('preguntas') ?>">
	                    <i class="far fa-question-circle iconos"></i>
	                    <h5>Preguntas Frecuentes</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-phone-volume iconos" ></i>
	                    <h5>Contáctenos</h5>
	                </a>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-12 div-enlaces">
	                <a href="https://www.facebook.com/subto.irizam"><i class="fab fa-facebook iconos-redes"></i></a>
	                <a href="https://twitter.com/CoolRodri"><i class="fab fa-twitter iconos-redes"></i></a>
	                <a href="https://www.youtube.com/channel/UCoHjLtQl55mPL7CnfX8vSAA?sub_confirmation=1"><i class="fab fa-youtube iconos-redes "></i></a>
	                <a href="https://www.instagram.com/irizam_/"><i class="fab fa-instagram iconos-redes"></i></a>
	            </div>
	        </div>  
	        <div class="row">
	        <div class="divisor"></div>
	            <p class="texto">TuCasero.com es un portal de comercio electrónico muy fácil de usar, donde podrás publicar todo lo que quieras vender y encontrarás lo que buscas entre miles de productos disponibles para tí. Las marcas comerciales y las marcas mencionadas son propiedad de sus propietarios correspondientes. El uso de este sitio implica la aceptación de los Términos y condiciones de uso, así como también de las Políticas de privacidad. TuCasero.com NO tiene responsabilidad alguna sobre los productos ofertados ni en las transacciones que ocurran entre los usuarios de este medio.</p>
	        </div>
	        <div class="row copy">
	            <div class="col-md-12 copy" >
	                <b>&copy2020 TuCasero.com | Todos los derechos reservados</b>
	                <a href=""><p>Terminos de condiciones de uso</p></a>
	                <a href=""><p>Politica de privacidad</p></a>
	            </div>
	        </div>
	    </div>
	</footer>
</body>
<script src="<?php echo base_url(); ?>public/scripts/scriptPublicProfileStand.js"></script>
<script src="https://kit.fontawesome.com/872ac11209.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptHead.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptCarrito.js"></script>
<script type="text/javascript">
	$(function(){
	$('#multipleStandOwners').form({
		users: {
				identifier: 'users',
				rules: [{ type: 'maxCount[<?php echo $getMaxNumStandOwners; ?>]', prompt: 'Su límite es de <?php echo $getMaxNumStandOwners; ?> usuarios' }]
			}
		});
	});
</script>
</html>
