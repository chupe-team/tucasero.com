<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12 pa-0">
            <div class="container mt-sm-60 mt-30" style="margin-bottom: 30px;">
                <div class="hk-row">
                    <div class="col-xl-12">
                        <div class="card card-lg">
                            <h3 class="card-header border-bottom-0">Preguntas Frecuentes</h3>
                            <div class="accordion accordion-type-2 accordion-flush" id="accordion_2">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between activestate">
                                        <a id="faq-link" class="faq-link" role="button" data-toggle="collapse" href="#collapse_1i" aria-expanded="true" style="padding-top: 13px;">¿Cómo creo una cuenta en TuCasero?</a>
                                    </div>
                                    <div id="collapse_1i" class="collapse show" data-parent="#accordion_2" role="tabpanel">
                                        <div class="card-body pa-15">Deberás introducir tus datos: nombre y apellidos, correo electrónico, nombre de usuario y contraseña, entre otros datos.</div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <a class="faq-link collapsed" role="button" data-toggle="collapse" href="#collapse_2i" aria-expanded="false" style="padding-top: 13px;">¿Cómo puedo editar mi perfil?</a>
                                    </div>
                                    <div id="collapse_2i" class="collapse" data-parent="#accordion_2">
                                        <div class="card-body pa-15">
                                            <p>Para editar tu perfil debes acceder a tu área personal, denominado "Tu Zona".</p>
                                            <p>Desde la página, podrás modificar su información pública (nombre completo y teléfono) y su información personal (email, fecha de nacimiento y sexo).</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <a class="faq-link collapsed" role="button" data-toggle="collapse" href="#collapse_3i" aria-expanded="false" style="padding-top: 13px;">¿Como puedo recuperar mi contraseña?</a>
                                    </div>
                                    <div id="collapse_3i" class="collapse" data-parent="#accordion_2">
                                        <div class="card-body pa-15">
                                            <p>Si no recuerdas qué contraseña pusiste al registrarte, accede a la opción "¿Olvidaste tu contraseña?" mediante el login, y recibirás una nueva clave en al correo vinculado a TuCasero.</p>
                                            <p>Una vez que recibas el correo, podrás registrar una nueva contraseña.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <a class="faq-link collapsed" role="button" data-toggle="collapse" href="#collapse_4i" aria-expanded="false" style="padding-top: 13px;">¿Como puedo darme de baja?</a>
                                    </div>
                                    <div id="collapse_4i" class="collapse" data-parent="#accordion_2">
                                        <div class="card-body pa-15">
                                            <p>Para desactivar tu cuenta, encontrarás la opción en "Tu Zona". La opción de desactivar la cuenta se encuentra, bajo el campo para modificar sus datos.</p>
                                            <p>Ten en cuenta que si das de baja tu cuenta, no será posible recuperarla y se eliminará sus respectivos stands y productos.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>