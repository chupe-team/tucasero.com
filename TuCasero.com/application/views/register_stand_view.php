<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Registrar Stand | TuCasero</title>
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/registerStandStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/styleHead.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/footerStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/carritoStyle.css">
</head>
<body>
<nav class="menu" id="menu">
		<div class="contenedor contenedor-botones-menu">
			<button id="btn-menu-barras" class="btn-menu-barras"><i class="fas fa-bars"></i></button>
			<button id="btn-menu-cerrar" class="btn-menu-cerrar"><i class="fas fa-times"></i></button>
		</div>

		<div class="contenedor contenedor-enlaces-nav">
			<a href="<?php echo site_url('Producto/Search') ?>">
				<img id="logoBienChevere" height="60px" src="<?php echo base_url(); ?>/images/logo3.png">
			</a>
			<div class="enlaces ml-auto">
				<?php if($_SESSION['userSesion']) { ?>
					<input type="hidden" id="MostrarCarrito" value="<?php echo base_url();?>index.php/Producto/mostrarCarrito">     		
						<a class="dropdown-toggle" id="carritoMostrarDatos"  data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="true"><i id="dropdownCarrito" class="fas fa-cart-plus" style="color: yellow;" ></i></a>
						<div class="dropdown-menu" aria-labelledby="dropdownCarrito" id="resultado">
							<div class="contenedorCarrito" id="contenedorCarrito">
														
							</div>
							<div class="botones-carrito" style="text-align: center;">
								<a class="btn btn-danger btn-lg" href="" id="vaciarCarrito">vaciar</a>
								<input id="direccionVaciarCarrito" type="hidden" value="<?php echo base_url()?>index.php/Producto/vaciarCarrito">
								<a class="btn btn-success btn-lg active"  href="<?php echo base_url(); ?>index.php/Producto/visitarCarrito">ir de compras</a>
							</div>
						</div>
				<?php } ?>	
			
				<?php if($_SESSION['userSesion'] == null) { ?>
					<a href="<?php echo base_url(); ?>Usuario/IniciarSesion">Iniciar Sesion</a>
					<a href="<?php echo base_url(); ?>Usuario/Registrarse">Registrarse</a>
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/TuZona">Mi Zona</a>
				<?php } ?>
				<?php if($_SESSION['userRole'] == 1) { ?>
					<div class="dropdown">
					  <a class="dropbtn" >Control de Datos</a>
					  <div class="dropdown-content">
					  	<a href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados" style="color: black;">Lista de Usuarios</a>
					    <a href="<?php echo base_url(); ?>Stand/ListaStands" style="color: black;">Lista de Stands</a>
					    <a href="<?php echo base_url(); ?>Producto/ListaProductos" style="color: black;">Lista de Productos</a>
					  </div>
					</div>
					<!--<div class="dropdown">
						<a class="dropa" href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados">Lista de Usuarios</a>
						<div class="dropdown-content">
							<a href="#">Lista de Productos</a>
							<a href="#">Lista de Stands</a>
						</div>
					</div> -->
					
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/CerrarSesion">Cerrar Sesion</a>
				<?php } 
				if($_SESSION['userRole'] == 2 && $_SESSION['isPremium'] == 0) { ?>
					<a class="btn btn-danger" href="<?php echo base_url(); ?>Usuario/premium" style="border-radius: 2px;" role="button"><b style="color: white; ">VOLVERSE PREMIUM</b></a>
				<?php } ?>
			</div>
		</div>
	</nav>
	<div class="container">
		<!--The first part of the page-->
		<div class="row">
			<div class="col-md-12"><br>
				<nav>
					<div class="row">
						<div class="col-md-12">
							<h1 id="h1RS">Registrar Stand</h1>
							<label>Aquí podrás registrar tus stands para contener tus productos.</label>
						</div>
					</div>
				</nav>
			</div>
		</div><br>
		<div id="registerStand" class="row">
			<div class="col-md-12">
				<main>
					<div class="row">
						<div class="col-md-12">
							<?php
								$atributos=array('id'=>'uiForm','class'=>'ui form', 'enctype'=>'multipart/form-data');
								echo form_open_multipart('Stand/RegisterStand',$atributos);?>
								<div class="card">
									<div class="field">
										<div class="form-group row">
											<label for="standName" class="col-sm-4 col-form-label">Nombre de Stand:</label>
											<div class="col-sm-8">
												<input type="text" class="form-control-plaintext" id="standName" name="standName" maxlength="40">
												<div id="stand-name-char-count">
													<span id="current-stand-name-char-count">0</span>
													<span id="maximum-stand-name-char-count">/ 40</span>
												</div>
											</div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
											<label for="standDescription" class="col-sm-4 col-form-label">Descripción:</label>
											<div class="col-sm-8">
												<textarea id="textareaRS" class="form-control" id="standDescription" name="standDescription" rows="2" maxlength="255"></textarea>
												<div id="the-count">
													<span id="current">0</span>
													<span id="maximum">/ 255</span>
												</div>
											</div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
											<label for="standImage" class="col-sm-4 col-form-label">Imagen:</label>
											<div class="col-sm-8">
												<span class="fa fa-cloud-upload text-muted" hidden="true"></span>
												<div id="standImage" class="input-group mb-3 px-2 py-2 rounded-pill shadow-sm" style="background-color: #DDDDDE">
													<input type="file" id="upload" name="upload" onchange="readURL(this);" class="form-control">
													<label id="upload-label" for="upload" class="font-weight-light text-muted">Elegir Imagen</label>
												</div>
												<div class="image-area mt-4">
													<img id="imageResult" src="#" alt="" class="img-fluid rounded shadow-sm mx-auto d-block">
												</div>
											</div>
										</div>
									</div>
								</div><br>
								<div class="ui error message"></div>
								<div class="row">
									<div class="col-md-12 text-center">
										<button class="ui button" type="submit" id="buttonSaveStand">REGISTRAR STAND</button>
									</div>
								</div><br>
								<?php
				            echo form_close();
				            ?>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>
	<footer style="padding-bottom: 30px; margin-top: 30px;">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('Producto/Search') ?>">
	                    <i class="fas fa-home iconos"></i>
	                    <h5>Inicio</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-money-check-alt iconos"></i>
	                    <h5>Formas de Pago</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('preguntas') ?>">
	                    <i class="far fa-question-circle iconos"></i>
	                    <h5>Preguntas Frecuentes</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-phone-volume iconos" ></i>
	                    <h5>Contáctenos</h5>
	                </a>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-12 div-enlaces">
	                <a href="https://www.facebook.com/subto.irizam"><i class="fab fa-facebook iconos-redes"></i></a>
	                <a href="https://twitter.com/CoolRodri"><i class="fab fa-twitter iconos-redes"></i></a>
	                <a href="https://www.youtube.com/channel/UCoHjLtQl55mPL7CnfX8vSAA?sub_confirmation=1"><i class="fab fa-youtube iconos-redes "></i></a>
	                <a href="https://www.instagram.com/irizam_/"><i class="fab fa-instagram iconos-redes"></i></a>
	            </div>
	        </div>  
	        <div class="row">
	        <div class="divisor"></div>
	            <p class="texto">TuCasero.com es un portal de comercio electrónico muy fácil de usar, donde podrás publicar todo lo que quieras vender y encontrarás lo que buscas entre miles de productos disponibles para tí. Las marcas comerciales y las marcas mencionadas son propiedad de sus propietarios correspondientes. El uso de este sitio implica la aceptación de los Términos y condiciones de uso, así como también de las Políticas de privacidad. TuCasero.com NO tiene responsabilidad alguna sobre los productos ofertados ni en las transacciones que ocurran entre los usuarios de este medio.</p>
	        </div>
	        <div class="row copy">
	            <div class="col-md-12 copy" >
	                <b>&copy2020 TuCasero.com | Todos los derechos reservados</b>
	                <a href=""><p>Terminos de condiciones de uso</p></a>
	                <a href=""><p>Politica de privacidad</p></a>
	            </div>
	        </div>
	    </div>
	</footer>
	<script src="<?php echo base_url(); ?>public/scripts/scriptAddStand.js"></script>
	<script src="https://kit.fontawesome.com/872ac11209.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script src="<?php echo base_url(); ?>public/scripts/scriptHead.js"></script>
	<script src="<?php echo base_url(); ?>public/scripts/scriptCarrito.js"></script>
	<script src="<?php echo base_url(); ?>public/scripts/scriptSearch.js"></script>
</body>
</html>
