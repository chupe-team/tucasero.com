<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Restablecer Contraseña | TuCasero</title>
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/resetPasswordStyle.css">
</head>
<body>
	<div class="h-100">
		<div class="container h-100">
			 <div class="row justify-content-center h-100">
			 	<div class="col-sm-8 align-self-center">
			 		<?php if(isset($error)) { ?>
			 		<div class="alert alert-danger"><?= $error; ?></div> <?php } ?>
			 		<div class="card" style="border-bottom-color: #333333;">
			 			<div class="card-header" style="background-color: #333333; color: #f6f6f6;">
						    <h5 class="card-title text-center">Restablecer Contraseña</h5>
						</div>
			 			<div class="card-body">
			 				<?php
	                      	$atributos=array('id'=>'uiForm','class'=>'ui form');
	                      	echo form_open_multipart('Usuario/ResetNewPassword',$atributos);
	                      	?>
							  <div class="form-group">
							    <label for="newPassword">Nueva Contraseña</label>
							    <input type="password" class="form-control" id="newPassword" name="newPassword">
							    <input type="text" class="form-control" hidden id="token" name="token" value="<?php echo $_GET['token']; ?>">
							  </div>
							  <div class="form-group">
							    <label for="newPassword2">Repetir Nueva Contraseña</label>
							    <input type="password" class="form-control" id="newPassword2" name="newPassword2">
							  </div>
							  <button type="submit" class="ui button" id="buttonReset" style="border-radius: 2px;">Establecer Contraseña</button>
							  <div class="ui error message"></div>
							<?php echo form_close(); ?>
			 			</div>
			 		</div>
			 		<label style="margin-top: 15px;">¿Ya dispones de una cuenta? <a style="color: #333333;" href="<?php echo site_url('Usuario/index'); ?>">Iniciar Sesión</a></label>
			 	</div>
			 </div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('#uiForm')
			  .form({
			    fields: {
			      userName: {
			        identifier: 'newPassword',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor introduzca una nueva contraseña'
			          }
			        ]
			      },
			      password: {
			        identifier: 'newPassword2',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor repita su nueva contraseña'
			          }
			        ]
			      },
			      match: {
			        identifier  : 'newPassword2',
			        rules: [
			          {
			            type   : 'match[newPassword]',
			            prompt : 'Las contraseñas no coinciden'
			          }
			        ]
			      }
			    }
			  })
			;
		});
	</script>
</body>
</html>
