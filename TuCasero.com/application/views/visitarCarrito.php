<?php if(!$_SESSION['userSesion']) { 
	redirect('Usuario/index','refresh');			
 } 
 else{?>
<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-secondary">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>/Producto/Search"><i class="home icon" ></i>Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mi Carrito de compras</li>      
    </ol>
    </nav>
</div>
<!-- tabla para el carrito -->
<!-- se hizo cambios a esta interfaz para que pueda insertar a la base de datos los items del carrito -->
<div class="container">
    <div class="row">
        <div class="col col-md-12 tablaCarrito">
        <h2 class="tituloCarrito" style="text-align: center; margin-bottom:50px">
            Tu carrito de compras con (<?php
                $numItems = 0;
                foreach ($productos->result() as $item) { $numItems += $item->quantity; }
                echo $numItems; ?>) productos
        </h2>
        <?php //echo $this->cart->total_items(); ?>
        <table class="ui selectable inverted table" style="margin-bottom: 50px;">
            <thead>
                <tr>
                <th>#</th>
                <th>Foto</th>
                <th>Descripcion</th>
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Mensaje</th>
                <th>Pedir</th>     
                </tr>
            </thead>
            <tbody>
                <?php
                $indice=1;
                
                    foreach ($productos->result() as $item) {?>
                        <tr>
                            <?php echo form_open_multipart('Producto/PeticionProducto') ?>
                            <td><?php echo $indice; ?></td>
                            <td><img class="imgCarrito" src="<?php echo $item->image?>" alt=""></td>
                            <td>
                                <b><?php echo $item->productName ?></b>
                                <p><?php echo $item->description?></p>
                            </td>
                            <td><?php echo $item->quantity ?></td>
                            <td><?php echo $item->cost.' Bs.' ?></td>
                            <td><textarea class="form form-control" name="message" id="" cols="30" rows="2"></textarea></td>
                            <input type="hidden" name="idProducto" value="<?php echo $item->productId?>">
                            <input type="hidden" name="idUsuario" value="<?php echo $_SESSION["userSesion"] ?>">
                            <input type="hidden" name="cantidad" value="<?php echo $item->quantity ?>">
                            <input type="hidden" name="idItemCarrito" value="<?php echo $item->cartId ?>">
                            <td><input class="btn btn-success" type="submit" value="Pedir producto" style="margin-bottom: 10px;"><?php echo form_close() ?> 
                            <?php echo form_open_multipart('Producto/quitarItemCarrito') ?>
                            <input type="hidden" name="idItemCarrito" value="<?php echo $item->cartId ?>">
                            <input class="btn btn-danger" type="submit" value="Quitar del carrito">
                            <?php echo form_close() ?>
                        </td>
                            

                        </tr>                   
                    <?php            
                    $indice++;}?>
            </tbody>
            </table>
        </div>
    </div>
</div>
<?php }?>