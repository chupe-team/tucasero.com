
<?php if(!$_SESSION['userSesion']) { 
	redirect('Usuario/index','refresh');			
 } 
 else{?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Detalles del Pago</h3>
				</div>
				<div class="panel-body">
					<form class="ui form" id="uiForm" method="post" action="becomingPremium">
						<div class="form-group">
							<label for="cardNumber">Número de tarjeta:</label>
							<div class="input-group">
								<input type="text" class="form-control" id="cardNumber" name="cardNumber" placeholder="Número de tarjeta válido (Ej: 4716251691824649)" maxlength="16" autofocus />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-7">
								<div class="form-group">
									<label for="expiryMonth">Fecha de vencimiento: (averiguar si la fecha debe ser mayor al día actual o no)</label>
									<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
										<input type="month" class="form-control" id="expiryMonth" name="expiryMonth"/>
									</div>
								</div>
							</div>
							<div class="col-md-5 pull-right">
								<div class="form-group">
									<label for="cvCode">Código de seguridad:</label>
									<input type="password" class="form-control" id="cvCode" name="cvCode"placeholder="Código de seguridad" maxlength="3"/>
								</div>
							</div>
						</div>
						<div class="ui error message"></div>
						<button type="submit" class="btn btn-success btn-md btn-block" style="border-radius: 2px;"><b>VOLVERSE PREMIUM</b></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php }?>