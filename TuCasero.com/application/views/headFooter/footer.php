    <footer style="padding-bottom: 30px; margin-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 div-enlaces">
                <a href="<?php echo site_url('Producto/Search') ?>">
                    <i class="fas fa-home iconos"></i>
                    <h5>Inicio</h5>
                </a>
            </div>
            <div class="col-md-3 div-enlaces">
                <a href="">
                    <i class="fas fa-money-check-alt iconos"></i>
                    <h5>Formas de Pago</h5>
                </a>
            </div>
            <div class="col-md-3 div-enlaces">
                <a href="<?php echo site_url('preguntas') ?>">
                    <i class="far fa-question-circle iconos"></i>
                    <h5>Preguntas Frecuentes</h5>
                </a>
            </div>
            <div class="col-md-3 div-enlaces">
                <a href="">
                    <i class="fas fa-phone-volume iconos" ></i>
                    <h5>Contáctenos</h5>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 div-enlaces">
                <a href="https://www.facebook.com/subto.irizam"><i class="fab fa-facebook iconos-redes"></i></a>
                <a href="https://twitter.com/CoolRodri"><i class="fab fa-twitter iconos-redes"></i></a>
                <a href="https://www.youtube.com/channel/UCoHjLtQl55mPL7CnfX8vSAA?sub_confirmation=1"><i class="fab fa-youtube iconos-redes "></i></a>
                <a href="https://www.instagram.com/irizam_/"><i class="fab fa-instagram iconos-redes"></i></a>
            </div>
        </div>  
        <div class="row">
        <div class="divisor"></div>
            <p class="texto">TuCasero.com es un portal de comercio electrónico muy fácil de usar, donde podrás publicar todo lo que quieras vender y encontrarás lo que buscas entre miles de productos disponibles para tí. Las marcas comerciales y las marcas mencionadas son propiedad de sus propietarios correspondientes. El uso de este sitio implica la aceptación de los Términos y condiciones de uso, así como también de las Políticas de privacidad. TuCasero.com NO tiene responsabilidad alguna sobre los productos ofertados ni en las transacciones que ocurran entre los usuarios de este medio.</p>
        </div>
        <div class="row copy">
            <div class="col-md-12 copy" >
                <b>&copy2020 TuCasero.com | Todos los derechos reservados</b>
                <a href=""><p>Terminos de condiciones de uso</p></a>
                <a href=""><p>Politica de privacidad</p></a>
            </div>
        </div>
    </div>
</footer>

<!-- scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/872ac11209.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptSearch.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptHead.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptUpdateProfile.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptCarrito.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptPremiumUser.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
</body>
</html>