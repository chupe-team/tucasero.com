<?php 
if(!isset($_SESSION['userSesion'])) { 
  $_SESSION['userSesion'] =0;
} 
if(!isset($_SESSION['userRole'])) { 
	$_SESSION['userRole'] =0;
  } 
if(!isset($_SESSION['recuerdame'])) { 
$_SESSION['recuerdame'] =0;
} 
if(!isset($_SESSION['isPremium'])) { 
	$_SESSION['isPremium'] =0;
	} 
/*unset($_SESSION['userSesion']);
unset($_SESSION['userRole']);
unset($_SESSION['recuerdame']);*/
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<link href="<?php echo base_url(); ?>public/css/styles.css" rel="stylesheet" >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/styleHead.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/updateProfileStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/footerStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/product_viewStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/carritoStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/paginacionStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/error404.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/faq.css" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.2.0/css/ionicons.min.css" integrity="sha256-F3Xeb7IIFr1QsWD113kV2JXaEbjhsfpgrKkwZFGIA4E=" crossorigin="anonymous" />

	<title><?php echo $headTitle; ?> | TuCasero</title>
</head>
<body>
	<nav class="menu" id="menu">
		<div class="contenedor contenedor-botones-menu">
			<button id="btn-menu-barras" class="btn-menu-barras"><i class="fas fa-bars"></i></button>
			<button id="btn-menu-cerrar" class="btn-menu-cerrar"><i class="fas fa-times"></i></button>
		</div>
		<div class="contenedor contenedor-enlaces-nav">
			<a href="<?php echo site_url('Producto/Search') ?>">
				<img id="logoBienChevere" height="60px" src="<?php echo base_url(); ?>/images/logo3.png">
			</a>
			<div class="enlaces ml-auto">
				<?php if($_SESSION['userSesion']) { ?>
					<input type="hidden" id="MostrarCarrito" value="<?php echo base_url();?>index.php/Producto/mostrarCarrito">     		
						<a class="dropdown-toggle" id="carritoMostrarDatos"  data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="true"><i id="dropdownCarrito" class="fas fa-cart-plus" style="color: yellow;" ></i></a>
						<div class="dropdown-menu" aria-labelledby="dropdownCarrito" id="resultado">
							<div class="contenedorCarrito" id="contenedorCarrito">
														
							</div>
							<div class="botones-carrito" style="text-align: center;">
								<a class="btn btn-danger btn-lg" href="" id="vaciarCarrito">vaciar</a>
								<input id="direccionVaciarCarrito" type="hidden" value="<?php echo base_url()?>index.php/Producto/vaciarCarrito">
								<a class="btn btn-success btn-lg active"  href="<?php echo base_url(); ?>index.php/Producto/visitarCarrito">ir de compras</a>
							</div>
						</div>
				<?php } ?>	
			
				<?php if($_SESSION['userSesion'] == null) { ?>
					<a href="<?php echo base_url(); ?>Usuario/IniciarSesion">Iniciar Sesion</a>
					<a href="<?php echo base_url(); ?>Usuario/Registrarse">Registrarse</a>
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/TuZona">Mi Zona</a>
				<?php } ?>
				<?php if($_SESSION['userRole'] == 1) { ?>
					<div class="dropdown">
					  <a class="dropbtn" >Control de Datos</a>
					  <div class="dropdown-content">
					  	<a href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados" style="color: black;">Lista de Usuarios</a>
					    <a href="<?php echo base_url(); ?>Stand/ListaStands" style="color: black;">Lista de Stands</a>
					    <a href="<?php echo base_url(); ?>Producto/ListaProductos" style="color: black;">Lista de Productos</a>
					  </div>
					</div>
					<!--<div class="dropdown">
						<a class="dropa" href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados">Lista de Usuarios</a>
						<div class="dropdown-content">
							<a href="#">Lista de Productos</a>
							<a href="#">Lista de Stands</a>
						</div>
					</div> -->
					
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/CerrarSesion">Cerrar Sesion</a>
				<?php } 
				if($_SESSION['userRole'] == 2 && $_SESSION['isPremium'] == 0) { ?>
					<a class="btn btn-danger" href="<?php echo base_url(); ?>Usuario/premium" style="border-radius: 2px;" role="button"><b style="color: white; ">VOLVERSE PREMIUM</b></a>
				<?php } ?>
			</div>
		</div>
	</nav>