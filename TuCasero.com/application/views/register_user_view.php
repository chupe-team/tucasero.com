<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($_SESSION['recuerdame'] == 0)
 {
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Registrarse | TuCasero</title>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/registerStyle.css">
	<script src="<?php echo base_url(); ?>public/scripts/scriptRegister.js"></script>
    <style type="text/css">
		.image-container
		{
			/*no se peude poner codigo php en un archivo*/
			background: url('<?php echo base_url(); ?>/images/background.jpg') center no-repeat;
			background-size: cover;
			min-height: 100vh;
		}		
	</style>

</head>
<body>
    <div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 form-container">
				<div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box">
					<div class="logo mb-3 text-center">
						<img src="<?php echo base_url(); ?>/images/logo2.png" height="180" width="200">
					</div>
					<?php
                      	$atributos=array('id'=>'uiForm','class'=>'ui form');
                      	echo form_open_multipart('Usuario/RegisterUser',$atributos);
                      	?>
					
					  <div class="field">
					    <input type="text" name="name" value="<?php echo $_SESSION['nombres'];?>" placeholder="Nombres">
                      </div>

                      <div class="field">
					    <input type="text" name="firstSurname" value="<?php echo $_SESSION['primerApellido'];?>" placeholder="Primer Apellido" >
                      </div>

                      <div class="field">
					    <input type="text" name="secondSurname" value="<?php echo $_SESSION['segundoApellido'];?>" placeholder="Segundo Apellido (Opcional)" >
                      </div>

					  <div class="field">
					    <input type="text" name="username" value="<?php echo $_SESSION['username'];?>" placeholder="Nombre De Usuario">
                      </div>

					  <div class="field">
						<input type="password" name="password" value="<?php echo $_SESSION['password'];?>" placeholder="Contraseña" id="pass1">
					  </div>

					  <div class="field">
					    <input type="password" name="re_password" value="<?php echo $_SESSION['password2'];?>" placeholder="Repita Contraseña">
                      </div>

                      <div class="field">
					    <input type="date" name="dateOfBirth" value="<?php echo $_SESSION['dateOfBirth'];?>" placeholder="Fecha Nacimiento" max="2002-12-30">
                      </div>

					  <div class="field">
					    <input type="text" name="ci" value="<?php echo $_SESSION['ci'];?>" placeholder="Carnet de identidad">
                      </div>

					  <div class="field">
					    <input type="text" name="email" value="<?php echo $_SESSION['email'];?>" placeholder="Correo electronico">
                      </div>

                      <div class="field">
					    <input type="text" name="phoneNumber" value="<?php echo $_SESSION['phoneNumber'];?>" placeholder="Numero De Celular">
                      </div>

                      <div class="field">
                        <select name="gender" class="ui dropdown">
							<option value="" disabled selected >Genero</option>
                            <option value="M" <?php if($_SESSION['gender'] == 'M') echo "selected"; ?> >Masculino</option>
                            <option value="F" <?php if($_SESSION['gender'] == 'F') echo "selected"; ?> >Femenino</option>
                            <option value="O" <?php if($_SESSION['gender'] == 'O') echo "selected"; ?>>Otro</option>
                        </select>
                      </div>
                      <button class="ui yellow button" type="submit" id="buttonRegister" onclick="validateData()">Registrarme</button>
					  <div class="ui error message"></div>
						<div class="text-center">
					  	<br>
                    <?php echo form_close(); ?>
					<div class="ui horizontal divider">O</div>
					<?php echo form_open_multipart('Usuario/IniciarSesion'); ?>
					<div class="field">						
                        <button class="ui orange button" type="submit" id="buttonLogin">Iniciar Sesión</button><br>
					</div>
					<?php echo form_close(); ?>
					<br>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 d-none d-md-block image-container">		
			</div>
	</div>
</body>
<script type="text/javascript">
	if (window.history.replaceState) {
		window.history.replaceState( null, null, window.location.href );
	}
</script>
</html>
<?php }
else
{
	redirect('Usuario/index','refresh');
} ?>