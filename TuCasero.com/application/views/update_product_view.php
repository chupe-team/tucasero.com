<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php foreach ($productInfo->result() as $row) { ?>
	<title>Actualizar <?php echo $row->productName; ?> | TuCasero</title>
	<?php } ?>
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/updateProductStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/styleHead.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/footerStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/carritoStyle.css">
	<style type="text/css">
		.ui.label.transition.visible {
			width: 100px;
			white-space:pre-wrap;
			word-wrap: break-word;
			text-align: center;
		}
	</style>
</head>
<body>
<nav class="menu" id="menu">
		<div class="contenedor contenedor-botones-menu">
			<button id="btn-menu-barras" class="btn-menu-barras"><i class="fas fa-bars"></i></button>
			<button id="btn-menu-cerrar" class="btn-menu-cerrar"><i class="fas fa-times"></i></button>
		</div>

		<div class="contenedor contenedor-enlaces-nav">
			<a href="<?php echo site_url('Producto/Search') ?>">
				<img id="logoBienChevere" height="60px" src="<?php echo base_url(); ?>/images/logo3.png">
			</a>
			<div class="enlaces ml-auto">
				<?php if($_SESSION['userSesion']) { ?>
					<input type="hidden" id="MostrarCarrito" value="<?php echo base_url();?>index.php/Producto/mostrarCarrito">     		
						<a class="dropdown-toggle" id="carritoMostrarDatos"  data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="true"><i id="dropdownCarrito" class="fas fa-cart-plus" style="color: yellow;" ></i></a>
						<div class="dropdown-menu" aria-labelledby="dropdownCarrito" id="resultado">
							<div class="contenedorCarrito" id="contenedorCarrito">
														
							</div>
							<div class="botones-carrito" style="text-align: center;">
								<a class="btn btn-danger btn-lg" href="" id="vaciarCarrito">vaciar</a>
								<input id="direccionVaciarCarrito" type="hidden" value="<?php echo base_url()?>index.php/Producto/vaciarCarrito">
								<a class="btn btn-success btn-lg active"  href="<?php echo base_url(); ?>index.php/Producto/visitarCarrito">ir de compras</a>
							</div>
						</div>
				<?php } ?>	
			
				<?php if($_SESSION['userSesion'] == null) { ?>
					<a href="<?php echo base_url(); ?>Usuario/IniciarSesion">Iniciar Sesion</a>
					<a href="<?php echo base_url(); ?>Usuario/Registrarse">Registrarse</a>
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/TuZona">Mi Zona</a>
				<?php } ?>
				<?php if($_SESSION['userRole'] == 1) { ?>
					<div class="dropdown">
					  <a class="dropbtn" >Control de Datos</a>
					  <div class="dropdown-content">
					  	<a href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados" style="color: black;">Lista de Usuarios</a>
					    <a href="<?php echo base_url(); ?>Stand/ListaStands" style="color: black;">Lista de Stands</a>
					    <a href="<?php echo base_url(); ?>Producto/ListaProductos" style="color: black;">Lista de Productos</a>
					  </div>
					</div>
					<!--<div class="dropdown">
						<a class="dropa" href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados">Lista de Usuarios</a>
						<div class="dropdown-content">
							<a href="#">Lista de Productos</a>
							<a href="#">Lista de Stands</a>
						</div>
					</div> -->
					
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/CerrarSesion">Cerrar Sesion</a>
				<?php } 
				if($_SESSION['userRole'] == 2 && $_SESSION['isPremium'] == 0) { ?>
					<a class="btn btn-danger" href="<?php echo base_url(); ?>Usuario/premium" style="border-radius: 2px;" role="button"><b style="color: white; ">VOLVERSE PREMIUM</b></a>
				<?php } ?>
			</div>
		</div>
	</nav>
	<div class="container" >
		<div class="row">
			<div class="col-md-12">
				<br>
				<nav>
					<div class="row">
						 <div class="col-md-12">
							<h3 id="h3UP">ACTUALIZAR PRODUCTO</h3>
						</div>
					</div>
				</nav>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-12">
				<main>
					<div class="row">
						<div class="col-md-12">
							<?php
								foreach ($productInfo->result() as $row) {
				            	$atributos=array('id'=>'uiForm','class'=>'ui form');
				                echo form_open_multipart('Producto/UpdateProduct',$atributos);
				                ?>
								<div class="card" style=" ">
									<div class="row">
										<h5 id="h5UP">INFORMACIÓN DE TU PRODUCTO</h5>
									</div><br>
									<div class="field">
										<div class="form-group row">
										    <label class="col-sm-2 col-form-label" id="label">¿Qué estás vendiendo?</label>
										    <div class="col-sm-7">
										      	<input type="text" class="form-control-plaintext" id="title" name="title" placeholder="En pocas palabras..." maxlength="25" value="<?php echo $row->productName; ?>">
												<?php $productNameCharCount = strlen($row->productName); ?>
												<div id="maxCharactersInput"
													<?php if($productNameCharCount >= 15) {
														echo 'style = "font-weight: bold; color: #8f0001;"';
													}?>>
													<span id="currentInput"><?php echo $productNameCharCount; ?></span>
													<span id="maximumInput">/ 25</span>
												</div>
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label id="labelUP" class="col-sm-2 col-form-label">Precio</label>
										    <div class="col-sm-2">
										      <input type="text" class="form-control" id="costNumber" name="costNumber" placeholder="(No te pases)" value="<?php echo $row->cost; ?>">
										    </div>
										    <div class="col-sm-1 col-form-label">
										    	<label id="labelUP">Bs.</label>
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label id="labelUP" class="col-sm-2 col-form-label">Cantidad</label>
										    <div class="col-md-2">
										      <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Cantidad" value="<?php echo $row->quantity; ?>">
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label id="labelUP" class="col-sm-2 col-form-label">Categoría</label>
										    <div class="col-sm-3">
										      	<select class="ui search selection dropdown" id="category" name="category">
													<option value="" selected disabled>Categoría</option>
													<?php foreach ($getCategories->result() as $cat) { ?>
													<option value="<?php echo $cat->categoryId; ?>"
														<?php if($row->categoryId == $cat->categoryId) echo "selected"; ?>>
														<?php echo $cat->categoryName; ?>
													</option>
													<?php } ?>
											  	</select>
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label id="labelUP" class="col-sm-2 col-form-label">Descripción</label>
										    <div class="col-sm-7">
										      	<textarea name="description" id="description" placeholder="Añade información revelante como estado, modelo, color..." maxlength="100" rows="2"><?php echo $row->description; ?></textarea>
												<?php $productDescriptionCharCount = strlen($row->description); ?>
										      	<div id="maxCharacters"
													<?php if($productDescriptionCharCount >= 80) {
														echo 'style = "font-weight: bold; color: #8f0001;"';
													}?>>
													<span id="current"><?php echo $productDescriptionCharCount; ?></span>
													<span id="maximum">/ 100</span>
												</div>
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label id="labelUP" class="col-sm-2 col-form-label">Tags</label>
										    <div class="col-sm-7">
										      	<select name="tags[]" id="tags" multiple="" class="ui search fluid dropdown">
													<option value="" selected disabled>Tags</option>
										      		<?php foreach ($tags->result() as $row1) { ?>
														<option value="<?php echo $row1->tagName; ?>" selected><?php echo $row1->tagName; ?></option>
													<?php } ?>
												</select>
										    </div>
										</div>
									</div>
								</div><br>
								<div class="card">
									<div class="row">
										<h5 id="h5UP">FOTO</h5>
									</div><br>
									<div class="field">
										<div class="form-group row">
											<label id="labelUP" for="standImage" class="col-sm-2 col-form-label">Imagen:</label>
										    <div class="col-sm-10">
										    	<div id="productImage" class="input-group mb-3 px-2 py-2 shadow-sm">
													<input type="file" name="upload" multiple id="files" class="form-control">
													<label id="files-label" for="files" style="color: #333333;">Elegir foto(s)</label>
												</div>

												<label id="labelUP">Imagen Antigua:</label>
												<div class="image-area mt-4">
													<img id="imageResult" src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" onchange="readURL(this);" width="300px" height="300px" alt="" class="img-fluid rounded shadow-sm mx-auto d-block">
												</div>
												<label id="labelUP">Nueva Imagen:</label>

										    	<!--<input type="file" multiple id="files"> -->
										    	<output id="result"/>
										    </div>
										</div>
									</div>
								</div><br><br>
								<div class="ui error message"></div>
								<div class="row">
									<div class="col-md-12 text-center">
										<input type="text" name="productId" hidden value="<?php echo $row->productId; ?>">
										<input type="text" name="standId" hidden value="<?php echo $row->standId; ?>">
										<button class="ui button" type="submit" id="buttonSave" >ACTUALIZAR PRODUCTO</button>
									</div>
								</div><br>
							<?php echo form_close(); 
							}
							?>
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="ui button" data-toggle="modal" data-target="#openDeleteProductModal" id="btnDeleteProduct">ELIMINAR PRODUCTO</button>
								</div>
							</div>
							<div class="modal fade" id="openDeleteProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Eliminar Producto</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">¿Estás seguro que quieres eliminar el producto?</div>
										<div class="modal-footer">
										<button type="button" class="ui blue button" data-dismiss="modal" id="btnCancel">Cancelar</button>
										<?php echo form_open_multipart('Producto/DeleteProduct');
											foreach ($productInfo->result() as $row) { ?>
												<input type="text" hidden name="productIdD" value="<?php echo $row->productId; ?>">
												<input type="text" hidden name="standIdD" value="<?php echo $row->standId; ?>"> 
												<button type="submit" class="ui red button" id="btnDeleteProduct2">Eliminar</button>
											<?php }
										echo form_Close(); ?>
										</div>
									</div>
								</div>
							</div><br>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>
	<footer style="padding-bottom: 30px; margin-top: 30px;">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('Producto/Search') ?>">
	                    <i class="fas fa-home iconos"></i>
	                    <h5>Inicio</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-money-check-alt iconos"></i>
	                    <h5>Formas de Pago</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('preguntas') ?>">
	                    <i class="far fa-question-circle iconos"></i>
	                    <h5>Preguntas Frecuentes</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-phone-volume iconos" ></i>
	                    <h5>Contáctenos</h5>
	                </a>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-12 div-enlaces">
	                <a href="https://www.facebook.com/subto.irizam"><i class="fab fa-facebook iconos-redes"></i></a>
	                <a href="https://twitter.com/CoolRodri"><i class="fab fa-twitter iconos-redes"></i></a>
	                <a href="https://www.youtube.com/channel/UCoHjLtQl55mPL7CnfX8vSAA?sub_confirmation=1"><i class="fab fa-youtube iconos-redes "></i></a>
	                <a href="https://www.instagram.com/irizam_/"><i class="fab fa-instagram iconos-redes"></i></a>
	            </div>
	        </div>  
	        <div class="row">
	        <div class="divisor"></div>
	            <p class="texto">TuCasero.com es un portal de comercio electrónico muy fácil de usar, donde podrás publicar todo lo que quieras vender y encontrarás lo que buscas entre miles de productos disponibles para tí. Las marcas comerciales y las marcas mencionadas son propiedad de sus propietarios correspondientes. El uso de este sitio implica la aceptación de los Términos y condiciones de uso, así como también de las Políticas de privacidad. TuCasero.com NO tiene responsabilidad alguna sobre los productos ofertados ni en las transacciones que ocurran entre los usuarios de este medio.</p>
	        </div>
	        <div class="row copy">
	            <div class="col-md-12 copy" >
	                <b>&copy2020 TuCasero.com | Todos los derechos reservados</b>
	                <a href=""><p>Terminos de condiciones de uso</p></a>
	                <a href=""><p>Politica de privacidad</p></a>
	            </div>
	        </div>
	    </div>
	</footer>
	<script src="<?php echo base_url(); ?>public/scripts/scriptUpdateProduct.js"></script>
	<script src="https://kit.fontawesome.com/872ac11209.js" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/scripts/scriptHead.js"></script>
	<script src="<?php echo base_url(); ?>public/scripts/scriptCarrito.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script type="text/javascript">
		if (window.history.replaceState) {
			window.history.replaceState( null, null, window.location.href );
		}
	</script>
</body>
</html>
