<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($_SESSION['recuerdame'] == 0)
 {
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Login | TuCasero</title>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<script src="<?php echo base_url(); ?>public/scripts/scriptLogin.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/loginStyle.css">
	<style>
		.image-container{
			/*no se puede utizar codigo php en un archivo .css por eso esta linea se queda aqui*/
			background: url('<?php echo base_url(); ?>/images/background.jpg') center no-repeat;
			background-size: cover;
			min-height: 100vh;
		}
	</style>	
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 d-none d-md-block image-container">		
			</div>
			<div class="col-lg-6 col-md-6 form-container">
				<div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box">
					<div class="logo mb-3 text-center">
						<img src="<?php echo base_url(); ?>/images/logo2.png" height="170" width="180">
					</div>
					<br><br>
					<?php
                      	$atributos=array('id'=>'uiForm','class'=>'ui form');
                      	echo form_open_multipart('Usuario/ValidateUser',$atributos);
                      	?>
					  	<div class="field">
					    	<input type="text" name="userName" placeholder="Nombre de Usuario">
					  	</div>
						<div class="field">
						    <input type="password" name="password" placeholder="Contraseña">
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="field">
									<div class="ui checkbox">
										<input type="checkbox" tabindex="0" id="remindMe" name="recuerdame">
									    <label>Recordarme</label>
									</div>
								</div>
								</div>
								<div class="col-md-6 text-right">
									<div class="field">
									<a style="color: #333333;" href="<?php echo site_url('Usuario/RecuperarContrasena'); ?>">¿Olvidaste tu contraseña?</a>
								</div>
							</div>
						</div>
						
						<button class="ui orange button" type="submit" id="buttonLogin">Iniciar Sesión</button><br>
						<div class="ui error message"></div>
						<div class="text-center">
					  	<?php
					  		switch ($msg)
					  		{
					  			case '1':
                                	?> <label id="lblMessage">Usuario y/o contraseña incorrectos</label> <?php
                                break;
                                default:
                                	echo "";
                                break;
					  		}
					  	?>
					  </div><br>
					<?php echo form_close(); ?>
					<div class="ui horizontal divider">O</div>
					<div class="field">
						<?php echo form_open_multipart('Usuario/Registrarse'); ?>
							<button class="ui yellow button" type="submit" id="buttonRegister">Registrarme</button>
						<?php echo form_Close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	if (window.history.replaceState) {
		window.history.replaceState( null, null, window.location.href );
	}
</script>
</html>
<?php }
else
{
	redirect('Usuario/index','refresh');
} ?>
