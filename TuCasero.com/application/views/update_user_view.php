<?php if(!$_SESSION['userSesion']) { 
	redirect('Usuario/index','refresh');			
 } 
 else{?>

	<div class="container" style="margin-bottom: 50px;">
		<!--The first part of the page-->
		<div class="row">
			<div class="col-md-12"><br>
				<nav>
					<div class="row">
						<div class="col-md-12">
							<h1>Tu Perfil</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4" style="display: flex; align-items: center;">
							<label style="margin-bottom: 0px;">Aquí podrás ver y editar los datos de tu perfil</label>
						</div>
						<div class="col-sm-4">
							<?php
								$id = crypt(crypt($_SESSION['userSesion'],'st'),'st');
								$atributos=array('id'=>'uiFormToPublic');
								echo form_open_multipart('Usuario/Usuario?id='.$id, $atributos); ?>
								<button type="submit" class="ui secondary fluid inverted button" style="border-radius: 2px;">Ver mi perfil público</button>
							<?php echo form_Close(); ?>
						</div>
					</div>
				</nav>
			</div>
		</div><br>
		<!--The four main buttons-->
		<div class="row">
			<div class="col-md-12">
				<nav>
					<div class="row">
						<div class="col-md-12" style="margin: 20px 0;">
							<div id="fourButtons" class="four ui buttons" >
							  <button class="ui button" name="updateProfileBtn" id="active" onclick="opennn('updateProfile')">PERFIL</button>
							  <button class="ui button" name="standsBtn" onclick="opennn('stands')" >STANDS</button>
							  <button class="ui button" name="favoritesBtn" onclick="opennn('favorites')" >FAVORITOS</button>
							  <button class="ui button" name="opinionsBtn" onclick="opennn('opinions')" >MIS PEDIDOS</button>
							</div>
						</div>
					</div>
				</nav>
			</div>
		</div><br>
		<!--Update user profile -->
		<div id="updateProfile" class="row">
			<div class="col-md-12">
				<main>
					<div class="row">
						<div class="col-md-12">
							<?php
				            	$atributos=array('id'=>'uiForm1','class'=>'ui form');
				                echo form_open_multipart('Usuario/UpdateUser',$atributos);
				                ?>
								<div class="card">
									<div class="row">
										<h5 style="font-weight: bold;">Información Publica</h5>
									</div><br>
									<div class="field">
										<div class="form-group row">
										    <label for="name" class="col-sm-2 col-form-label">Nombre(s)</label>
										    <div class="col-sm-7">
										      <input type="text" class="form-control-plaintext" id="name1" name="name" maxlength="60" value="<?php echo $name; ?>">
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label for="firstSurname" class="col-sm-2 col-form-label">Primer Apellido</label>
										    <div class="col-sm-7">
										      <input type="text" class="form-control" id="firstSurnamme1" name="firstSurname" maxlength="60" value="<?php echo $firstSurname; ?>">
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label for="secondSurname" class="col-sm-2 col-form-label">Segundo Apellido</label>
										    <div class="col-sm-7">
										      <input type="text" class="form-control" id="secondSurnamme1" name="secondSurname" maxlength="60" value="<?php echo $secondSurname; ?>">
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label for="phoneNumber" class="col-sm-2 col-form-label">Teléfono</label>
										    <div class="col-sm-4">
										      <input type="text" class="form-control" id="phoneNumber1" name="phoneNumber" maxlength="8" value="<?php echo $phoneNumber; ?>">
										    </div>
										</div>
									</div>
								</div><br><br>
								<div class="card">
									<div class="row">
										<h5 style="font-weight: bold;">Información Personal</h5>
									</div><br>
									<div class="field">
										<div class="form-group row">
										    <label for="dateOfBirth" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
										    <div class="col-sm-4">
										      <input type="date" class="form-control" id="dateOfBirth1" name="dateOfBirth"
										      value="<?php echo $dateOfBirth; ?>">
										    </div>
										</div>
									</div>
									<div class="field">
										<div class="form-group row">
										    <label for="phoneNumber" class="col-sm-2 col-form-label">Email</label>
										    <div class="col-sm-6">
										      <input type="text" class="form-control" id="email1" name="email" value="<?php echo $email; ?>">
										    </div>
										</div>
									</div>
									<div class="form-group row">
									    <label class="col-sm-2 col-form-label">Sexo:</label>
									    <div class="col-sm-4">
									      <select class="ui fluid dropdown" id="gender1" name="gender">
										    <option value="M" <?php if($gender == 'M') echo "selected"; ?>>Masculino</option>
										    <option value="F" <?php if($gender == 'F') echo "selected"; ?>>Femenino</option>
                            				<option value="O" <?php if($gender == 'O') echo "selected"; ?>>Otro</option>
										  </select>
									    </div>
									</div>
								</div><br><br>
								<div class="ui error message"></div>
								<div class="row">
									<div class="col-md-12 text-center">
										<button class="ui button" type="submit" id="buttonSave" style="border-radius: 2px; width: 200px;">GUARDAR</button>
									</div>
								</div><br>
								<?php
				            echo form_close();
				            ?>
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="ui button" data-toggle="modal" data-target="#exampleModal" id="btnDeleteAccount">ELIMINAR CUENTA</button>
								</div>
							</div>
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Eliminar su Cuenta</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							      	¿Estás seguro que quieres eliminar tu cuenta?<br>
							      	(Subscribe to <a href="https://www.youtube.com/channel/UCoHjLtQl55mPL7CnfX8vSAA?sub_confirmation=1">Irizam</a>)
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="ui blue button" data-dismiss="modal" id="btnCancel">Cancelar</button>
									<?php $atributos=array('id'=>'uiForm','class'=>'ui form');
									 echo form_open_multipart('Usuario/DeleteUser',$atributos); ?> 
								        <button type="submit" class="ui red button" id="btnDeleteAccount2">Eliminar</button>
									<?php echo form_Close(); ?>
							      </div>
							    </div>
							  </div>
							</div>
						</div>
					</div>
				</main>
			</div>
		</div>
		<div id="stands" class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<h4>Mis Stands:</h4>
					</div>
				</div>
				<div class="ui four doubling cards">
					<?php foreach ($listOfStands->result() as $row) { ?>
						<a href="<?php echo site_url('stand/stand?id=').$row->encryptedStandId; ?>" class="card">
							<img class="card-img-top" src="<?php echo base_url(); ?>/images/TuCaseroImages/stands/<?php echo $row->standId;?>.jpg" height="235" alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title"><?php echo $row->standName; ?></h5>
								<p class="card-text"><?php echo $row->standDescription; ?></p>
							</div>
						</a>
					<?php } ?>
					<?php if($numUserStands < $maxNumUserStands) { ?>
					<a href="<?php echo site_url('stand/registrarStand') ?>" class="card" id="addStandBtn">
						<img class="card-img-top" height="235" style="visibility: hidden;">
						<div class="card-body">
							<h5 class="card-title">
								<span class="fa fa-plus-circle fa-5x" aria-hidden="true"></span>
							</h5>
							<p class="card-text">Agregar Stand</p>
						</div>
					</a>
					<?php } ?>
				</div>
				<?php if($numNonMainUserStands > 0) { ?>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<h4>Stands compartidos conmigo:</h4>
					</div>
				</div>
				<div class="ui four doubling cards">
					<?php foreach ($listOfNonMainOwnerStands->result() as $row) { ?>
						<a href="<?php echo site_url('stand/stand?id=').$row->encryptedStandId; ?>" class="card">
							<img class="card-img-top" src="<?php echo base_url(); ?>/images/TuCaseroImages/stands/<?php echo $row->standId;?>.jpg" height="235" alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title"><?php echo $row->standName; ?></h5>
								<p class="card-text"><?php echo $row->standDescription; ?></p>
							</div>
						</a>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<div id="favorites" class="row">
			<div class="col-md-12">
				<div class="ui four doubling cards">
					<?php foreach ($listOfProductsFavorites->result() as $row) { ?>
						<a href="<?php echo site_url('Producto/Producto?id='.$row->encryptedProductId);?>" class="card">
							<img class="card-img-top" src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" height="235" alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title"><?php echo $row->productName; ?></h5>
								<p class="card-text"><?php echo $row->cost; ?> bs.</p>
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
		<div id="opinions" class="row">
			<?php if($userOrders->num_rows() > 0) { ?>
			<div class="col-md-12">
				<table class="ui selectable inverted table" style="margin-bottom: 50px;">
					<thead>
						<tr>
							<th>Imagen Producto</th>
							<th>Nombre Producto</th>
							<th>Mensaje</th>
							<th>Total</th>
							<th>Stand</th>
							<th>Fecha De Petición</th>
							<th>Cancelar Pedido</th>     
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach ($userOrders->result() as $row) 
					{?>
						<tr>					
							<th>  
								<a href="<?php echo base_url()?>Producto/Producto?id=<?php echo $row->encryptedProductId ?>">
									<img src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" width="75" height="75"alt="Card image cap">
								</a> 
							</th>
							<th>  
								<a href="<?php echo base_url()?>Producto/Producto?id=<?php echo $row->encryptedProductId ?>" style="color:#FFF;">
									<?php echo $row->productName; ?>
								</a> 
							</th>
							<th><?php echo $row->orderMessage; ?></th>
							<th><?php echo $row->total; ?> Bs.</th>
							<th>  
								<a href="<?php echo base_url()?>Stand/Stand?id=<?php echo $row->encryptedStandId ?>" style="color:#FFF;">
									<?php echo $row->standName; ?>
								</a> 
							</th>
							<th><?php echo $row->orderDate; ?> </th>
							<th>
								<?php echo form_open_multipart('Producto/cancelarPedido') ?>
									<input type="hidden" name="ordenId" value="<?php echo $row->orderID ?>">
									<input class="btn btn-danger" type="submit" value="Cancelar">
								<?php echo form_close() ?>
							</th>

						</tr>                   
					<?php
					}?>
					</tbody>
				</table>
			</div>
			<?php } else { ?>
				<h3>No realizaste ningún pedido todavía. Vuelva cuando ya realizó algún pedido.</h3>
			<?php } ?>
		</div>
	</div>
<?php }?>