<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-secondary" >
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Producto/Search"><i class="home icon"></i>Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Buscar</li>
        <li class="breadcrumb-item active" aria-current="page"><?php if(isset($id)) echo $id;if(isset($textoBusqueda)) echo $textoBusqueda; ?></li>
    </ol>
    </nav>
</div>
<div class="container" style="margin-bottom: 50px;">
    <div class="row">
        <!-- productos recomendados -->
        
        <div class="col-md-3 recomendados">
            <div class="card bg-secondary" style="width: 18rem; padding:2px;">
                <div class="card-header" style="color:white">Recomendados</div>
                <div class="ui link cards" >
                    <div class="card">
                        <div class="image">
                        <img src="https://3.bp.blogspot.com/-JBh2OWZSH8A/TiX4r1aUqqI/AAAAAAAAABk/qeZ4ndAjYLA/s1600/OFERTAS.png">
                        </div>
                        <div class="content">
                        <div class="header">Producto recomendado</div>
                        <div class="meta">
                            <a>Yanbal</a>
                        </div>
                            
                        </div>
                        <div class="extra content">
                        <span class="right floated">
                            <div class="ui vertical animated button" tabindex="0">
                                <div class="hidden content">Carrito</div>
                                <div class="visible content">
                                    <i class="shop icon"></i>
                                </div>
                            </div>
                        </span>
                        <span>
                        <i class="star icon"></i>
                        <i class="star icon"></i>
                        <i class="star icon"></i>
                        <i class="star icon"></i>
                        <i class="star outline icon"></i>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="ui link cards" >
                    <div class="card">
                        <div class="image">
                        <img src="https://i.pinimg.com/originals/f6/5c/b2/f65cb2e51814e7f2c2a4a26af3e61a13.png">
                        </div>
                        <div class="content">
                        <div class="header">Producto recomendado</div>
                        <div class="meta">
                            <a>Yanbal</a>
                        </div>
                            
                        </div>
                        <div class="extra content">
                        <span class="right floated">
                            <div class="ui vertical animated button" tabindex="0">
                                <div class="hidden content">Carrito</div>
                                <div class="visible content">
                                    <i class="shop icon"></i>
                                </div>
                            </div>
                        </span>
                        <span>
                        <i class="star icon"></i>
                        <i class="star icon"></i>
                        <i class="star icon"></i>
                        <i class="star icon"></i>
                        <i class="star outline icon"></i>
                        </span>
                        </div>
                    </div>
                </div>

            </div>
            
        </div>
        
        <!-- fin productos recomendados -->
        <!-- criterios de busqueda -->
        <div class="col-md-9">
            <h1 class="ui dividing header" style="text-align: center; margin:20px">
                Buscar - <?php if(isset($id)) echo $id; if(isset($textoBusqueda)) echo $textoBusqueda; ?>
                <a class="anchor" id="types" ></a>
            </h1> <!-- TO-DO: arregar el paso de la variable buscada -->
            <?php
             $atributos=array('class'=>'form','method'=>'get');
             echo form_open_multipart('Producto/Search',$atributos);?>
                <h4 style="margin-top: 20px;">Criterios de búsqueda</h4>
                <div class="row">
                    <div class="col-sm-4" >
                        <input class="form-control" id="textoBusqueda" type="text" name="textoBusqueda" value="<?php if(isset($_GET['textoBusqueda'])) echo $_GET['textoBusqueda']; ?>" placeholder="Buscar..." style="margin-top: 20px;">
                    </div>
                    <div class="col-sm-3" style="margin-top: 20px;" >
                        <select class="form-control" name="categoria">
                                <option>todas las categorías</option>
                            <?php foreach ($categories->result() as $row) { ?>                               
                                 <option <?php if(isset($_GET['categoria'])){
                                    if ($_GET['categoria'] == $row->categoryName) { ?> selected <?php } } ?>>
                                    <?php echo $row->categoryName; ?>
                                </option>
                            <?php } ?>                          
                        </select>
                    </div>
                    <div class="col-sm-5" style="margin-top: 20px;" >  
                        <div class="ui multiple dropdown" style="margin:0px;">
                            <input type="hidden" name="filters">
                            <i class="filter icon"></i>
                            <span class="text">Filtrar Tags</span>
                            <div class="menu">
                                <div class="ui icon search input">
                                    <i class="search icon"></i>
                                    <input type="text" placeholder="buscar tags...">
                                </div>
                                <div class="divider"></div>
                                <div class="header">
                                    <i class="tags icon"></i>
                                    Nombre de los Tags
                                </div>
                                <div class="scrolling menu" style="width: 200px;
                                            height: 300px;
                                            overflow: scroll;">
                                <?php foreach ($tags->result() as $row) { ?>
                                    
                                        <div class="item" data-value="<?php echo $row->tagName; ?>">
                                            <div class="ui red empty circular label"></div>
                                            <?php echo $row->tagName; ?>
                                        </div>
                                    
                                <?php } ?>   
                                </div>                            
                            </div>
                        </div>                 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="field" style="margin-top: 10px;">
                            <div class="ui checkbox">
                            <input type="checkbox" name="checkDesc" <?php if(isset($_GET['checkDesc'])) { ?> checked="true" <?php } ?>>
                            <label>Buscar en las descripciones</label>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    <input class="form-control bg-secondary" style="color: white;" type="submit" value="Buscar">
                    </div>
                </div>
            <?php echo form_close(); ?>       
            <!-- fin criterios de busqueda -->
            <h2 class="ui dividing header" style="margin:20px">Resultados<a class="anchor" id="types" ></a></h2>
            <!-- desde aqui se hizo cambios para que funcione el carrito -->
            <!-- respuesta a la busqueda llena de productos -->   
            <div class="row" id="productosRespuesta">
                <input type="hidden" id="direccionCarrito" value="<?php echo base_url(); ?>Producto/carrito">
            <?php if (isset($productos) && $productos!=false) {
                if(count($productos) == 0 ) { echo "<h1>sin resultados</h1>"; }
                $i=1;
                foreach ($productos as $row) { ?>                                                                
                    <div class="col-md-4" style="margin: 0px; padding:0px;">           
                            <div class="ui link cards" style="width:100%;">
                                <div class="card" style="height: 420px ; margin-bottom:30px">
                                    <div class="image">
                                        <a href="<?php echo base_url()?>Producto/Producto?id=<?php echo $row->encryptedProductId ?>">
                                            <img height="240px" width="100%"
                                                src="<?php echo base_url()?>/images/TuCaseroImages/products/<?php echo $row->productId; ?>/<?php echo $row->productId;?>.jpg">
                                        </a>
                                        <input type="hidden" id="direccionImagen<?php echo $i ?>" value="<?php echo base_url()?>/images/TuCaseroImages/products/<?php echo $row->productId; ?>/<?php echo $row->productId;?>.jpg">
                                    </div>
                                    <div class="content" onclick="window.location.href = '<?php echo base_url()?>Producto/Producto?id=<?php echo $row->encryptedProductId ?>';">
                                        <div class="header"><?php echo $row->productName; ?></div>
                                        
                                        <div class="meta">
                                            <p>Precio: <?php echo $row->cost ?>Bs.</p>
                                            <input type="hidden" value="<?php echo $row->productName; ?>" id="nombreProducto<?php echo $i ?>">
                                            <input type="hidden" value="<?php echo $row->cost; ?>" id="precio<?php echo $i ?>" name="precio">
                                            <input type="hidden" value="<?php echo $_SESSION['userSesion']; ?>" id="userId">
                                            <input type="hidden" value="<?php echo $row->productId; ?>" id="productId<?php echo $i ?>" name="id">
                                        </div>
                                        <div class="description">
                                            <?php echo $row->description; ?> <br>
                                            <b>Stock</b> <?php echo $row->quantity; ?> u.
                                            <input type="hidden" value=" <?php echo $row->description; ?>" id="descripcion<?php echo $i ?>" name="descripcion">
                                            <input type="hidden" value=" <?php echo $row->quantity; ?>" id="stock<?php echo $i ?>" name="stock">
                                        </div>
                                    </div>
                                    <?php if($_SESSION['userSesion']) { ?>
                                        <div class="extra content">
                                        <span class="right floated">
                                            <div class="ui vertical" tabindex="0" >
                                                <button class="btn btn-success" type="submit" id="<?php echo $i ?>">
                                                    <i class="shop icon enviar" ></i> 
                                                </button>                                                                      
                                            </div>
                                        </span>
                                        <div class="row">
                                            <div class="col col-md-4" style="padding:0px">
                                                <label style="margin-top:20%" >Cantidad</label>                                          
                                            </div>
                                            <div class="col col-md-6" style="padding:0px">
                                                <input class="form-control " type="number" min="1" max="<?php echo $row->quantity; ?>" required id="cantidad<?php echo $i ?>" name="cantidad" value="1">
                                            </div>
                                        </div>
                                        </div>
                                    <?php } ?> 
                                </div>
                            </div>
                    </div>
                    <?php $i++; ?> 
                <?php } 
                    } 
                    else{
                        echo '<h3 style="margin-left:30px;">lo sentimos no hay resultados para tu busqueda :(</h3>';
                    }
                    ?>                        
            </div>                    
        </div><!-- fin a la respuesta de busqueda llena de productos -->
        <!-- desde aqui terminan los cambios para que funcione el carrito -->
        
    </div>
    <div class="row justify-content-center">
        <div>
            <section class="paginacion">
                <ul>
                    <?php echo $this->pagination->create_links()?>
                    </ul>
            </section>
        </div>
    </div>
</div>