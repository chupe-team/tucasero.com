<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php foreach ($productInfo1->result() as $row) { ?>
	<title><?php echo $row->productName; ?> | TuCasero</title>
	<?php } ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="<?=base_url()?>images/favicon.ico" type="image/ico">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/SemanticUI/semantic.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>public/SemanticUI/semantic.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.2.0/css/ionicons.min.css" integrity="sha256-F3Xeb7IIFr1QsWD113kV2JXaEbjhsfpgrKkwZFGIA4E=" crossorigin="anonymous" />

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/publicProfileProductStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/styleHead.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/footerStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/carritoStyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/faq.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/paginacionStyle.css">
	<style type="text/css">
		@media screen and (max-width: 768px) {
			#imgProduct {
				height: 300px;
			}
			#btnDeleteProduct {
				margin-top: 10px;
			}
		}
	</style>
</head>
<body style="background-color: #E8E8E8;">

<nav class="menu" id="menu">
		<div class="contenedor contenedor-botones-menu">
			<button id="btn-menu-barras" class="btn-menu-barras"><i class="fas fa-bars"></i></button>
			<button id="btn-menu-cerrar" class="btn-menu-cerrar"><i class="fas fa-times"></i></button>
		</div>

		<div class="contenedor contenedor-enlaces-nav">
			<a href="<?php echo site_url('Producto/Search') ?>">
				<img id="logoBienChevere" height="60px" src="<?php echo base_url(); ?>/images/logo3.png">
			</a>

			<div class="enlaces ml-auto">
				<?php if($_SESSION['userSesion']) { ?>
					<input type="hidden" id="MostrarCarrito" value="<?php echo base_url();?>index.php/Producto/mostrarCarrito">     		
						<a class="dropdown-toggle" id="carritoMostrarDatos"  data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="true"><i id="dropdownCarrito" class="fas fa-cart-plus" style="color: yellow;" ></i></a>
						<div class="dropdown-menu" aria-labelledby="dropdownCarrito" id="resultado">
							<div class="contenedorCarrito" id="contenedorCarrito">
														
							</div>
							<div class="botones-carrito" style="text-align: center;">
								<a class="btn btn-danger btn-lg" href="" id="vaciarCarrito">vaciar</a>
								<input id="direccionVaciarCarrito" type="hidden" value="<?php echo base_url()?>index.php/Producto/vaciarCarrito">
								<a class="btn btn-success btn-lg active"  href="<?php echo base_url(); ?>index.php/Producto/visitarCarrito">ir de compras</a>
							</div>
						</div>
				<?php } ?>	
			
				<?php if($_SESSION['userSesion'] == null) { ?>
					<a href="<?php echo base_url(); ?>Usuario/IniciarSesion">Iniciar Sesion</a>
					<a href="<?php echo base_url(); ?>Usuario/Registrarse">Registrarse</a>
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/TuZona">Mi Zona</a>
				<?php } ?>
				<?php if($_SESSION['userRole'] == 1) { ?>
					<div class="dropdown">
					  <a class="dropbtn" >Control de Datos</a>
					  <div class="dropdown-content">
					  	<a href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados" style="color: black;">Lista de Usuarios</a>
					    <a href="<?php echo base_url(); ?>Stand/ListaStands" style="color: black;">Lista de Stands</a>
					    <a href="<?php echo base_url(); ?>Producto/ListaProductos" style="color: black;">Lista de Productos</a>
					  </div>
					</div>
					<!--<div class="dropdown">
						<a class="dropa" href="<?php echo base_url(); ?>Usuario/UsuariosHabilitados">Lista de Usuarios</a>
						<div class="dropdown-content">
							<a href="#">Lista de Productos</a>
							<a href="#">Lista de Stands</a>
						</div>
					</div> -->
					
				<?php } ?>
				<?php if($_SESSION['userSesion']) { ?>
					<a href="<?php echo base_url(); ?>Usuario/CerrarSesion">Cerrar Sesion</a>
				<?php } 
				if($_SESSION['userRole'] == 2 && $_SESSION['isPremium'] == 0) { ?>
					<a class="btn btn-danger" href="<?php echo base_url(); ?>Usuario/premium" style="border-radius: 2px;" role="button"><b style="color: white; ">VOLVERSE PREMIUM</b></a>
				<?php } ?>
			</div>
		</div>
	</nav>
	<div class="container" style="background-color: #E8E8E8;">
		<div class="row justify-content-center h-100">
            <div class="col-sm-8">
                <div class="card" style="margin-top: 40px;">
				  	<div class="card-body">
				  		<div class="row">
				  			<div class="col-sm-9">
				  				<?php
				            	foreach ($productInfo3->result() as $row) 
				            	{ 
				            		$productId = $row->productId;?>
				            		<div class="row">
				            			<div class="col-md-12">
				            				<img src="https://w7.pngwing.com/pngs/81/570/png-transparent-profile-logo-computer-icons-user-user-blue-heroes-logo-thumbnail.png" class="rounded-circle" width="50" height="50">	
											<a style="font-size: 20px; color: #333333;" href="<?php echo site_url('Usuario/Usuario?id='.$row->encryptedUserId);?>">
												<?php echo $row->name.' '.$row->firstSurname.' '.$row->secondSurname.' ('.$row->username.')'; ?>
											</a><br>
				            			</div>
				            		</div>
								<?php
								}
								?>
				  			</div>
				  			<div class="col-md-3 text-right">
							  <?php if($_SESSION['userSesion']) { ?>
								<?php $atributos=array('id'=>'uiForm','class'=>'ui form');
									echo form_open_multipart('Usuario/ProductAddFavorite'); ?>
									<input type="text" hidden name="productId" value="<?php echo  $row->productId; ?>"> 
		            				<button class="ui vertical animated button" id="btnProductAddFavorites" tabindex="0" style="background-color: #CCCCCC;">
									  <div class="hidden content" style="color: #B90504;">Favorito</div>
									  <div class="visible content">
									    <i class="heart icon" style="color: #B90504;"></i>
									  </div>
									</button>
								<?php echo form_Close(); ?>
							  <?php } ?>
	            			</div>
				  		</div>
						<?php
						foreach ($productInfo2->result() as $row) 
						{ 
							$StandId = $row->standId;
							?>
							<a style="font-size: 18px; color: #333333; margin-left: 53px;" href="<?php echo site_url('Stand/Stand?id='.$row->encryptedStandId);?>">
								<?php echo $row->standName; ?>
							</a>
						<?php 
						} ?>
						<hr style="background-color: #990100;">
					  	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
						  <div class="carousel-inner" style="border-radius: 5px;">
						    <div class="carousel-item active">
						      <?php
								foreach ($productInfo1->result() as $row) 
							{ ?>
					      		<img class="d-block w-100" src="<?php echo base_url(); ?>/images/TuCaseroImages/products/<?php echo $row->productId;?>/<?php echo $row->productId;?>.jpg" alt="First slide" height="550" style="background-size: contain;"  id="imgProduct">
						 	 <?php
							}?>
						    </div>					    
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
						<?php
						foreach ($productInfo1->result() as $row) 
						{ ?>
							<h5 class="card-title" style="font-size: 32px; margin-top: 10px;"><?php echo $row->productName; ?></h5>
							<h6 class="card-subtitle mb-2 " style="font-size: 28px;">Precio: <?php echo $row->cost; ?> Bs.</h6>
							<hr>
							<label style="font-size: 20px;">Descripción: <?php echo $row->description; ?></label><br>
							<label style="font-size: 18px;">Cantidad: <?php echo $row->quantity; ?></label>
							<?php $ProductId = $row->productId; 
						}?>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
				  	<?php
				  	$userOwner = false;
					foreach ($productInfo3->result() as $row) {
						if($row->userId == $_SESSION['userSesion']){
							$userOwner = true;
						}
					}
					if($userOwner == true){?>
					<div class="col-md-6">
						<?php 
						echo form_open_multipart('Producto/ActualizarProducto'); ?>
							<?php foreach ($productInfo1->result() as $row) { ?>
								<input type="text" name="productIdU" hidden value="<?php echo $row->productId; ?>">
							<?php } ?>
							<input type="text" name="standIdU" hidden value="<?php echo $StandId; ?>">
							<button class="ui button" id="btnUpdateProduct">Editar</button>
						<?php echo form_close(); ?>
					</div>
					<div class="col-md-6">
						<button class="ui button" id="btnDeleteProduct" data-toggle="modal" data-target="#exampleModal" style="width: 100%;">Eliminar</button><br>
						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
							    <div class="modal-content">
							      	<div class="modal-header">
							        	<h5 class="modal-title" id="exampleModalLabel">Eliminar Producto</h5>
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							      	<div class="modal-body">
							      		¿Está seguro que desea eliminar este producto?<br>
							      	</div>
							      	<div class="modal-footer">
							        	<button type="button" class="ui blue button" data-dismiss="modal" id="btnCancel">Cancelar</button>
										<?php
											echo form_open_multipart('Producto/DeleteProduct'); ?>
											<?php foreach ($productInfo1->result() as $row) { ?>
											<input type="text" hidden name="productIdD" value="<?php echo $row->productId; ?>">
											<?php } ?>
											<input type="text" hidden name="standIdD" value="<?php echo $StandId; ?>"> 
											<button type="submit" class="ui red button" id="btnDeleteProduct2">Eliminar</button>
										<?php echo form_Close(); ?>
							      	</div>
							    </div>
							</div>
						</div>
					</div>
					<?php
					}?>
				</div><br>
            </div>
        </div>
	</div>
	<footer style="padding-bottom: 30px; margin-top: 30px;">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('Producto/Search') ?>">
	                    <i class="fas fa-home iconos"></i>
	                    <h5>Inicio</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-money-check-alt iconos"></i>
	                    <h5>Formas de Pago</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="<?php echo site_url('preguntas') ?>">
	                    <i class="far fa-question-circle iconos"></i>
	                    <h5>Preguntas Frecuentes</h5>
	                </a>
	            </div>
	            <div class="col-md-3 div-enlaces">
	                <a href="">
	                    <i class="fas fa-phone-volume iconos" ></i>
	                    <h5>Contáctenos</h5>
	                </a>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-12 div-enlaces">
	                <a href="https://www.facebook.com/subto.irizam"><i class="fab fa-facebook iconos-redes"></i></a>
	                <a href="https://twitter.com/CoolRodri"><i class="fab fa-twitter iconos-redes"></i></a>
	                <a href="https://www.youtube.com/channel/UCoHjLtQl55mPL7CnfX8vSAA?sub_confirmation=1"><i class="fab fa-youtube iconos-redes "></i></a>
	                <a href="https://www.instagram.com/irizam_/"><i class="fab fa-instagram iconos-redes"></i></a>
	            </div>
	        </div>  
	        <div class="row">
	        <div class="divisor"></div>
	            <p class="texto">TuCasero.com es un portal de comercio electrónico muy fácil de usar, donde podrás publicar todo lo que quieras vender y encontrarás lo que buscas entre miles de productos disponibles para tí. Las marcas comerciales y las marcas mencionadas son propiedad de sus propietarios correspondientes. El uso de este sitio implica la aceptación de los Términos y condiciones de uso, así como también de las Políticas de privacidad. TuCasero.com NO tiene responsabilidad alguna sobre los productos ofertados ni en las transacciones que ocurran entre los usuarios de este medio.</p>
	        </div>
	        <div class="row copy">
	            <div class="col-md-12 copy" >
	                <b>&copy2020 TuCasero.com | Todos los derechos reservados</b>
	                <a href=""><p>Terminos de condiciones de uso</p></a>
	                <a href=""><p>Politica de privacidad</p></a>
	            </div>
	        </div>
	    </div>
	</footer>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://kit.fontawesome.com/872ac11209.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptHead.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptCarrito.js"></script>
<script src="<?php echo base_url(); ?>public/scripts/scriptPremiumUser.js"></script>
</html>
