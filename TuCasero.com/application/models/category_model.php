<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category_model extends CI_Model {

	public function insertCategory($data) {
		$this->db->insert('category', $data);
	}

	public function getAllCategories() {
		$this->db->select('*');
		$this->db->from('category');
		return $this->db->get();
	}

	public function updateProduct($categoryID, $data) {
		$this->db->where('categoryId', $categoryID);
		$this->db->update('category', $data);
	}
	
	public function deleteProduct($categoryID, $data){
		$this->db->where('categoryId', $categoryID);
		$this->db->update('category', $data);
	}
}?>