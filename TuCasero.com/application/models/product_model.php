<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model {

	public function insertProduct($data, $tags) {
		$this->db->trans_begin();
		$ID1 = $this->db->query("SELECT ifnull(MAX(productId)+1,1) AS ProductMaxID FROM product");
		foreach ($ID1->result() as $row)
		{
			$encryptedProductId = crypt(crypt(crypt(crypt($row->ProductMaxID,'st'),'st'),'st'),'st');
			$data['encryptedProductId'] = $encryptedProductId;
			$IDP=$row->ProductMaxID;
		}

		$this->db->select('st.productTypeId');
		$this->db->from('stand_type st');
		$this->db->join('stand s', 's.standTypeId = st.standTypeId');
		$this->db->where('s.standId', $data['standId']);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			$data['productTypeId'] = $row->productTypeId;
		}

		$this->db->insert('product', $data);
		$ID1 = $this->db->query("SELECT ifnull(MAX(productId),0) AS ProductMaxID FROM product");
		foreach ($ID1->result() as $row)
		{
			$IDP=$row->ProductMaxID;
		}

		for ($i=0; $i < count($tags); $i++) {  //['tags']
			$tagName = $tags[$i];
			$search=$this->db->query("SELECT tagId FROM tag WHERE tagName = '$tagName'");
			if ($search->num_rows() == 0) {
				$tag['tagName'] = $tags[$i];
				$this->db->insert('tag',$tag);
				$ID2 = $this->db->query("SELECT ifnull(MAX(tagId),0) AS TagMaxID FROM tag");
				foreach ($ID2->result() as $row)
				{
					$IDL = $row->TagMaxID;
				}
				$insertLP['tagId']=$IDL;
				$insertLP['productId']=$IDP;
				$this->db->insert('product_tag',$insertLP);
			}
			else
			{
				foreach ($search->result() as $row)
				{
					$tagId1 = $row->tagId;
				}
				$insertLP['tagId']=$tagId1;
				$insertLP['productId']=$IDP;
				$this->db->insert('product_tag',$insertLP);
			}
		}
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $encryptedProductId;
	}

	public function searchProduct($filter) {
		$this->db->select('*');
		$this->db->from('product');
		$this->db->like('productName', $filter);
		$this->db->or_like('description', $filter);
		$this->db->or_like('quantity', $filter);
		$this->db->or_like('cost', $filter);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getProduct($ProductID) {
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('productId', $ProductID);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getProduct2($State) {
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('status',$State);
		return $this->db->get();
	}

	public function updateProduct($ProductID, $data, $tags, $tagsFromDB) {
		$this->db->where('productId', $ProductID);
		$this->db->update('product', $data);

		//return gettype($tags);

		for ($i = 0; $i < count($tags); $i++) {
			if (!in_array($tags[$i], $tagsFromDB)) {
				$this->db->select('tagId');
				$this->db->from('tag');
				$this->db->where('tagName', $tags[$i]);
				$query=$this->db->get();
				if($query->num_rows() > 0) { //tag existe en base de datos, pero no asociado con producto
					foreach($query->result() as $row) {
						$pt_data["productId"] = $ProductID;
						$pt_data["tagId"] = $row->tagId;
						$this->db->insert('product_tag', $pt_data);
					}
				}
				else { //tag no existe en base de datos
					$t_data["tagName"] = $tags[$i];
					$this->db->insert('tag', $t_data);

					$this->db->select('tagId');
					$this->db->from('tag');
					$this->db->where('tagName', $tags[$i]);
					$query=$this->db->get();

					foreach($query->result() as $row) {
						$pt_data["productId"] = $ProductID;
						$pt_data["tagId"] = $row->tagId;
						$this->db->insert('product_tag', $pt_data);
					}
				}
			}
		}
		for ($i = 0; $i < count($tagsFromDB); $i++) {
			if (!in_array($tagsFromDB[$i], $tags)) {
				//tag existía en base de datos, pero ya no
				$this->db->select('tagId');
				$this->db->from('tag');
				$this->db->where('tagName', $tagsFromDB[$i]);
				$query=$this->db->get();
				foreach($query->result() as $row) {
					$TagID = $row->tagId;
				}
				$this->db->where('productId', $ProductID);
				$this->db->where('tagId', $TagID);
				$this->db->delete('product_tag');
			}
		}

		$encryptedProductId = $this->db->query("SELECT encryptedProductId FROM product WHERE productId = '$ProductID'");
		foreach ($encryptedProductId->result() as $row) {
			return $row->encryptedProductId;
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
		}
	}

	public function deleteProduct($ProductID, $data){
		$this->db->where('productId', $ProductID);
		$this->db->update('product', $data);
		$encryptedStandId = $this->db->query("SELECT s.encryptedStandId FROM product p
			INNER JOIN stand s ON s.standId = p.standId WHERE p.productId = '$ProductID'");
		foreach ($encryptedStandId->result() as $row)
		{
			return $row->encryptedStandId;
		}
		return 0;
	}

	public function deleteProduct2($ProductID,$data){
		$this->db->where('productId', $ProductID);
		$this->db->update('product', $data);
	}

	public function selectProducts()
	{
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getProductFromProduct($ProductID) {
		$this->db->select('productId, productName, description, quantity, cost');
		$this->db->from('product');
		$this->db->where('encryptedProductId', $ProductID);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getStandFromProduct($ProductID) {
		$this->db->select('p.standId, s.encryptedStandId, s.standName, s.standDescription');
		$this->db->from('product p');
		$this->db->join('stand s', 's.standId = p.standId');
		$this->db->where('p.encryptedProductId', $ProductID);
		$this->db->where('p.status', 1);
		return $this->db->get();
	}

	public function getUserFromProduct($ProductID) {
		$this->db->select('so.userId, u.encryptedUserId, u.name, u.firstSurname, u.secondSurname, u.username,p.productId,u.userId');
		$this->db->from('product p');
		$this->db->join('stand_owner so', 'so.standId = p.standId');
		$this->db->join('user u', 'u.userId = so.userId');
		$this->db->where('p.encryptedProductId', $ProductID);
		$this->db->where('p.status', 1);
		return $this->db->get();
	}
	
	public function findProduct($ProductID){
		$this->db->select('*');
		$this->db->from('product');
		$this->db->where('encryptedProductId', $ProductID);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getTagsProduct($ProductID){ //public function selectLabelsProduct($ProductID){
		$this->db->select('*');
		$this->db->from('tag t');
		$this->db->join('product_tag pt', 'pt.tagId = t.tagId');
		$this->db->where('pt.productId', $ProductID);
		return $this->db->get();
	}
	public function getTags(){
		$this->db->select('*');
		$this->db->from('tag');
		return $this->db->get();
	}
	public function getCategories(){
		$this->db->select('*');
		$this->db->from('category');
		return $this->db->get();
	}
	// aqui enpieza las busquedas de los productos
	public function SearchWithDescription($data){
		$this->db->select('*');
		$this->db->from('product p');
		$this->db->where('status',1);
		$this->db->limit($data["limit_per_page"],$data["start_index"]);
		$this->db->like("CONCAT(p.productName,' ',p.description)",$data['textoBusqueda']);
		$resultado=$this->db->get();
		if ($resultado->num_rows()>0) {
			foreach ($resultado->result() as $row) 
			{
				$respuesta[] = $row;
			}
			Return $respuesta;
		}
		return false;
		
	}
	public function cantSearchWithDescription($data){
		$this->db->select('*');
		$this->db->from('product p');
		$this->db->where('status',1);
		$this->db->like("CONCAT(p.productName,' ',p.description)",$data['textoBusqueda']);
		return $this->db->get();		
    }
	public function SearchWithCategory($data){

		$this->db->select('*');
		$this->db->from('product p');
		$this->db->limit($data["limit_per_page"],$data["start_index"]);
		$this->db->like('p.productName',$data['textoBusqueda']);
		$this->db->join('category c','p.categoryId=c.categoryId');
		$this->db->where('c.categoryName',$data['categoria']);
		$this->db->where('status',1);
		$resultado=$this->db->get();
		if ($resultado->num_rows()>0) {
			foreach ($resultado->result() as $row) 
			{
				$respuesta[] = $row;
			}
			Return $respuesta;
		}
		return false;
	}
	public function cantSearchWithCategory($data){

		$this->db->select('*');
		$this->db->from('product p');
		$this->db->like('p.productName',$data['textoBusqueda']);
		$this->db->join('category c','p.categoryId=c.categoryId');
		$this->db->where('c.categoryName',$data['categoria']);
		$this->db->where('status',1);
		return $this->db->get();
	}
	public function SearchWithCategoryDescription($data){
		$this->db->select('*');
		$this->db->from('product p');
		$this->db->where('status',1);
		$this->db->limit($data["limit_per_page"],$data["start_index"]);
		$this->db->like("CONCAT(p.productName,' ',p.description)",$data['textoBusqueda']);	
		$this->db->join('category c','p.categoryId=c.categoryId');	
		$this->db->where('c.categoryName',$data['categoria']);
		$resultado=$this->db->get();
		if ($resultado->num_rows()>0) {
			foreach ($resultado->result() as $row) 
			{
				$respuesta[] = $row;
			}
			Return $respuesta;
		}
		return false;
		
	}
	public function cantSearchWithCategoryDescription($data){
		$this->db->select('*');
		$this->db->from('product p');
		$this->db->where('status',1);
		$this->db->like("CONCAT(p.productName,' ',p.description)",$data['textoBusqueda']);	
		$this->db->join('category c','p.categoryId=c.categoryId');	
		$this->db->where('c.categoryName',$data['categoria']);
		return $this->db->get();
		
	}
	public function Search($data){		
		$this->db->select('*');
		$this->db->from('product');	
		$this->db->where('status',1);
		$this->db->limit($data["limit_per_page"],$data["start_index"]);		
		$this->db->like("productName",$data['textoBusqueda']);		
		$resultado=$this->db->get();
		if ($resultado->num_rows()>0) {
			foreach ($resultado->result() as $row) 
			{
				$respuesta[] = $row;
			}
			Return $respuesta;
		}
		return false;
		
	}
	public function cantSearch($data){
		$this->db->select('*');
		$this->db->from('product');	
		$this->db->where('status',1);
		$this->db->like("productName",$data['textoBusqueda']);	
		return $this->db->get();
	}
	// aqui termina las busquedas de los productos	

	public function getProductId($encryptedProductId){
		$this->db->select('productId AS idProduct');
		$this->db->from('product');
		$this->db->where('encryptedProductId', $encryptedProductId);
		$idProducts = $this->db->get();
		foreach($idProducts->result() as $row) {
			return $row->idProduct;
		}
	}
	//aqui insertas el producto del carrito a la base de datos
	public function insertarPedido($data){
		$this->db->insert("order",$data);
	
	}

	public function cancelarOrden($orderId, $data) {
		$this->db->where('orderId', $orderId);
		$this->db->update('order', $data);
	}

	public function obtener12ProductosAleatorios() {
		$this->db->select('*');
		$this->db->from('product AS pro');
		$this->db->order_by('rand()');
		$this->db->limit(12);
		return $this->db->get();
	}

	public function selectProductsByMonths() {
		$this->db->select('Month(createDate) AS mesP, COUNT(productId) AS totalProducts');
		$this->db->from('product');	
		$this->db->where('createDate BETWEEN "2020-01-01" AND "2020-12-31"');
		$this->db->group_by('Month(createDate)');
		$this->db->order_by('1');	
		return $this->db->get();
	}

	public function AddProductToCart($data)
	{
		$this->db->where('productId',$data['productId']);
		$this->db->where('userId',$data['userId']);
		$this->db->select('*');
		$this->db->from('cart');		
		$resultado=$this->db->get()->num_rows();
		if($resultado>0){
			$this->db->where('productId',$data['productId']);
			$this->db->where('userId',$data['userId']);
			$this->db->update('cart',$data);
			
		}
		else{
			$this->db->insert("cart",$data);
		}
		
	}
	public function showCart($data){
		
		$this->db->select('P.productName,P.cost,C.image,C.quantity,P.description,C.cartId,P.productId');
		$this->db->from('cart C');
		$this->db->where('C.userId',$data['userId']);
		$this->db->join('product P','C.productId=P.productId');
		return $this->db->get();		
	}
	public function destroyCart($data){
		$this->db->where('userId',$data['userId']);
		$this->db->delete('cart');
	}
	public function deleteItemCart($data){
		$this->db->where('cartId',$data['cartId']);
		$this->db->delete('cart');
	}
	public function updateStatus($orderId, $data) {
		$this->db->where('orderId', $orderId);
		$this->db->update('order', $data);
	}
}?>