<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stand_model extends CI_Model {

	public function selectStandFromUser($UserID, $isMainOwner){
		$this->db->select('s.standId, s.encryptedStandId, s.standName, s.standDescription, so.userId');
		$this->db->from('stand AS s');
		$this->db->join('stand_owner AS so', 's.standId = so.standId');
		$this->db->join('user AS u', 'u.userId = so.userId');
		$this->db->where('so.userId', $UserID);
		$this->db->where('so.isMainOwner', $isMainOwner);
		$this->db->where('s.status', 1);
		$this->db->where('u.status', 1);
		return $this->db->get();
	}

	public function insertStand($data) {
		$maxStandId = $this->db->query("SELECT ifnull(MAX(standId)+1,1) AS maxStandId FROM stand");
		foreach ($maxStandId->result() as $row) {
			$encryptedStandId = crypt(crypt(crypt($row->maxStandId,'st'),'st'),'st');
			$data['encryptedStandId'] = $encryptedStandId;
			$standID = $row->maxStandId;
		}
		$this->db->select('ur.standTypeId AS standTypeId');
		$this->db->from('user u');
		$this->db->join('user_role ur', 'u.roleId = ur.userRoleId');
		$this->db->where('u.userId', $_SESSION['userSesion']);
		$standTypeId = $this->db->get();
		foreach ($standTypeId->result() as $row) {
			$data['standTypeId'] = $row->standTypeId;
		}
		$this->db->insert('stand', $data);
		$standOwner['standId'] = $standID;
		$standOwner['userId'] = $_SESSION['userSesion'];
		$standOwner['isMainOwner'] = 1;
		$this->db->insert('stand_owner', $standOwner);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
			return $encryptedStandId;
		}
		return 0;
	}

	public function searchStand($filter) {
		$this->db->select('*');
		$this->db->from('stand');
		$this->db->like('standName', $filter);
		$this->db->or_like('standDescription', $filter);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getStand($StandID) {
		$this->db->select('*');
		$this->db->from('stand');
		$this->db->where('standId', $StandID);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getStand2($status) {
		$this->db->select('*');
		$this->db->from('stand');
		$this->db->where('status',$status);
		return $this->db->get();
	}

	public function updateStand($StandID, $data) {
		$this->db->where('standId', $StandID);
		$this->db->update('stand', $data);
	}

	public function deleteStand($StandID, $data){
		$this->db->where('standId', $StandID);
		$this->db->update('stand', $data);

		$this->db->select("productId");
		$this->db->from('product');
		$this->db->where('standId', $StandID);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			$data1['status'] = 0;
			$this->db->where('productId', $row->productId);
			$this->db->update('product', $data1);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
		}
	}

	public function reportStand($data){
		$this->db->insert('user_report_stand', $data);
	}

	public function getStandFromStand($standId){
		$this->db->select('standId, encryptedStandId, standName, standDescription');
		$this->db->from('stand');
		$this->db->where('encryptedStandId', $standId);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getUsersFromStand($standId){
		$this->db->select('u.userId, u.encryptedUserId, u.name, u.firstSurname, u.secondSurname');
		$this->db->from('stand AS s');
		$this->db->join('stand_owner AS so', 's.standId = so.standId');
		$this->db->join('user AS u', 'u.userId = so.userId');
		$this->db->where('s.encryptedStandId', $standId);
		$this->db->where('s.status', 1);
		$this->db->where('u.status', 1);
		$this->db->order_by('so.isMainOwner', 'DESC');
		return $this->db->get();
	}

	public function getProductsFromStand($standId){
		$this->db->select('p.productId, p.encryptedProductId, p.productName, p.description, p.cost');
		$this->db->from('stand AS s');
		$this->db->join('product AS p', 's.standId = p.standId');
		$this->db->where('s.encryptedStandId', $standId);
		$this->db->where('p.status', 1);
		return $this->db->get();
	}

	public function getUserOwnersFromStand($standId){
		$this->db->select('so.userId');
		$this->db->from('stand_owner AS so');
		$this->db->join('stand AS s', 's.standId = so.standId');
		$this->db->where('s.encryptedStandId', $standId);
		$this->db->where('s.status', 1);
		return $this->db->get();
	}

	public function findStand($standId){
		$this->db->select('*');
		$this->db->from('stand');
		$this->db->where('encryptedStandId', $standId);
		$this->db->where('status', 1);
		return $this->db->get();
	}

	public function getNumStandProducts($standId){
		$this->db->select('COUNT(p.productId) AS numStandProducts');
		$this->db->from('stand s');
		$this->db->join("product p", "s.standId = p.standId");
		$this->db->where("s.standId", $standId);
		$this->db->where("s.status", 1);
		$this->db->where("p.status", 1);
		$numStandProducts = $this->db->get();
		foreach($numStandProducts->result() as $row) {
			return $row->numStandProducts;
		}
	}

	public function getMaxNumStandProducts($standId){
		$this->db->select('st.numProducts');
		$this->db->from('stand s');
		$this->db->join("stand_type st", "s.standTypeId = st.standTypeId");
		$this->db->where("s.standId", $standId);
		$numMaxStandProducts = $this->db->get();
		foreach($numMaxStandProducts->result() as $row) {
			return $row->numProducts;
		}
	}

	public function getMaxNumStandOwners($standId){
		$this->db->select('st.numUsers');
		$this->db->from('stand s');
		$this->db->join("stand_type st", "s.standTypeId = st.standTypeId");
		$this->db->where("s.standId", $standId);
		$numMaxStandOwners = $this->db->get();
		foreach($numMaxStandOwners->result() as $row) {
			return $row->numUsers;
		}
	}

	public function getAllUsersForStand($standId){
		$query = $this->db->query("SELECT u.userId, u.name, u.firstSurname, u.secondSurname, u.username, IFNULL((SELECT so.isMainOwner FROM stand_owner so WHERE so.userId = u.userId AND so.standId=$standId), -1) AS role FROM user u");
		return $query->result();
	}

	public function getNonMainOwnersFromStand($standId){
		$this->db->select('so.userId');
		$this->db->from('stand_owner so');
		$this->db->join("user u", "so.userId = u.userId");
		$this->db->where("so.standId", $standId);
		$this->db->where("so.isMainOwner", 0);
		$this->db->where("u.status", 1);
		return $this->db->get();
	}

	public function updateMainOwnersFromStand1($standId, $users, $usersFromDB){
		for ($i = 0; $i < count($users); $i++) {
			if (!in_array($users[$i], $usersFromDB)) {
				$data["userId"]=$users[$i];
				$data["standId"]=$standId;
				$data["isMainOwner"]=0;
				$this->db->insert('stand_owner', $data);
			}
		}
		for ($i = 0; $i < count($usersFromDB); $i++) {
			if (!in_array($usersFromDB[$i], $users)) {
				$this->db->where('userId', $usersFromDB[$i]);
				$this->db->where('standId', $standId);
				$this->db->delete('stand_owner');
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
		}
	}

	public function updateMainOwnersFromStand2($standId, $usersFromDB){
		for ($i = 0; $i < count($usersFromDB); $i++) {
			$this->db->where('userId', $usersFromDB[$i]);
			$this->db->where('standId', $standId);
			$this->db->delete('stand_owner');
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
		}
	}

	public function isPartOfStand($standId){
		$this->db->select("s.encryptedStandId");
		$this->db->from("stand s");
		$this->db->join("stand_owner so", "s.standId = so.standId");
		$this->db->where("s.standId", $standId);
		$this->db->where("so.userId", $_SESSION['userSesion']);
		$owner = $this->db->get();
		if($owner->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function getStandId($encryptedStandId){
		$this->db->select('standId AS idStand');
		$this->db->from('stand');
		$this->db->where('encryptedStandId', $encryptedStandId);
		$idStands = $this->db->get();
		foreach($idStands->result() as $row) {
			return $row->idStand;
		}
	}

	public function selectTotalStands() {
		$this->db->select('COUNT(*) as totalStands');
		$this->db->from('stand');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalStands;
		}
	}

	public function selectPremiumStands() {
		$this->db->select('COUNT(*) as totalStandsPremium');
		$this->db->from('stand s');
		$this->db->join("stand_type st", "st.standTypeId = s.standTypeId");
		$this->db->where('st.standTypeName="Premium"');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalStandsPremium;
		}
	}

	public function selectNormalStands() {
		$this->db->select('COUNT(*) as totalNormalStands');
		$this->db->from('stand s');
		$this->db->join("stand_type st", "st.standTypeId = s.standTypeId");
		$this->db->where('st.standTypeName="Normal"');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalNormalStands;
		}
	}


	public function getReportsStands() {
		$this->db->select('s.standName AS NombreStand,u.username AS Dueño,u.email AS Email, COUNT(urs.reportId) AS totalDenuncias');
		$this->db->from('user_report_stand urs');
		$this->db->join("stand s", "s.standId = urs.standId");
		$this->db->join("user u", "u.userId = urs.userId");
		$this->db->where('urs.status=1');
		$this->db->group_by('1');
		$this->db->order_by('u.username', 'ASC');
		return $this->db->get();
	}

	public function selectTotalReports() {
		$this->db->select('COUNT(*) as totalReports');
		$this->db->from('user_report_stand');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalReports;
		}
	}

	public function registeredStandByMonth() {
		$this->db->select('Month(createDate) AS month, COUNT(standId) AS totalStandsRegistered');
		$this->db->from('stand');
		$this->db->where('createDate BETWEEN "2020-01-01" AND "2020-12-31"');
		$this->db->group_by('Month(createDate)');
		$this->db->order_by('1');
		return $this->db->get();
	}

	public function selectReportsByMonth() {
		$this->db->select('Month(createDate) AS mes, COUNT(standId) AS standsReported');
		$this->db->from('user_report_stand');
		$this->db->where('createDate BETWEEN "2020-01-01" AND "2020-12-31"');
		$this->db->group_by('Month(createDate)');
		$this->db->order_by('1');
		return $this->db->get();
	}

	public function cargarPedidosPorStand($standId) {
		$this->db->select('ord.productId AS productId , ord.orderId AS orderID, pro.encryptedProductId AS encryptedProductId, pro.productName AS productName, ord.message AS orderMessage, (pro.cost * ord.quantity)  AS total, us.username  AS username, us.userId AS userId, us.encryptedUserId AS encryptedUserId , ord.orderDate  AS orderDate, sta.encryptedStandId AS standId');
		$this->db->from('order AS ord');
		$this->db->join('product AS pro', 'ord.productId = pro.productId');
		$this->db->join('user AS us', 'us.userId = ord.userId');
		$this->db->join('stand AS sta', 'sta.standId = pro.standId');
		$this->db->where('ord.status', 1);
		$this->db->where('ord.is_checked', 0);
		$this->db->where('sta.standId', $standId);
		$this->db->order_by('ord.orderDate', 'ASC');
		return $this->db->get();
	}


	public function contarNumeroDenuncias($userId) {
		$this->db->select('COUNT(urs.userId) AS num');
		$this->db->from('user_report_stand AS urs');
		$this->db->where('urs.userId', $userId);
		$idStands = $this->db->get();
		foreach($idStands->result() as $row) {
			return $row->num;
		}
	}
}?>