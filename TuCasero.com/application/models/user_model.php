<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
	public function insertUser($data) {
		$maxUserId = $this->db->query("SELECT ifnull(MAX(userId)+1,1) AS maxUserId FROM user");
		foreach ($maxUserId->result() as $row) {
			$data['encryptedUserId'] = crypt(crypt($row->maxUserId,'st'),'st');
		}
		$this->db->insert('user', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
		}
	}
	public function selectUser($status) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('status', $status);
		return $this->db->get();
	}
	public function searchUser($filter) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->like('username', $filter);
		$this->db->or_like('name', $filter);
		$this->db->or_like('firstSurname', $filter);
		$this->db->or_like('secondSurname', $filter);
		return $this->db->get();
	}
	public function getUser($UserID) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('encryptedUserId', $UserID);
		return $this->db->get();
	}
	public function getStarsFromUser($userRatingId, $userRatedId){
		$this->db->select('uru.ratingNumber');
		$this->db->from('user_rating_user as uru');
		$this->db->join('user u', 'uru.userRatedId = u.userId');
		$this->db->where('uru.userRatingId', $userRatingId);
		$this->db->where('u.encryptedUserId', $userRatedId);
		$query=$this->db->get();
		if($query->num_rows()>0) {
        	foreach($query->result() as $row) {
        		return $row->ratingNumber;
        	}
        }
        else {
        	return 0;
        }
	}
	public function getAverageStarsFromUser($userRatedId){
		$this->db->select("IFNULL(ROUND(SUM(ratingNumber)/COUNT(*), 2), 'Sin Calificaciones') AS estrellas");
		$this->db->from('user_rating_user uru');
		$this->db->join('user u', 'uru.userRatedId = u.userId');
		$this->db->where('u.encryptedUserId', $userRatedId);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->estrellas;
		}
	}
	public function validateUser($Username, $Password) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $Username);
		$this->db->where('password', $Password);
		$this->db->where('status', 1);
		return $this->db->get();
	}
	public function validateUsername($Username){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $Username);
		return $this->db->get();
	}
	public function validatePhoneNumber($PhoneNumber){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('phoneNumber', $PhoneNumber);
		return $this->db->get();
	}

	public function validateEmail($email){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $email);
		return $this->db->get();
	}

	public function validateCi($ci){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id', $ci);
		return $this->db->get();
	}

	public function updateUser($UserID, $data) {
		$this->db->where('userId', $UserID);
		$this->db->update('user', $data);
	}
	public function deleteUser($UserID, $data){
		$this->db->where('userId', $UserID);
		$this->db->update('user', $data);
	}
	/*
	public function getProductsFavorites($UserID)
	{
		$this->db->select('P.productName, P.description, P.cost, P.productId');
		$this->db->from('user_favorite_product as PF');
		$this->db->join('product as P', 'PF.productId = P.productId');
		$this->db->join('user as U', 'U.userId = PF.userId');
		$this->db->where('PF.UserID', $UserID);
		return $this->db->get();
	}
	*/
	public function ProductAddFavorites($data)
	{
		$this->db->insert('user_favorite_product', $data);
	}
	public function ValidateProductAddFavorites($data)
	{
		$this->db->select('*');
		$this->db->from('user_favorite_product');
		$this->db->where('userId', $data['userId']);
		$this->db->where('productId', $data['productId']);
		return $this->db->get();
	}
	public function getProductsFavorites($userId)
	{
		$this->db->select('P.productId,P.encryptedProductId,P.productName,P.cost');
		$this->db->from('user_favorite_product PF');
		$this->db->join('product P', 'P.productId = PF.productId');
		$this->db->where('userId', $userId);
		return $this->db->get();
	}

	public function ProductDeleteFavorites($data)
	{
		//$this->db->query("DELETE FROM user_favorite_product WHERE productId='"$data['productId']"'' and userId='"$data['userId']"' ")
		$this->db->where('productId',$data['productId']);
		$this->db->where('userId',$data['userId']);
		$this->db->delete('user_favorite_product');
		
	}
	public function getProductsFromUser($UserID){
		$this->db->select('P.encryptedProductId, P.productName, P.description, P.cost, P.productId,U.userId');
		$this->db->from('product as P');
		$this->db->join('stand as S', 'S.standId = P.standId');
		$this->db->join('stand_owner as SO', 'SO.standId = S.standId');
		$this->db->join('user as U', 'U.userId = SO.userId');
		$this->db->where('U.encryptedUserId', $UserID);
		$this->db->where('p.status', 1);
		return $this->db->get();
	}
	public function getStandsFromUser($UserID){
		$this->db->select('S.standId, S.encryptedStandId, S.standName, S.standDescription');
		$this->db->from('stand as S');
		$this->db->join('stand_owner as SO', 'SO.standId = S.standId');
		$this->db->join('user as U', 'U.userId = SO.userId');
		$this->db->where('U.encryptedUserId', $UserID);
		$this->db->where('s.status', 1);
		return $this->db->get();
	}
	public function findUser($UserID){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('encryptedUserId', $UserID);
		$this->db->where('status', 1);
		return $this->db->get();
	}
	public function rateUser($userRatingId, $userRatedId, $rating){
		$this->db->select('*');
		$this->db->from('user_rating_user');
		$this->db->where("userRatingId", $userRatingId);
		$this->db->where("userRatedId", $userRatedId);
		$userRatingquery = $this->db->get();
		$userRatingResult = $userRatingquery->result_array();
		if(count($userRatingResult) > 0){
			$value=array('ratingNumber'=>$rating);
			$this->db->where("userRatingId", $userRatingId);
			$this->db->where("userRatedId", $userRatedId);
			$this->db->update('user_rating_user',$value);
		}else{
			$userRating = array("userRatingId" => $userRatingId, "userRatedId" => $userRatedId, "ratingNumber" => $rating);
			$this->db->insert('user_rating_user', $userRating);
		}
		$this->db->select("IFNULL(ROUND(SUM(ratingNumber)/COUNT(*), 2), 'Sin Calificaciones') AS estrellas");
		$this->db->from('user_rating_user');
		$this->db->where('userRatedId', $userRatedId);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			$estrellas = $row->estrellas;
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return "Sin Calificaciones";
		}
		else {
			$this->db->trans_commit();
			return $estrellas;
		}
	}

	public function getUserFromEmail($email) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where("email", $email);
		$this->db->where("status", 1);
		return $this->db->get();
	}

	public function verifyToken($token) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('encryptedUserId', $token);
		return $this->db->get();
	}

	public function updatePasswordRecover($UserID,$data) {
		$this->db->where('encryptedUserId',$UserID);
		$this->db->update('user',$data);
	}

	public function getNumUserStands($userId, $isMainOwner){
		$this->db->select('COUNT(so.standId) AS numUserStands');
		$this->db->from('user u');
		$this->db->join("stand_owner so", "so.userId = u.userId");
		$this->db->join("stand s", "s.standId = so.standId");
		$this->db->where("u.userId", $userId);
		$this->db->where("so.isMainOwner", $isMainOwner);
		$this->db->where("s.status", 1);
		$numUserStands = $this->db->get();
		foreach($numUserStands->result() as $row) {
			return $row->numUserStands;
		}
	}
	
	public function getMaxNumUserStands($userId){
		$this->db->select('ur.numStands AS numMaxUserStands');
		$this->db->from('user u');
		$this->db->join("user_role ur", "ur.userRoleId = u.roleId");
		$this->db->where("u.userId", $userId);
		$numMaxUserStands = $this->db->get();
		foreach($numMaxUserStands->result() as $row) {
			return $row->numMaxUserStands;
		}
	}

	public function getUserRole($userId){
		$this->db->select('roleId');
		$this->db->from('user');
		$this->db->where("userId", $userId);
		$userRoleId = $this->db->get();
		foreach($userRoleId->result() as $row) {
			return $row->roleId;
		}
	}

	public function registerPremiumUser($data)
	{
		$this->db->insert('premium_user', $data);

		$data1['roleId'] = 3;
		$this->db->where('userId', $data['premiumUserId']);
		$this->db->update('user', $data1);

		$this->db->select('userId, standId');
		$this->db->from('stand_owner');
		$this->db->where('userId', $data['premiumUserId']);
		$this->db->where('isMainOwner', 1);
		$query1=$this->db->get();
		foreach($query1->result() as $row) {
			$data2['standTypeId'] = 2;
			$this->db->where('standId', $row->standId);
			$this->db->update('stand', $data2);
		}

		$this->db->select('p.productId');
		$this->db->from('product p ');
		$this->db->join("stand s", "s.standId = p.standId");
		$this->db->join("stand_owner so", "so.standId = s.standId");
		$this->db->where('so.userId', $data['premiumUserId']);
		$this->db->where('so.isMainOwner', 1);
		$query2=$this->db->get();
		foreach($query2->result() as $row) {
			$data3['productTypeId'] = 2;
			$this->db->where('productId', $row->productId);
			$this->db->update('product', $data3);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		}
		else {
			$this->db->trans_commit();
		}
	}

	public function getUserOrders($userId){
		$this->db->select('ord.orderId AS orderID, ord.productId AS productId, pro.encryptedProductId AS encryptedProductId, pro.productName AS productName, ord.message AS orderMessage, (pro.cost * ord.quantity)  AS total, sta.standName  AS standName, sta.encryptedStandId AS encryptedStandId , ord.orderDate  AS orderDate');
		$this->db->from('order AS ord');
		$this->db->join('product AS pro', 'ord.productId = pro.productId');
		$this->db->join('stand AS sta', 'sta.standId = pro.standId');
		$this->db->where('ord.status', 1);
		$this->db->where('ord.is_checked', 0);
		$this->db->where('ord.userId', $userId);
		$this->db->order_by('ord.orderDate', 'ASC');
		return $this->db->get();
	}

	public function selectTotalUser() {
		$this->db->select('COUNT(*) as total');
		$this->db->from('user');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->total;
		}
	}

	public function selectDisabledUser($status) {
		$this->db->select('COUNT(*) as totalDisabled');
		$this->db->from('user');
		$this->db->where('status', $status);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalDisabled;
		}
	}

	public function selectEnabledUser($status) {
		$this->db->select('COUNT(*) as totalEnabled');
		$this->db->from('user');
		$this->db->where('status', $status);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalEnabled;
		}
	}

	public function selectTotalOrders() {
		$this->db->select('COUNT(orderId) as totalOrders1');
		$this->db->from('order');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalOrders1;
		}
	}

	public function selectTotalQuantityProduct() {
		$this->db->select('SUM(quantity) as totalQuantityProducts');
		$this->db->from('order');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalQuantityProducts;
		}
	}

	public function selectTotalValueOrders() {
		$this->db->select('SUM(o.quantity * p.cost) as totalValueOrders');
		$this->db->from('order o');
		$this->db->join('product p', 'p.productId = o.productId');
		$query=$this->db->get();
		foreach($query->result() as $row) {
			return $row->totalValueOrders;
		}
	}

	public function selectUserOrder() {
		$this->db->select('u.username AS Dueño,u.email AS Email, COUNT(o.orderId) AS totalPedidos, SUM(o.quantity) AS cantidadTotalPedidos,SUM(o.quantity * p.cost) AS totalVendido');
		$this->db->from('order o');
		$this->db->join('user u', 'u.userId = o.userId');
		$this->db->join('product p', 'p.productId = o.productId');
		$this->db->where('o.status=1');
		$this->db->group_by('1'); 
		return $this->db->get();
	}

	public function selectUserOrderByMonth() {
		$this->db->select('MONTH(o.orderDate) AS mes,COUNT(o.orderId) AS totalOrders,SUM(o.quantity) AS totalQuantity,SUM(o.quantity*p.cost) AS totalMoney');
		$this->db->from('order o');
		$this->db->join('product p','p.productId=o.productId');
		$this->db->where('orderDate BETWEEN "2020-01-01" AND "2020-12-31"');
		$this->db->group_by('MONTH(orderDate)');
		$this->db->order_by('1');
		return $this->db->get();
	}

	public function selectRegisterUserByMonth() {
		$this->db->select('MONTH(createDate) AS mesRU,COUNT(userId) AS totalUsersRegister');
		$this->db->from('user');
		$this->db->where('createDate BETWEEN "2020-01-01" AND "2020-12-31"');
		$this->db->group_by('MONTH(createDate)');
		$this->db->order_by('1');
		return $this->db->get();
	}

	public function selectDisabledUserByMonth() {
		$this->db->select('Month(createDate) AS monthDU, COUNT(userId) AS totalUsersDisabled');
		$this->db->from('user');
		$this->db->where('status = 0 AND createDate BETWEEN "2020-01-01" AND "2020-12-31"');
		$this->db->group_by('MONTH(createDate)');
		$this->db->order_by('1');
		return $this->db->get();
	}

	public function isPremium($userId) {
		$this->db->select('premiumUserId');
		$this->db->from('premium_user');
		$this->db->where('premiumUserId', $userId);
		$query=$this->db->get();
		foreach($query->result() as $row) {
			if($row->premiumUserId)
			{
				return 1;
			}
			return 0;
		}
	}

	public function getDataForCorrectEmailbyIdOrderId($orderId){
		$this->db->select('us.username AS username, us.email AS email,  pro.productName AS productName, ord.message AS orderMessage, (pro.cost * ord.quantity)  AS total, sta.standName  AS standName, sta.encryptedStandId AS encryptedStandId , ord.orderDate  AS orderDate, sta.standId AS standId');
		$this->db->from('order AS ord');
		$this->db->join('product AS pro', 'ord.productId = pro.productId');
		$this->db->join('stand AS sta', 'sta.standId = pro.standId');
		$this->db->join('user AS us', 'us.userId = ord.userId');
		$this->db->where('ord.orderId', $orderId);
		return $this->db->get();
	}

	public function encargadosByStandid($standId){
		$this->db->select('CONCAT(us.name, " ", us.firstSurname, " ",us.secondSurname) AS fullname, us.username AS user, us.email AS email, us.phoneNumber AS phoneNumber');
		$this->db->from('stand AS sta');
		$this->db->join('stand_owner AS so', 'so.standId = sta.standId');
		$this->db->join('user AS us', 'us.userId = so.userId');
		$this->db->where('sta.standId', $standId);
		$this->db->where('us.status', 1);
		return $this->db->get();
	}


}?>