$(function(){
    $('#uiForm')
    .form({
        name: {
            identifier: 'standName',
            rules: [{
                type   : 'empty',
                prompt : 'Por favor introduce el nombre del stand'
            }]
        },
        description: {
            identifier: 'standDescription',
            rules: [{
                type   : 'empty',
                prompt : 'Por favor introduzca una descripción para su stand'
            }]
        }
    });
});
$('#standName').keyup(function() {
    var characterCount = $(this).val().length, current = $('#current-stand-name-char-count'), maximum = $('#maximum-stand-name-char-count'), theCount = $('#stand-name-char-count');
    current.text(characterCount);
    if (characterCount < 30) {
        maximum.css('color', '#666');
        current.css('color', '#666');
        theCount.css('font-weight','normal');
    } else {
        maximum.css('color', '#8f0001');
        current.css('color', '#8f0001');
        theCount.css('font-weight','bold');
    }
});
$('textarea').keyup(function() {
    var characterCount = $(this).val().length, current = $('#current'), maximum = $('#maximum'), theCount = $('#the-count');
    current.text(characterCount);
    if (characterCount < 230) {
        maximum.css('color', '#666');
        current.css('color', '#666');
        theCount.css('font-weight','normal');
    } else {
        maximum.css('color', '#8f0001');
        current.css('color', '#8f0001');
        theCount.css('font-weight','bold');
    }
});
function readURL(input) {
    if(input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#imageResult')
            .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});
var input = document.getElementById( 'upload' );
var infoArea = document.getElementById( 'upload-label' );
input.addEventListener( 'change', showFileName );
function showFileName( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = 'Archivo: ' + fileName;
}

if (window.history.replaceState) {
    window.history.replaceState( null, null, window.location.href );
}