$('.ui.dropdown').dropdown();
		$(function(){
			$('#uiForm')
			  .form({
			    fields: {
			      name: {
			        identifier: 'name',
			        rules: [
			          {
			            type   : 'regExp',
						value : '[a-z A-Z]',
			            prompt : 'Por favor introduce tus nombre(s)'
			          }
			        ]
			      },
				  firstSurname: {
			        identifier: 'firstSurname',
			        rules: [
			          {
			            type   : 'regExp',
						value : '[a-z A-Z]',
			            prompt : 'Por favor introduce tu primer Apellido'
			          }
			        ]
			      },
				  username: {
			        identifier: 'username',
			        rules: [
			          {
			            type   : 'regExp',
						value : '[a-z A-Z 0-9]',
			            prompt : 'Por favor introduce tu nombre de Usuario (Debe ser unico y no se podra cambiar)'
			          }
			        ]
			      },
				  pass: { 
			        identifier: 'password',
			        rules: [		          
					  {
			            type   : 'empty',
			            prompt : 'Por favor introduce tu contraseña'
			          }
			        ]
			      },
				  re_password: {
			        identifier: 're_password',
			        rules: [
			          {
			            type   : 'match[pass1]',
			            prompt : 'Por favor confirma tu contraseña debe ser igual a la primera contraseña'
			          }
			        ]
			      },
				  dateOfBirth: {
			        identifier: 'dateOfBirth',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor introduce tu fecha De Nacimiento'
			          }
			        ]
			      },
				  ci: {
			        identifier: 'ci',
			        rules: [
			          {
			            type   : 'regExp[[0-9]{7}]',
			            prompt : 'Por favor introduce tu carnet de identidad (Debe ser unico y no se podra cambiar)'
			          }
			        ]
			      },
				  email: {
			        identifier: 'email',
			        rules: [
			          {
			            type   : 'email',
			            prompt : 'Por favor introduce un correo electronico valido (Debe ser unico)'
			          }
			        ]
			      },
				  phoneNumber: {
			        identifier: 'phoneNumber',
			        rules: [
			          {
			            type   : 'integer',
			            prompt : 'Por favor introduce tu numero de celular (Omite el +591) (Debe ser unico)'
			          },
			          {
			            type   : 'regExp[[0-9]{8}]',
			            prompt : 'Número de teléfono no válido '
			          },
			          {
			            type   : 'maxLength[8]',
			            prompt : 'Número de teléfono no válido '
			          }
			        ]
			      },
			      gender: {
			        identifier: 'gender',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor selecciona tu genero'
			          }
			        ]
			      }
			    }
			  });  
		});

		function validateUSername() 
		{
			var data = $(".formu").serialize();
            $.post('<?php echo base_url() ?>index.php/user/usernameExists')
            .done(function(resp){
                $(".resultado").html(resp);
            })
            .fail(function(err){
                $(".resultado").html(err);
            })
			
		}