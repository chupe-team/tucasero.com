
$("#productosRespuesta").on('click',"button",function(event){
    var i=($(this).attr("id"));
    var nombre=$('#nombreProducto'+i).val();
    var precio=$('#precio'+i).val();
    var userId=$('#userId').val();
    var productId=$('#productId'+i).val();
    var cantidad=$('#cantidad'+i).val();
    var descripcion=$("#descripcion"+i).val();
    var stock=$("#stock"+i).val();
    var imagen=$("#direccionImagen"+i).val();
    if (cantidad>0) {
        $.ajax({
            url: $("#direccionCarrito").val(),
            type: 'POST',
            data:{              
                'userId':userId,
                'productId':productId,
                'quantity': cantidad,
                'image': imagen
            },
            success: function(datos) {
                //$("#contenedorCarrito").html(datos);
                toastr.success(cantidad+' '+nombre,'Se agrego al carrito');                 
            },
            error: function(){
                alert('Error!');
            }
        });
    }
    else{
        toastr.error('Agregue una cantidad válida de producto','No se agrego el producto al carrito');
        $('#cantidad'+i).addClass('alertarCantidad');
    }
       
});
$("#carritoMostrarDatos").click(function(){   
    $.ajax({
        url: $("#MostrarCarrito").val(),
        type: 'POST',       
        success: function(datos) {
            $("#contenedorCarrito").html(datos);          
        },
        error: function(){
            alert('Error!');
        }
    });
});
$("#vaciarCarrito").click(function(event){  
    event.preventDefault(); 
    $.ajax({
        url: $("#direccionVaciarCarrito").val(),
        type: 'POST',       
        success: function(datos) {
              $("#contenedorCarrito").html("");  
              toastr.warning('se eliminaron todos los productos de tu carrito','Se vacio tu carrito con éxito');                 
      
        },
        error: function(){
            alert('Error!');
        }
    });
});
$("#productosRespuesta").on("click","input",function(){
    var i=($(this).attr("id"));
    $("#"+i).removeClass("alertarCantidad");
});
