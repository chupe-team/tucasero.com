$('.ui.dropdown') .dropdown();
$('textarea').keyup(function() {
	var characterCount = $(this).val().length, current = $('#current'), maximum = $('#maximum'), theCount = $('#maxCharacters');
	current.text(characterCount);
	if (characterCount < 80) {
		maximum.css('color', '#333333');
		current.css('color', '#333333');
		theCount.css('font-weight','normal');
	} else {
		maximum.css('color', '#B90504');
		current.css('color', '#B90504');
		theCount.css('font-weight','bold');
	}
});