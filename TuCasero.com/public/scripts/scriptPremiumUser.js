$(function(){
	$('#uiForm').form({
		card: {
			identifier: 'cardNumber',
			rules: [
				{
					type   : 'empty',
					prompt : 'Por favor introduzca el número de tarjeta'
				},
				{
					type   : 'creditCard[visa,mastercard,amex]',
					prompt : 'Este número de tarjeta no es real'
				}
			]
		},
		expiryMonth: {
			identifier: 'expiryMonth',
			rules: [
				{
					type   : 'empty',
					prompt : 'Por favor introduzca la fecha de expiración'
				}
			]
		},
		cvCode: {
			identifier: 'cvCode',
			rules: [
				{
					type   : 'empty',
					prompt : 'Por favor introduzca su código de seguridad'
				},
				{
					type   : 'minLength[3]',
					prompt : 'La cantidad del código de seguridad es de 3 dígitos'
				}
			]
		}
	});
});









/*function cardNumberOnKeyPress() {
	var x = document.getElementById("cardNumber").value;
	if ((x.length == 4 || x.length == 9 || x.length == 14) && x.charAt(x.length - 1) != '-'
		&& numberOfHyphens(x) < 3) {
		document.getElementById("cardNumber").value = x + "-";
	}
	else if((x.length == 4 || x.length == 9 || x.length == 14) && x.charAt(x.length - 1) == '-'
		&& numberOfHyphens(x) < 3){
		document.getElementById("cardNumber").value = x.substring(0, x.length - 1);
	}
}

function cardNumberOnKeyUp(){
	var x = document.getElementById("cardNumber").value;
	if(x.length>=5){
		if(x.charAt(4) != '-' && numberOfHyphens(document.getElementById("cardNumber").value) < 3) {
			y = x.substring(0, 4) + "-" + x.substring(4, x.length);
			document.getElementById("cardNumber").value = y;
		}
	}
	if(x.length>=10) {
		if(x.charAt(9) != '-' && numberOfHyphens(document.getElementById("cardNumber").value) < 3){
			z = y.substring(0, 9) + "-" + y.substring(9, y.length);
			document.getElementById("cardNumber").value = z;
		}
	}
	if(x.length>=15) {
		if(x.charAt(14) != '-' && numberOfHyphens(document.getElementById("cardNumber").value) < 3){
			document.getElementById("cardNumber").value = z.substring(0, 14) + "-" + z.substring(14, z.length);
		}
	}
}

function numberOfHyphens($str){
	return $str.split('-').length - 1;
}*/

/*<input type="text" class="form-control" id="cardNumber"
name="cardNumber" placeholder="Número de tarjeta válido"
onkeypress="cardNumberOnKeyPress()" onkeyup="cardNumberOnKeyUp()"
maxlength="19" autofocus />*/