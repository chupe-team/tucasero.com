$('.ui.dropdown').dropdown();

$('.ui.search.fluid.dropdown').dropdown({
	allowAdditions: true,
});
$.fn.form.settings.rules.greaterThan = function (inputValue, validationValue) {
	return inputValue >= validationValue;
}
$(function(){
	$('#uiForm')
		.form({
			title: {
				identifier: 'title',
				rules: [
					{
						type : 'empty',
						prompt : 'Por favor introduce el nombre del producto'
					}
				]
			},
			cost: {
				identifier: 'costNumber',
				rules: [
					{
						type : 'empty',
						prompt : 'Por favor introduce el precio'
					},
					{
						type : 'decimal',
						prompt : 'El tipo de dato del precio no es correcto'
					},
					{
						type   : 'greaterThan[0.00]',
						prompt : 'El precio debe ser mayor a 0 Bs.',
					}
				]
			},
			quantity: {
				identifier: 'quantity',
				rules: [
					{
						type : 'empty',
						prompt : 'Por favor introduzca la cantidad'
					},
					{
						type   : 'integer',
						prompt : 'Dato no válido en stock'
					},
					{
						type   : 'greaterThan[0.00]',
						prompt : 'La cantidad del producto debe ser mayor a 0',
					}
				]
			},
			description: {
				identifier: 'description',
				rules: [
					{
						type : 'empty',
						prompt : 'Por favor introduce la descripción del producto '
					}
				]
			},
			category: {
				identifier: 'category',
				rules: [
					{
						type : 'empty',
						prompt : 'Por favor seleccione una categoría'
					}
				]
			},
			tags: {
			    identifier: 'tags',
			    rules: [
			        {
			            type   : 'minCount[1]',
			            prompt : 'Por favor seleccione minímo un tag'
			        },
					{
						type   : 'maxCount[10]',
						prompt : 'Su límite es de 10 tags'
					}
			    ]
			}//,
			/*files: {
				identifier: 'files',
				rules: [
			    	{
						type : 'empty',
						prompt : 'Por favor introduzca una foto del producto'
			    	}
				]
			}*/
		})
	;
});

$('#title').keyup(function() {
	var characterCountInput = $(this).val().length, current = $('#currentInput'), maximum = $('#maximumInput'), theCount = $('#maxCharactersInput');
	current.text(characterCountInput);
	if (characterCountInput < 15) {
		maximum.css('color', '#333333');
		current.css('color', '#333333');
		theCount.css('font-weight','normal');
	} else {
		maximum.css('color', '#B90504');
		current.css('color', '#B90504');
		theCount.css('font-weight','bold');
	}
});

$('textarea').keyup(function() {
	var characterCount = $(this).val().length, current = $('#current'), maximum = $('#maximum'), theCount = $('#maxCharacters');
	current.text(characterCount);
	if (characterCount < 80) {
		maximum.css('color', '#333333');
		current.css('color', '#333333');
		theCount.css('font-weight','normal');
	} else {
		maximum.css('color', '#B90504');
		current.css('color', '#B90504');
		theCount.css('font-weight','bold');
	}
});
		
window.onload = function() {
	if (window.File && window.FileList && window.FileReader) {
		$('#files').change(function(event) {
			var files = event.target.files;
			var output = document.getElementById("result");
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				if (file.type.match('image.*')) {
					if (this.files[0].size < 2097152) {
						var picReader = new FileReader();
						picReader.addEventListener("load", function(event) {
							var picFile = event.target;
							var div = document.createElement("div");
							div.innerHTML = "<img class='thumbnail' width='150' height='150' src='" + picFile.result + "'" +"title='preview image'/>";
							output.insertBefore(div, null);
						});
						$('#clear, #result').show();
						picReader.readAsDataURL(file);
					} else {
						alert("Solo se permiten imagenes de 2MB de tamaño");
						$(this).val("");
					}
				} else {
					alert("Solamente se perimiten imagenes");
					$(this).val("");
				}
			}
		});
	} else {
		console.log("Tu navegador no soporta la subida de archivos");
	}
}

$('#files').click(function() {
	$('.thumbnail').parent().remove();
	$('result').hide();
	$(this).val("");
});