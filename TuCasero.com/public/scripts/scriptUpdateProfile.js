$('.ui.dropdown').dropdown();
		$(function(){
			$('#uiForm1')
			  .form({
			      name: {
			        identifier: 'name1',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor introduce tu nombre'
			          },
			          {
			            type   : 'regExp[/^[a-z ,.-]+$/i]',
			            prompt : '¿Seguro que has introducido el nombre correctamente?'
			          },
			        ]
			      },
			      firstSurnamme: {
			        identifier: 'firstSurnamme1',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor introduce tu primer apellido '
			          },
			          {
			          	type   : 'regExp[/^[A-Z]+$/i]',
			            prompt : '¿Seguro que has introducido el apellido correctamente?'
			          }
			        ]
			      },
			      phoneNumber: {
			        identifier: 'phoneNumber1',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor introduce tu teléfono '
			          },
			          {
			            type   : 'regExp[[0-9]{8}]',
			            prompt : 'Número de teléfono no válido '
			          },
			          {
			            type   : 'maxLength[8]',
			            prompt : 'Número de teléfono no válido '
			          }
			        ]
			      },
			      dateOfBirth: {
			        identifier: 'dateOfBirth1',
			        rules: [
			          {
			            type   : 'empty',
			            prompt : 'Por favor introduce tu fecha de nacimiento '
			          }
			        ]
			      },
			      email: {
			        identifier: 'email1',
			        rules: [
			          {
			            type   : 'email',
			            prompt : 'Por favor introduce un email válido '
			          }
			        ]
			      }
			  })
			;
		});
		function opennn(openTab) {
			var newButton = openTab+"Btn";
			document.getElementsByName("updateProfileBtn")[0].setAttribute('id', '');
			document.getElementsByName("standsBtn")[0].setAttribute('id', '');
			document.getElementsByName("favoritesBtn")[0].setAttribute('id', '');
			document.getElementsByName("opinionsBtn")[0].setAttribute('id', '');
			document.getElementsByName(newButton)[0].setAttribute('id', 'active');
			document.getElementById("updateProfile").style.display = "none";
			document.getElementById("stands").style.display = "none";
			document.getElementById("favorites").style.display = "none";
			document.getElementById("opinions").style.display = "none";
			document.getElementById(openTab).style.display = "block";
		}

		if (window.history.replaceState) {
			window.history.replaceState( null, null, window.location.href );
		}