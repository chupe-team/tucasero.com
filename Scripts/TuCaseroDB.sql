﻿SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tucaserodb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tucaserodb` DEFAULT CHARACTER SET utf16 ;
USE `tucaserodb` ;

-- -----------------------------------------------------
-- Table `tucaserodb`.`product_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`product_type` (
  `productTypeId` TINYINT NOT NULL AUTO_INCREMENT,
  `productTypeName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`productTypeId`))
ENGINE = InnoDB
AUTO_INCREMENT = 3;

INSERT INTO `product_type` VALUES (1,'Normal'), (2,'Premium');

-- -----------------------------------------------------
-- Table `tucaserodb`.`stand_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`stand_type` (
  `standTypeId` TINYINT NOT NULL AUTO_INCREMENT,
  `standTypeName` VARCHAR(45) NOT NULL,
  `numProducts` TINYINT NOT NULL,
  `numUsers` TINYINT NOT NULL,
  `productTypeId` TINYINT NOT NULL,
  PRIMARY KEY (`standTypeId`),
  INDEX `fk_stand_type_product_type_idx` (`productTypeId` ASC),
  CONSTRAINT `fk_stand_type_product_type`
    FOREIGN KEY (`productTypeId`)
    REFERENCES `tucaserodb`.`product_type` (`productTypeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3;

INSERT INTO `stand_type` VALUES (1,'Normal',20,3,1), (2,'Premium',30,5,2);

-- -----------------------------------------------------
-- Table `tucaserodb`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`category` (
  `categoryId` SMALLINT(6) NOT NULL,
  `categoryName` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`categoryId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `category`
VALUES (1,'Salud'),(2,'Comida'),(3,'Móviles y telefonía'),(4,'Informática y electrónica'),(5,'Deporte y Ocio'),
(6,'Bicicletas'),(7,'Consolas y Videojuegos'),(8,'Hogar y Jardín'),(9,'Electrodomésticos'),(10,'Cine, Libros y Música'),(11,'TV, Audio y Foto'),
(12,'Ropa'),(13,'Otro');

-- -----------------------------------------------------
-- Table `tucaserodb`.`path`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`path` (
  `pathId` TINYINT(4) NOT NULL AUTO_INCREMENT,
  `pathName` VARCHAR(50) NOT NULL,
  `pathDescription` VARCHAR(240) NOT NULL,
  PRIMARY KEY (`pathId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tucaserodb`.`stand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`stand` (
  `standId` INT(11) NOT NULL AUTO_INCREMENT,
  `encryptedStandId` VARCHAR(13) NOT NULL,
  `standName` VARCHAR(40) NOT NULL,
  `standDescription` VARCHAR(255) NOT NULL,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` TIMESTAMP NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '1',
  `standTypeId` TINYINT NOT NULL,
  PRIMARY KEY (`standId`),
  INDEX `fk_stand_stand_role1_idx` (`standTypeId` ASC),
  CONSTRAINT `fk_stand_stand_role1`
    FOREIGN KEY (`standTypeId`)
    REFERENCES `tucaserodb`.`stand_type` (`standTypeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

INSERT INTO `stand`
VALUES (1,'st6Wr70gM.106','Tu Casero Stand','Stand oficial del sitio web tucasero.com','2020-09-14 21:51:14',NULL,1,1);

-- -----------------------------------------------------
-- Table `tucaserodb`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`product` (
  `productId` INT(11) NOT NULL AUTO_INCREMENT,
  `encryptedProductId` VARCHAR(13) NOT NULL,
  `productName` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `quantity` INT(11) NOT NULL,
  `cost` DOUBLE NOT NULL DEFAULT '0',
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` TIMESTAMP NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '1',
  `standId` INT(11) NOT NULL,
  `categoryId` SMALLINT(6) NOT NULL,
  `productTypeId` TINYINT NOT NULL,
  PRIMARY KEY (`productId`),
  INDEX `standId` (`standId` ASC),
  INDEX `categoryId` (`categoryId` ASC),
  INDEX `fk_product_product_type1_idx` (`productTypeId` ASC),
  CONSTRAINT `product_ibfk_1`
    FOREIGN KEY (`standId`)
    REFERENCES `tucaserodb`.`stand` (`standId`),
  CONSTRAINT `product_ibfk_2`
    FOREIGN KEY (`categoryId`)
    REFERENCES `tucaserodb`.`category` (`categoryId`),
  CONSTRAINT `fk_product_product_type1`
    FOREIGN KEY (`productTypeId`)
    REFERENCES `tucaserodb`.`product_type` (`productTypeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

INSERT INTO `product`
VALUES (1,'stnKibARuah3w','Negrito','Rico helado de vainilla y chocolate',89,5,'2020-09-14 21:51:14',NULL,1,1,2,1),
(2,'st6icswzCiIVk','Venda','Refuerzo para torceduras y fracturas',5,15,'2020-09-14 21:51:14',NULL,1,1,1,1);

-- -----------------------------------------------------
-- Table `tucaserodb`.`tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`tag` (
  `tagId` INT(11) NOT NULL AUTO_INCREMENT,
  `tagName` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`tagId`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;

INSERT INTO `tag` VALUES (1,'nuevo'),(2,'seguro'),(3,'2020'),(4,'garantizado'),(5,'delicioso');

-- -----------------------------------------------------
-- Table `tucaserodb`.`product_tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`product_tag` (
  `productId` INT(11) NOT NULL,
  `tagId` INT(11) NOT NULL,
  PRIMARY KEY (`productId`, `tagId`),
  INDEX `labelId` (`tagId` ASC),
  CONSTRAINT `product_label_ibfk_1`
    FOREIGN KEY (`productId`)
    REFERENCES `tucaserodb`.`product` (`productId`),
  CONSTRAINT `product_label_ibfk_2`
    FOREIGN KEY (`tagId`)
    REFERENCES `tucaserodb`.`tag` (`tagId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `product_tag` VALUES (1,1),(1,5),(2,2),(2,3),(2,4);

-- -----------------------------------------------------
-- Table `tucaserodb`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`user_role` (
  `userRoleId` TINYINT(4) NOT NULL AUTO_INCREMENT,
  `userRoleName` VARCHAR(30) NOT NULL,
  `numStands` TINYINT(4) NOT NULL,
  `standTypeId` TINYINT NOT NULL,
  PRIMARY KEY (`userRoleId`),
  INDEX `fk_user_role_stand_role1_idx` (`standTypeId` ASC),
  CONSTRAINT `fk_user_role_stand_role1`
    FOREIGN KEY (`standTypeId`)
    REFERENCES `tucaserodb`.`stand_type` (`standTypeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

INSERT INTO `user_role`
VALUES (1,'Admin',1,1), (2,'Usuario Normal',3,1), (3,'Usuario Premium',10,2);

-- -----------------------------------------------------
-- Table `tucaserodb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`user` (
  `userId` MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(25) NOT NULL,
  `password` VARBINARY(100) NOT NULL,
  `encryptedUserId` VARCHAR(13) NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `firstSurname` VARCHAR(60) NOT NULL,
  `secondSurname` VARCHAR(60) NULL DEFAULT NULL,
  `id` VARCHAR(12) NOT NULL,
  `dateOfBirth` DATE NOT NULL,
  `email` VARCHAR(70) NOT NULL,
  `facebook` VARCHAR(250) NULL DEFAULT NULL,
  `phoneNumber` VARCHAR(8) NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` TIMESTAMP NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '1',
  `roleId` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userId`),
  INDEX `fk_User_Role_idx` (`roleId` ASC),
  CONSTRAINT `fk_User_Role`
    FOREIGN KEY (`roleId`)
    REFERENCES `tucaserodb`.`user_role` (`userRoleId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

INSERT INTO `user`
VALUES (1,'Admin',X'3230326362393632616335393037356239363462303731353264323334623730','stqmfmnFoI5tw','Admin','Admin','Admin','11111111','2000-09-24','iriartezam18@gmail.com',NULL,'23456789','M','2020-09-14 21:51:14',NULL,1,1),
(2,'Irizam',X'3230326362393632616335393037356239363462303731353264323334623730','stb3eZKYhC.3U','Rodrigo','Iriarte','Zamorano','13419279','2000-09-24','rodrigo.iriarte14@gmail.com','https://www.facebook.com/MyNameIsRodri/','68464817','M','2020-09-14 21:51:14',NULL,1,2),
(3,'Axelito',X'3832376363623065656138613730366334633334613136383931663834653762','stMg/nmubcEvI','Axel','Martinez','Ayala','15448786','2000-01-01','eddymartinez2022@gmail.com','https://www.facebook.com/axel.martinezayala.1','12345678','M','2020-09-14 22:02:56',NULL,1,3);

-- -----------------------------------------------------
-- Table `tucaserodb`.`premium_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`premium_user` (
  `premiumUserId` MEDIUMINT(9) NOT NULL,
  `cardNumber` VARCHAR(16) NOT NULL,
  `expiryMonth` DATE NOT NULL,
  `cvCode` VARBINARY(100) NOT NULL,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` TIMESTAMP NULL,
  `status` TINYINT NOT NULL DEFAULT 1,
  INDEX `fk_premiumUser_user_idx` (`premiumUserId` ASC),
  CONSTRAINT `fk_premiumUser_user`
    FOREIGN KEY (`premiumUserId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `premium_user`
VALUES (3, '4716251691824649', '2020-10-01', X'3230326362393632616335393037356239363462303731353264323334623730','2020-09-14 22:02:56', NULL, 1);

-- -----------------------------------------------------
-- Table `tucaserodb`.`stand_owner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`stand_owner` (
  `userId` MEDIUMINT(9) NOT NULL,
  `standId` INT(11) NOT NULL,
  `isMainOwner` TINYINT NOT NULL,
  PRIMARY KEY (`userId`, `standId`),
  INDEX `fk_stand_owner_stand1_idx` (`standId` ASC),
  CONSTRAINT `stand_owner_ibfk_1`
    FOREIGN KEY (`userId`)
    REFERENCES `tucaserodb`.`user` (`userId`),
  CONSTRAINT `fk_stand_owner_stand1`
    FOREIGN KEY (`standId`)
    REFERENCES `tucaserodb`.`stand` (`standId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `stand_owner` VALUES (1,1,1);

-- -----------------------------------------------------
-- Table `tucaserodb`.`user_comment_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`user_comment_user` (
  `userCommentingId` MEDIUMINT(9) NOT NULL,
  `userCommentedId` MEDIUMINT(9) NOT NULL,
  `userComment` VARCHAR(240) NOT NULL,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` TIMESTAMP NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userCommentingId`, `userCommentedId`),
  INDEX `fk_table2_user1_idx` (`userCommentingId` ASC),
  INDEX `fk_table2_user2_idx` (`userCommentedId` ASC),
  CONSTRAINT `fk_table2_user1`
    FOREIGN KEY (`userCommentingId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table2_user2`
    FOREIGN KEY (`userCommentedId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tucaserodb`.`user_favorite_product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`user_favorite_product` (
  `userId` MEDIUMINT(9) NOT NULL,
  `productId` INT(11) NOT NULL,
  `favoriteDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userId`, `productId`),
  INDEX `fk_product_has_user_user1_idx` (`userId` ASC),
  INDEX `fk_product_has_user_product1_idx` (`productId` ASC),
  CONSTRAINT `fk_product_has_user_product1`
    FOREIGN KEY (`productId`)
    REFERENCES `tucaserodb`.`product` (`productId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_has_user_user1`
    FOREIGN KEY (`userId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tucaserodb`.`user_rating_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`user_rating_user` (
  `userRatingId` MEDIUMINT(9) NOT NULL,
  `userRatedId` MEDIUMINT(9) NOT NULL,
  `ratingNumber` TINYINT(4) NOT NULL,
  PRIMARY KEY (`userRatingId`, `userRatedId`),
  INDEX `fk_Rating_user_idx` (`userRatingId` ASC),
  INDEX `fk_Rating_user1_idx` (`userRatedId` ASC),
  CONSTRAINT `fk_Rating_user`
    FOREIGN KEY (`userRatingId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Rating_user1`
    FOREIGN KEY (`userRatedId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tucaserodb`.`report_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`report_type` (
  `reportTypeId` TINYINT NOT NULL AUTO_INCREMENT,
  `reportTypeName` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`reportTypeId`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;

INSERT INTO `report_type`
VALUES (1,'Sospecha de fraude'), (2, 'Fraude'), (3, 'No asistencia a la cita'),
(4, 'Mal comportamiento o abuso'), (5, 'Artículo defectuoso o incorrecto'), (6, 'Otras causas');


-- -----------------------------------------------------
-- Table `tucaserodb`.`user_report_stand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tucaserodb`.`user_report_stand` (
  `reportId` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `userId` MEDIUMINT(9) NOT NULL,
  `standId` INT(11) NOT NULL,
  `reportTypeId` TINYINT(4) NOT NULL,
  `description` VARCHAR(100) NOT NULL,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` TIMESTAMP NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '1',
  `checkedBy` MEDIUMINT(9) NULL DEFAULT NULL,
  PRIMARY KEY (`reportId`, `userId`, `standId`),
  INDEX `fk_user_has_stand_stand1_idx` (`standId` ASC),
  INDEX `fk_user_has_stand_user1_idx` (`userId` ASC),
  INDEX `fk_user_report_stand_report_type1_idx` (`reportTypeId` ASC),
  CONSTRAINT `fk_user_has_stand_stand1`
    FOREIGN KEY (`standId`)
    REFERENCES `tucaserodb`.`stand` (`standId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_stand_user1`
    FOREIGN KEY (`userId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_report_stand_report_type1`
    FOREIGN KEY (`reportTypeId`)
    REFERENCES `tucaserodb`.`report_type` (`reportTypeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ci_sessions` (
	`id` varchar(128) NOT NULL
	,`ip_address` varchar(45) NOT NULL
	,`timestamp` int(10) unsigned DEFAULT 0 NOT NULL
	,`data` blob NOT NULL,KEY `ci_sessions_timestamp` (`timestamp`));


CREATE TABLE IF NOT EXISTS `tucaserodb`.`order` (
  `orderId` INT NOT NULL AUTO_INCREMENT,
  `userId` MEDIUMINT NOT NULL,
  `productId` INT NOT NULL,
  `quantity` INT NOT NULL,
  `message` VARCHAR(255) NOT NULL,
  `orderDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_checked` TINYINT NOT NULL DEFAULT '0',
  `status` TINYINT NOT NULL DEFAULT '1',
  PRIMARY KEY (`orderId`),
  CONSTRAINT `fk_order_user1`
    FOREIGN KEY (`userId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_product1`
    FOREIGN KEY (`productId`)
    REFERENCES `tucaserodb`.`product` (`productId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tucaserodb`.`cart` (
  `cartId` INT NOT NULL AUTO_INCREMENT,
  `userId` MEDIUMINT NOT NULL,
  `productId` INT NOT NULL,
  `quantity` SMALLINT NOT NULL,
  `image` VARCHAR(255) NULL,
  PRIMARY KEY (`cartId`),
  INDEX `fk_cart_product_idx` (`productId` ASC),
  INDEX `fk_cart_user1_idx` (`userId` ASC),
  CONSTRAINT `fk_cart_product`
    FOREIGN KEY (`productId`)
    REFERENCES `tucaserodb`.`product` (`productId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_user1`
    FOREIGN KEY (`userId`)
    REFERENCES `tucaserodb`.`user` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;