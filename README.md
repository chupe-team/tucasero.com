# TuCasero.com

Es un sistema web que se basa en la compra-venta de productos, donde te permite subir tus productos para su venta. 
De igual manera, podrás editar o eliminar su disponibilidad. Por otra parte, el comprador, podrá ponerse en contacto contigo y establecer el modo de venta y compra.

### Integrantes
 - Jose Luis Bascope Tejada
 - Sergio Rodrigo Iriarte Zamorano
 - Pablo Daniel Rodriguez Solis
 - Francisco Joaquin Paniagua Claure
 - Axel Eddy Martinez Ayala

### Marco Teórico
 - XAMPP: entorno de desarrollo con PHP.
 - CODEIGNITER: marco de desarrollo de aplicaciones, un conjunto de herramientas, para personas que crean sitios web utilizando PHP.
 - MVC: arquitectura del software utilizada para separar el código por sus distintas responsabilidades, manteniendo distintas capas que se encargan de hacer una tarea muy concreta.
 - HTML: lenguaje que se emplea para el desarrollo de páginas de internet.
 - CSS: lenguaje de hojas de estilo en cascada que estiliza elementos escritos en un lenguaje de marcado como HTML. CSS separa el contenido de la representación visual del sitio.
 - BOOTSTRAP: framework front-end utilizado para desarrollar aplicaciones web y sitios mobile first
 - JAVASCRIPT: lenguaje de programación o de secuencias de comandos que te permite implementar funciones complejas en páginas web.
 - PHP: lenguaje de código abierto muy popular especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML.
 - SUBLIME TEXT/VISUAL STUDIO CODE: editores de código multiplataforma.
 - VIVIFYSCRUM: solución de gestión de proyectos que ayuda a las organizaciones a obtener resultados al liberar los poderes de colaboración de sus equipos.
 - GITLAB: alojamiento de repositorios con varias funciones de seguimientos de problemas.
 - CANVANIZER: permite esbozar ideas de negocios.
 - SQL: lenguaje de consulta estructurado (SQL) es el lenguaje de base de datos.
 - MySQL-FRONT: herramienta para la gestión y administración de bases de datos MySQL. 

### Marco Práctico
 - Sublime Text 3/ Visual Studio Code como editor de código fuente 
 - MySQL Front como gestor de base de datos. 
 - XAMPP para implantar el sistema de manera local. 
 - CodeIgniter como framework para sistemas web basados en MVC.
 - Bootstrap como framework de interfaz gráfica. 
 - PHP como lenguaje de programación para conectarse a la base de datos. 
 - JavaScript es el lenguaje de programación que permitirá llevar a cabo actividades tanto simples como complejas en nuestro sitio web. 
 - HTML es el lenguaje de etiquetas que usaremos para crear nuestra página web. 
 - CSS es el lenguaje que usaremos para diseñar nuestro documento HTML. 
 - SQL será utilizado para administrar y recuperar información del sistema de gestión de una base de datos relacional. 
